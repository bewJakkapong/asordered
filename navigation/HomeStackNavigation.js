import React, {Component} from 'react';
import {TabNavigator, TabBarBottom, StackNavigator} from 'react-navigation'
import HomeScreen from '../screen/HomeScreen'
import NearbyCateScreen from '../screen/category_screen/NearbyCateScreen'
import PopularCateScreen from '../screen/category_screen/PopularCateScreen'
import PriceCateScreen from '../screen/category_screen/PriceCateScreen'
import TopRateCateScreen from '../screen/category_screen/TopRateCateScreen'
// import ProductSelected from '../screen/ProductSelectedScreen'

export default StackNavigator({
    HomeScreen: {
        screen: HomeScreen,
        navigationOptions: {
            header:null,
        },
    },
    NearbyCateScreen:{
        screen: NearbyCateScreen,
        navigationOptions: {
            gesturesEnabled:true
        },
    },
    PopularCateScreen:{
        screen: PopularCateScreen,
        navigationOptions: {
            gesturesEnabled:true
        },
    },
    PriceCateScreen:{
        screen: PriceCateScreen,
        navigationOptions: {
            gesturesEnabled:true
        },
    },
    TopRateCateScreen:{
        screen: TopRateCateScreen,
        navigationOptions: {
            gesturesEnabled:true
        },
    },
},{
    onTransitionStart:null
})