import React, {Component} from 'react';
import FontAwesome, {Icons} from 'react-native-fontawesome';
import {TabNavigator, TabBarBottom, StackNavigator} from 'react-navigation';
import {
    TouchableWithoutFeedback,
    TouchableOpacity
} from 'react-native';
import CategoryScreen from '../screen/CategoryScreen'
import CategorySubScreen from '../screen/CategorySubScreen'
import PinScreen from '../screen/PinScreen'
import SearchScreen from '../screen/SearchScreen'
import AuthenScreen from '../auth/AuthenScreen'
import ShopManageScreen from '../users/component/shop_manager/ShopManageScreen'
import UserManagement from '../users/component/admin/UserManagement'
import UserDetailScreen from '../users/UserTabScreen'
import OrderScreen from '../users/component/shop_manager/OrderScreen'
import SendingOrderScreen from '../users/component/shop_manager/SendingOrderScreen'
import DeliveredOrderScreen from '../users/component/shop_manager/DeliveredOrderScreen'
import MyOrder from '../users/component/common/MyOrder'
import HelpScreen from '../screen/HelpScreen'
import SettingScreen from '../setting/SettingScreen'
import AddProductScreen from '../screen/AddProductScreen'
import TabBarComponent from '../config/TabBarComponent.js'
import HomeStackNavigation from '../navigation/HomeStackNavigation'
import Colors from '../constant/Color'

const categoryStack =  StackNavigator({
    CategoryScreen: {screen: CategoryScreen},
    CategorySubScreen: {screen: CategorySubScreen},
})

const userDetailStack =  StackNavigator({
    UserDetailScreen: {screen: UserDetailScreen},
    AuthenScreen: {screen: AuthenScreen},
    AddProductScreen: {screen: AddProductScreen},
    ShopManageScreen: {screen: ShopManageScreen},
    UserManagement: {screen: UserManagement},
    OrderScreen: {screen: OrderScreen},
    SendingOrderScreen: {screen: SendingOrderScreen},
    DeliveredOrderScreen: {screen: DeliveredOrderScreen},
    MyOrder: {screen: MyOrder},
    HelpScreen: {screen: HelpScreen},
    SettingScreen: {screen: SettingScreen},
})

export default TabNavigator({
    MainScreen: {
        screen: HomeStackNavigation,
        navigationOptions: {
            header: null,
            tabBarLabel: 'Home',
            tabBarIcon: ({focused}) =>
                <FontAwesome color={focused ? Colors.tabIconSelected : Colors.tabIconDefault}
                             style={{fontSize: 28}}>{Icons.home}</FontAwesome>
        }
    },
    CategoryScreen: {
        screen: categoryStack,
        navigationOptions: {
            header: null,
            tabBarLabel: 'Category',
            tabBarIcon: ({focused}) =>
                <FontAwesome color={focused ? Colors.tabIconSelected : Colors.tabIconDefault}
                             style={{fontSize: 28}}>{Icons.book}</FontAwesome>
        }
    },
    PinScreen: {
        screen: PinScreen,
        navigationOptions: {
            tabBarLabel: 'Saved',
            tabBarIcon: ({focused}) =>
                <FontAwesome color={focused ? Colors.tabIconSelected : Colors.tabIconDefault}
                             style={{fontSize: 28}}>{Icons.heart}</FontAwesome>
        }
    },
    SearchScreen: {
        screen: SearchScreen,
        navigationOptions: {
            header: null,
            tabBarLabel: 'Search',
            tabBarIcon: ({focused}) =>
                <FontAwesome color={focused ? Colors.tabIconSelected : Colors.tabIconDefault}
                             style={{fontSize: 28}}>{Icons.search}</FontAwesome>
        }
    },
    UserDetailScreen: {
        screen: userDetailStack,
        navigationOptions: {
            header: null,
            tabBarLabel: 'User',
            tabBarIcon: ({focused}) =>
                <FontAwesome
                    color={focused ? Colors.tabIconSelected : Colors.tabIconDefault}
                    style={{fontSize: 28}}>{Icons.userCircleO}</FontAwesome>

        }
    },
}, {
    initialRouteName: 'MainScreen',
    tabBarOptions: {
        activeTintColor: '#1354C7',
        labelStyle: {
            fontSize: 12,
        },
        style: {
            backgroundColor: '#fff',
        },
        showLabel: false
    },
    tabBarComponent: TabBarComponent,
    tabBarPosition: 'bottom',
    animationEnabled: false,
    swipeEnabled: false,
});