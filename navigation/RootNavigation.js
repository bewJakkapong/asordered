import React, {Component} from 'react';
import {TabNavigator, TabBarBottom, StackNavigator, StackRouter} from 'react-navigation'
import MainTabNavigation from '../navigation/MainTabNavigation'
// import PaymentNavigation from '../navigation/PaymentNavigation'
import AuthenScreen from '../auth/AuthenScreen'
import ProductSelected from '../screen/ProductSelectedScreen'
import PaymentFirstScreen from '../payment/PaymentFirstScreen'
import PaymentSecondScreen from '../payment/PaymentSecondScreen'
import PaymentThirdScreen from '../payment/PaymentThirdScreen'
import CheckOut from '../payment/CheckOut'
import CameraScreen from '../screen/CameraScreen'
import CommentScreen from '../screen/CommentScreen'
import CartScreen from '../users/component/common/Cart'

const PaymentNavigation = StackNavigator({
    PaymentFirstScreen: {
        screen: PaymentFirstScreen,
        navigationOptions: {
            header: null,
        },
    },
    PaymentSecondScreen: {
        screen: PaymentSecondScreen,
        navigationOptions: {
            header: null,
        },
    },
    PaymentThirdScreen: {
        screen: PaymentThirdScreen,
        navigationOptions: {
            header: null,
        },
    }
}, {
    transitionConfig: () => ({screenInterpolator: () => null}),
    initialRouteName: 'PaymentFirstScreen',
    initialRouteParams: {...this.state}
})

export const RootNavigation = StackNavigator({
    Main: {
        screen: MainTabNavigation,
        navigationOptions: {
            // title: 'As Ordered',
            // headerTitleStyle: {
            //     alignSelf: 'center',
            // },
        },
    },
    PaymentNavigation: {
        screen: PaymentNavigation,
        navigationOptions: {
            header: null,
            gesturesEnabled: true
        },
    },
    AuthenScreen: {
        screen: AuthenScreen,
        navigationOptions: {
            header: null,
            tabBarLabel: 'Authen'
        }
    },
    ProductSelected: {
        screen: ProductSelected,
        navigationOptions: {
            header: null,
            gesturesEnabled: true
        }
    },
    CameraScreen: {
        screen: CameraScreen,
        navigationOptions: {
            header: null,
        },
    },
    CartScreen: {
        screen: CartScreen
    },
    CommentScreen: {
        screen: CommentScreen
    },
    CheckOut: {
        screen: CheckOut,
    },
})