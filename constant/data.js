export const productData = [
    {
        owner: 'ตำแซ่บ',
        ownerImg: 'https://lh3.googleusercontent.com/ez8pDFoxU2ZqDmyfeIjIba6dWisd8MY_6choHhZNpO0WwLhICu0v0s5eV2WHOhuhKw=w170',
        productName: 'ส้มตำ',
        productImg: 'https://food.mthai.com/app/uploads/2015/06/iStock_000049519500_Small.jpg',
        price: '50',
        starCount: 5,
        isFavorite:true,
        isOnSale:true
    }, {
        owner: 'ลุงแดงตามสั่ง',
        ownerImg: 'https://lh3.googleusercontent.com/ez8pDFoxU2ZqDmyfeIjIba6dWisd8MY_6choHhZNpO0WwLhICu0v0s5eV2WHOhuhKw=w170',
        productName: 'ผัดกะเพรา',
        productImg: 'https://www.ifit4health.com/wp-content/uploads/2015/07/%E0%B8%82%E0%B9%89%E0%B8%B2%E0%B8%A7%E0%B8%81%E0%B8%A3%E0%B8%B0%E0%B9%80%E0%B8%9E%E0%B8%A3%E0%B8%B2-plamplaza.jpg',
        price: '40',
        starCount: 3.5,
        isFavorite:true,
        isOnSale:true
    }, {
        owner: 'ก๋วยเตี๋ยวเรือ',
        ownerImg: 'https://lh3.googleusercontent.com/ez8pDFoxU2ZqDmyfeIjIba6dWisd8MY_6choHhZNpO0WwLhICu0v0s5eV2WHOhuhKw=w170',
        productName: 'ก๋วยเตี๋ยว',
        productImg: 'http://www.goodlifeupdate.com/wp-content/uploads/2017/06/IMG_9873-1.jpg',
        price: '45',
        starCount: 5,
        isFavorite:true,
        isOnSale:true
    }, {
        owner: 'Bakery',
        ownerImg: 'https://lh3.googleusercontent.com/ez8pDFoxU2ZqDmyfeIjIba6dWisd8MY_6choHhZNpO0WwLhICu0v0s5eV2WHOhuhKw=w170',
        productName: 'เค้กช็อคโกแลต',
        productImg: 'https://bakerykhunmea.files.wordpress.com/2015/04/cake.jpg',
        price: '70',
        starCount: 4.5,
        isFavorite:false,
        isOnSale:true
    }, {
        owner: 'ลุงดำขนมจีน',
        ownerImg: 'https://lh3.googleusercontent.com/ez8pDFoxU2ZqDmyfeIjIba6dWisd8MY_6choHhZNpO0WwLhICu0v0s5eV2WHOhuhKw=w170',
        productName: 'ขนมจีน',
        productImg: 'http://s.isanook.com/he/0/ud/0/3769/istock_92782817_medium.jpg',
        price: '40',
        starCount: 3.5,
        isFavorite:true,
        isOnSale:true
    }
]

export const categoryList = [
    {
        name: 'ก๋วยเตี๊ยว',
        cate_id: 'cate001',
        avatar_url: 'http://www.kinaddhatyai.com/wp-content/uploads/2016/03/IMG_8526.jpg',
    },
    {
        name: 'หมูกระทะ',
        cate_id: 'cate002',
        avatar_url: 'https://img.wongnai.com/p/l/2016/06/25/537cfc373d9d4a5eab45ede11833eff5.jpg',
        subtitle: 'Vice Chairman'
    },
    {
        name: 'อาหารอีสาน',
        cate_id: 'cate003',
        avatar_url: 'https://aa5c7776681e451efc62-48b60f67464290d47e3cedcb7e171621.ssl.cf2.rackcdn.com/0ab66520-9957-6259.jpg',
        subtitle: 'Vice Chairman'
    },
    {
        name: 'ชาบู',
        cate_id: 'cate004',
        avatar_url: 'https://img.wongnai.com/p/l/2016/08/01/323ccdeaa80141b2a79202bc35517a86.jpg',
        subtitle: 'Vice Chairman'
    },
    {
        name: 'สเต๊ก',
        cate_id: 'cate005',
        avatar_url: 'https://i.ytimg.com/vi/GtO8GChLWK4/hqdefault.jpg',
        subtitle: 'Vice Chairman'
    },
]

export const OrderList = [
    {
        owner_name: 'สมชาย',
        product_name: 'ก๋วยเตี๋ยว',
        description: 'ธรรมดา',
        total: '2',
        payment_type:'เก็บเงินปลายทาง',
        avatar_url: 'https://lh3.googleusercontent.com/ez8pDFoxU2ZqDmyfeIjIba6dWisd8MY_6choHhZNpO0WwLhICu0v0s5eV2WHOhuhKw=w170',
    },{
        owner_name: 'สมหญิง',
        product_name: 'กะเพราไก่',
        description: 'พิเศษ',
        total: '1',
        payment_type:'เก็บเงินปลายทาง',
        avatar_url: 'https://lh3.googleusercontent.com/ez8pDFoxU2ZqDmyfeIjIba6dWisd8MY_6choHhZNpO0WwLhICu0v0s5eV2WHOhuhKw=w170',
    },{
        owner_name: 'สมศักดิ์',
        product_name: 'ส้มตำ',
        description: 'ธรรมดา',
        total: '1',
        payment_type:'เก็บเงินปลายทาง',
        avatar_url: 'https://lh3.googleusercontent.com/ez8pDFoxU2ZqDmyfeIjIba6dWisd8MY_6choHhZNpO0WwLhICu0v0s5eV2WHOhuhKw=w170',
    },
]