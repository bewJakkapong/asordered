/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, {Component} from 'react';
import {Platform, StyleSheet, Text, View, ScrollView, Alert} from 'react-native';
import {ButtonGroup, Button, Card, FormInput, CheckBox} from 'react-native-elements'
import {hostIP, port, createOrder} from '../common/AuthenVariable'
import Layout from "../constant/Layout";
import {NavigationActions} from 'react-navigation'
import MapView from 'react-native-maps';
import PopupDialog, {DialogTitle, DialogButton} from 'react-native-popup-dialog';

const instructions = Platform.select({
    ios: 'Press Cmd+R to reload,\n' +
    'Cmd+D or shake for dev menu',
    android: 'Double tap R on your keyboard to reload,\n' +
    'Shake or press menu button for dev menu',
});

export default class PaymentThirdScreen extends Component<> {

    constructor(props) {
        super(props);
        const param = props.navigation.state.params.product
        this.state = {
            ...props.navigation.state.params,
            selectedIndex: 2,
            regisDataChecked: true,
            textInputEditable: false,
            loginMsgDialog: '',
            error: '',
            region: {
                latitude: 14.987966,
                longitude: 102.117760,
                latitudeDelta: 0.0922,
                longitudeDelta: 0.0421,
            },
            coordinate: {
                latitude: 14.987966,
                longitude: 102.117760,
            },
        };
    }

    onMapPress(e) {
        this.setState({
            coordinate: e.nativeEvent.coordinate,
        });
    }

    // ${userID},${stID},${orderDate},${orderStatusID},${lat},${long},${phoneNumber},${paymentID}
    // ${pdID},${qty},${comment},${isExtra}
    onFinished() {
        this.props
            .navigation
            .dispatch(NavigationActions.reset(
                {
                    index: 0,
                    key: null,
                    actions: [
                        NavigationActions.navigate({routeName: 'Main'})
                    ]
                }));
    }

    updateCheck() {
        let newItem = this.state.currentUser
        if (this.state.regisDataChecked) {
            newItem.lat = this.state.coordinate.latitude
            newItem.long = this.state.coordinate.longitude
            newItem.phoneNumber = this.state.phoneNumber

            this.setState({
                regisDataChecked: false,
                textInputEditable: true,
                currentUser: newItem,
            })
        } else {
            newItem.lat = this.state.regisLat
            newItem.long = this.state.regisLong
            newItem.phoneNumber = this.state.regisPhone

            this.setState({
                regisDataChecked: true,
                textInputEditable: false,
                currentUser: newItem,
            })
        }
    }

    confirmDialog() {
        Alert.alert(
            'AsOrdered',
            'ยืนยันการสั่งซื้อ',
            [
                {text: 'ยกเลิก', onPress: () => console.log('Cancel Pressed'), style: 'cancel'},
                {text: 'ตกลง', onPress: () => this.checkInfo()},
            ],
            {cancelable: false}
        )
    }

    checkInfo() {
        if (!this.state.currentUser.lat || !this.state.currentUser.phoneNumber) {
            Alert.alert(
                'AsOrdered',
                'กรุณากรอกข้อมูลการติดต่อให้ครบถ้วน เช่น ที่อยู่ หรือ เบอร์โทรศัพท์',
                [
                    {text: 'ตกลง', onPress: () => console.log('Waiting for update info')},
                ],
                {cancelable: false}
            )
        } else {
            this.addOrder()
        }
    }

    updatePhoneNumber(phoneNumber) {
        let newItem = this.state.currentUser
        newItem.phoneNumber = phoneNumber

        this.setState({
            currentUser: newItem,
        })

    }

    addOrder() {
        fetch(`${hostIP}${port}${createOrder}`, {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({
                userID: this.state.currentUser.userID,
                stID: this.state.product.stID,
                orderDate: new Date(new Date().getTime() + 7 * 3600 * 1000).toUTCString(),
                orderStatusID: 1,
                lat: this.state.currentUser.lat,
                long: this.state.currentUser.long,
                phoneNumber: this.state.currentUser.phoneNumber,
                paymentID: this.state.paymentID ? this.state.paymentID : 1,
                pdID: this.state.product.pdID,
                qty: this.state.productCount,
                comment: this.state.productRequirement,
                isExtra:this.state.isExtra,
                pdName:this.state.product.pdName,
                price:this.state.product.price,
            })
        })
            .then((response) => response.json())
            .then((res) => {
                console.log(res)
                if (res.ok) {
                    this.setState({
                        finished: true
                    })
                    Alert.alert(
                        'AsOrdered',
                        'การสั่งซื้อสำเร็จ กรุณารอสักครู่เพื่อจัดสส่งสินค้า',
                        [
                            {text: 'ตกลง', onPress: () => this.onFinished()},
                        ],
                        {cancelable: false}
                    )
                } else {
                    Alert.alert(
                        'AsOrdered',
                        'พบปัญหาในการสั่งสินค้า กรุณารอสักครู่แล้วลองอีกครั้ง',
                        [
                            {text: 'ตกลง', onPress: () => console.log('OK')},
                        ],
                        {cancelable: false}
                    )
                }
            })
            .done()
    }

    showPopupDialog(Msg, firebaseMsg) {
        this.setState({
            loginMsgDialog: Msg,
            error: firebaseMsg
        })
        this.popupDialog.show()
    }

    alertDialog() {
        return (
            <PopupDialog
                ref={(fadeAnimationDialog) => {
                    this.popupDialog = fadeAnimationDialog;
                }}
                dialogTitle={<DialogTitle title="AsOrdered"/>}
                width={Layout.window.width * 0.9}
                actions={[
                    <DialogButton
                        text="ปิดหน้าต่างนี้"
                        onPress={() => {
                            this.popupDialog.dismiss();
                        }}
                        onDismissed={this.onFinished()}
                        key="button-dismiss"
                    />,
                ]}
            ><View style={{flex: 1, justifyContent: 'center', alignItems: 'center'}}>
                <Text style={{textAlign: 'center'}}>
                    {this.state.loginMsgDialog}
                </Text>
                <Text style={{textAlign: 'center'}}>
                    {this.state.error}
                </Text>
            </View>
            </PopupDialog>
        )
    }

    render() {
        const FirstPayment = () => <Text>1</Text>
        const SecondPayment = () => <Text>2</Text>
        const ThirdPayment = () => <Text>3</Text>

        const buttons = [{element: FirstPayment}, {element: SecondPayment}, {element: ThirdPayment}]
        const {selectedIndex} = this.state

        console.log('PaymentThird', this.state)
        return (
            <View style={{flex: 1}}>
                <ScrollView>
                    <View style={{alignItems: 'center'}}>
                        <Card
                            width={Layout.window.width}
                            containerStyle={{marginBottom: 30, padding: 0}}>
                            <CheckBox
                                onPress={() => this.updateCheck()}
                                containerStyle={{width: Layout.window.width - (20)}}
                                title='ใช้ข้อมูลตามที่ลงทะเบียนไว้'
                                iconType='font-awesome'
                                checkedIcon='check-square-o'
                                uncheckedIcon='square-o'
                                checkedColor='red'
                                checked={this.state.regisDataChecked}
                            />
                            <FormInput
                                containerStyle={{margin: 5}}
                                placeholder="เบอร์โทรศัพท์ผู้รับ" editable={this.state.textInputEditable}
                                value={this.state.currentUser.phoneNumber}
                                onChangeText={(phoneNumber) => this.updatePhoneNumber(phoneNumber)}
                            />
                            <View style={{justifyContent: 'center', alignItems: 'center', margin: 10}}>
                                <Text>ที่อยู่ในการจัดส่ง</Text>
                                <MapView provider={this.props.provider}
                                         style={{flex: 1, width: Layout.window.width * 0.8, height: 200}}
                                         initialRegion={this.state.region}
                                         onPress={(e) => this.onMapPress(e)}
                                >
                                    <MapView.Marker coordinate={this.state.coordinate}>

                                    </MapView.Marker>
                                </MapView>
                            </View>
                        </Card>
                        <Button
                            buttonStyle={{
                                width: Layout.window.width * 0.8,
                                borderRadius: 5,
                                backgroundColor: '#03A9F4',
                                marginBottom: 10
                            }}
                            iconRight={{name: 'check', type: 'font-awesome'}}
                            title='ยืนยันการทำรายการ'
                            onPress={() => this.confirmDialog()}/>
                        <Button
                            buttonStyle={{
                                width: Layout.window.width * 0.8,
                                borderRadius: 5,
                                backgroundColor: '#B01B00',
                                marginBottom: 10
                            }}
                            iconRight={{name: 'close', type: 'font-awesome'}}
                            title='ยกเลิกการทำรายการ'
                            onPress={() => this.onFinished()}/>
                    </View>
                </ScrollView>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'flex-start',
        alignItems: 'center',
        backgroundColor: '#FFF',
    },
    welcome: {
        fontSize: 20,
        textAlign: 'center',
        margin: 10,
    },
    instructions: {
        textAlign: 'center',
        color: '#333333',
        marginBottom: 5,
    },
});
