/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, {Component} from 'react';
import {StyleSheet, Text, View, TouchableOpacity, ScrollView} from 'react-native';
import {NavigationActions} from 'react-navigation'
import {Card, ButtonGroup, Button, CheckBox, FormInput} from 'react-native-elements'
import Layout from "../constant/Layout";
import FontAwesome, {Icons} from 'react-native-fontawesome';

export default class PaymentFirstScreen extends Component<> {
    constructor(props) {
        super(props)
        this.state = {
            ...props.navigation.state.params,
            productRequirement: null,
            selectedIndex: 0,
            productCount: 1,
            normalChecked: true,
            isExtra: false
        }
    }

    componentDidMount() {
        // try {
        //     this.setState({
        //         paramProduct: this.props.navigation.state.params.product
        //     })
        // } catch (error) {
        //
        // }
    }

    updateProductCount(isPlus) {
        let count = this.state.productCount
        if (isPlus) {
            count += 1
            this.setState({productCount: count})
        } else if (count > 1) {
            count -= 1
            this.setState({productCount: count})
        }
    }

    updateCheck() {
        if (this.state.normalChecked) {
            this.setState({
                normalChecked: false,
                isExtra: true
            })
        } else {
            this.setState({
                normalChecked: true,
                isExtra: false
            })
        }
    }

    onFinished() {
        this.props
            .navigation
            .dispatch(NavigationActions.reset(
                {
                    index: 0,
                    key: null,
                    actions: [
                        NavigationActions.navigate({routeName: 'Main'})
                    ]
                }));
    }

    render() {
        const FirstPayment = () => <Text>1</Text>
        const SecondPayment = () => <Text>2</Text>
        const ThirdPayment = () => <Text>3</Text>

        const buttons = [{element: FirstPayment}, {element: SecondPayment}, {element: ThirdPayment}]
        const {selectedIndex} = this.state

        // console.log('product',this.state.paramProduct)
        return (
            <ScrollView>
                <ButtonGroup
                    disableSelected={true}
                    onPress={this.updateIndex}
                    selectedIndex={selectedIndex}
                    buttons={buttons}
                    selectedBackgroundColor={'#03A9F4'}
                    selectedTextStyle={{color: '#FFF', fontSize: 24}}
                    containerStyle={{height: 40, marginTop: (Layout.window.height * 0.05)}}
                />
                <View style={styles.container}>
                    <View style={{alignItems: 'center'}}>
                        <Card
                            width={Layout.window.width}
                            image={{uri: this.state.product.imgURL}}
                            imageStyle={{height: 300}}
                            containerStyle={{marginBottom: 30}}>
                            <View style={styles.cardContainer}>
                                <Text style={{fontSize: 24}}>{this.state.product.pdName}</Text>
                                <View style={{
                                    flexDirection: 'row', justifyContent: 'center',
                                    alignItems: 'center'
                                }}>
                                    <TouchableOpacity onPress={() => this.updateProductCount(false)}>
                                        <FontAwesome style={{fontSize: 24}}>{Icons.minusCircle}</FontAwesome>
                                    </TouchableOpacity>
                                    <Text style={{fontSize: 24, marginHorizontal: 15}}>{this.state.productCount}</Text>
                                    <TouchableOpacity onPress={() => this.updateProductCount(true)}>
                                        <FontAwesome style={{fontSize: 24}}>{Icons.plusCircle}</FontAwesome>
                                    </TouchableOpacity>
                                </View>
                                <View style={{flex: 1, flexDirection: 'row', justifyContent: 'center'}}>
                                    <CheckBox
                                        onPress={() => this.updateCheck()}
                                        center
                                        containerStyle={{width: Layout.window.width * 0.40}}
                                        title='ธรรมดา'
                                        iconType='font-awesome'
                                        checkedIcon='check-square-o'
                                        uncheckedIcon='square-o'
                                        checkedColor='red'
                                        checked={this.state.normalChecked}
                                    />
                                    <CheckBox
                                        onPress={() => this.updateCheck()}
                                        center
                                        containerStyle={{width: Layout.window.width * 0.40}}
                                        title='พิเศษ'
                                        iconType='font-awesome'
                                        checkedIcon='check-square-o'
                                        uncheckedIcon='square-o'
                                        checkedColor='red'
                                        checked={this.state.isExtra}
                                    />
                                </View>
                                <FormInput
                                    containerStyle={{margin: 5}}
                                    placeholder="ระบุข้อความเพิ่มเติม"
                                    onChangeText={(productRequirement) => this.setState({productRequirement})}
                                />
                            </View>
                        </Card>
                        <Button
                            buttonStyle={{
                                width: Layout.window.width * 0.8,
                                borderRadius: 5,
                                backgroundColor: '#00B013',
                                marginBottom: 10
                            }}
                            iconRight={{name: 'arrow-right', type: 'font-awesome'}}
                            title='ขั้นตอนต่อไป'
                            onPress={() => this.props.navigation.navigate('PaymentSecondScreen', {...this.state})}/>
                        <Button
                            buttonStyle={{
                                width: Layout.window.width * 0.8,
                                borderRadius: 5,
                                backgroundColor: '#B01B00',
                                marginBottom: 10
                            }}
                            iconRight={{name: 'close', type: 'font-awesome'}}
                            title='ยกเลิกการทำรายการ'
                            onPress={() => this.onFinished()}/>
                    </View>
                </View>
            </ScrollView>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'space-between',
        alignItems: 'center',
        backgroundColor: '#E7E7E7',
    },
    cardContainer: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#FFF',
    },
    welcome: {
        fontSize: 20,
        textAlign: 'center',
        margin: 10,
    },
    instructions: {
        textAlign: 'center',
        color: '#333333',
        marginBottom: 5,
    },
});
