/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, {Component} from 'react';
import {TextInput, StyleSheet, Text, View, ScrollView, TouchableOpacity, AsyncStorage} from 'react-native';
import {NavigationActions} from 'react-navigation'
import {hostIP, port, getPaymentList} from '../common/AuthenVariable'
import {FormLabel, Card, ButtonGroup, Button} from 'react-native-elements'
import Layout from "../constant/Layout";
import ModalPicker from 'react-native-modal-picker'
import {Badge, Icon, Avatar, ThemeProvider} from 'react-native-material-ui';

export default class PaymentSecondScreen extends Component<> {

    constructor(props) {
        super(props);
        this.state = {
            ...props.navigation.state.params,
            selectedIndex: 1,
            paymentText: null,
            paymentID: null,
            paymentList: [],
            cartBadge: false
        };
        console.log('PaymentSecond', props)
        console.log('PaymentSecond State', this.state)
    }

    componentWillMount() {
        this._getPaymentList().done()
        this.refreshCart().done()
    }

    componentDidMount() {

    }

    //============= Get Data =============//
    _getPaymentList = async () => {
        fetch(hostIP + port + getPaymentList, {
            method: 'GET',
        })
            .then((response) => response.json())
            .then((responseJson) => {
                if (responseJson.status === 'error') {
                } else if (responseJson.status === 'success') {
                    this.setState({
                        paymentList: responseJson.data
                    })
                }
            })
            .catch((error) => {
                console.error(error)
            });
    }

    productTotalPrice() {
        let totalPrice
        totalPrice = this.state.product.price * this.state.productCount
        return totalPrice
    }

    onFinished() {
        this.props
            .navigation
            .dispatch(NavigationActions.reset(
                {
                    index: 0,
                    key: null,
                    actions: [
                        NavigationActions.navigate({routeName: 'Main'})
                    ]
                }));
    }

    addToCart() {
        // onPress={() => this.setState({cartBadge:!cartBadge})}
        AsyncStorage.getItem('cartItem', (err, result) => {
            if (result) {
                let item = JSON.parse(result)
                let existIndex = item.pdID.findIndex((id) => this.state.product.pdID === id)

                if (existIndex >= 0){
                    item.qty[existIndex] += this.state.productCount

                    let newItem = JSON.stringify(item)
                    AsyncStorage.setItem('cartItem', newItem, () => {
                        this.refreshCart().done()
                    })
                } else if (existIndex < 0){
                    item.imgURL.push(this.state.product.imgURL)
                    item.stName.push(this.state.product.stName)
                    item.orderStatusID.push(1)
                    item.stID.push(this.state.product.stID)
                    item.pdID.push(this.state.product.pdID)
                    item.qty.push(this.state.productCount)
                    item.comment.push(this.state.productRequirement)
                    item.isExtra.push(this.state.isExtra)
                    item.pdName.push(this.state.product.pdName)
                    item.price.push(this.state.product.price)

                    let newItem = JSON.stringify(item)
                    AsyncStorage.setItem('cartItem', newItem, () => {
                        this.refreshCart().done()
                    })
                }
            } else {
                let item = JSON.stringify({
                    userID: this.state.currentUser.userID,
                    orderDate: new Date(new Date().getTime() + 7 * 3600 * 1000).toUTCString(),
                    lat: this.state.currentUser.lat,
                    long: this.state.currentUser.long,
                    phoneNumber: this.state.currentUser.phoneNumber,
                    paymentID: this.state.paymentID ? this.state.paymentID : 1,
                    imgURL: [this.state.product.imgURL],
                    stName: [this.state.product.stName],
                    orderStatusID: [1],
                    stID: [this.state.product.stID],
                    pdID: [this.state.product.pdID],
                    qty: [this.state.productCount],
                    comment: [this.state.productRequirement],
                    isExtra: [this.state.isExtra],
                    pdName: [this.state.product.pdName],
                    price: [this.state.product.price],
                })
                AsyncStorage.setItem('cartItem', item, () => {
                    this.refreshCart().done()
                })
            }
        })
    }

    refreshCart = async () => {
        AsyncStorage.getItem('cartItem', (err, result) => {
            console.log(result)
            if (result) {
                let item = JSON.parse(result)
                let qty = item.qty
                let itemCount = null
                for (let x = 0; x < qty.length; x++) {
                    itemCount += qty[x]
                }
                this.setState({
                    cartCount: itemCount
                })
            } else {
                this.setState({
                    cartCount: null
                })
            }
        })
    }

    handleOnNavigateBack = (cartCount) => {
        this.setState({
            cartCount
        })
    }

    render() {
        const FirstPayment = () => <Text>1</Text>
        const SecondPayment = () => <Text>2</Text>
        const ThirdPayment = () => <Text>3</Text>

        const buttons = [{element: FirstPayment}, {element: SecondPayment}, {element: ThirdPayment}]
        const {selectedIndex} = this.state

        const data = this.state.paymentList

        for (let x = 0; x < data.length; x++) {
            data[x]['key'] = data[x].paymentID ? data[x].paymentID : data[x].key
            data[x]['label'] = data[x].paymentName ? data[x].paymentName : data[x].label

            delete data[x].paymentID
            delete data[x].paymentName
        }

        const cartBadge = this.state.cartCount ?
            <View style={{
                flex: 1,
                flexDirection: 'row',
                justifyContent: 'flex-end',
                paddingRight: 10,
                paddingTop: 10
            }}>
                <Badge text={String(this.state.cartCount)}>
                    <TouchableOpacity
                        onPress={() => this.props.navigation.navigate('CartScreen', {onNavigateBack: this.handleOnNavigateBack})}>
                        <Icon name="shopping-cart"/>
                    </TouchableOpacity>
                </Badge>
            </View>
            :
            null;

        // console.log('PaymentSecond', this.state.paramProduct)
        return (
            <ScrollView>
                <ButtonGroup
                    disableSelected={true}
                    onPress={this.updateIndex}
                    selectedIndex={selectedIndex}
                    buttons={buttons}
                    selectedBackgroundColor={'#03A9F4'}
                    selectedTextStyle={{color: '#FFF', fontSize: 24}}
                    containerStyle={{height: 40, marginTop: (Layout.window.height * 0.05)}}
                />
                <View style={styles.container}>
                    <View style={{alignItems: 'center'}}>
                        <Card
                            title={this.state.product.productName}
                            width={Layout.window.width}
                            imageStyle={{height: 300}}
                            containerStyle={{marginBottom: 5, marginTop: 0}}>
                            {cartBadge}
                            <View style={{
                                flex: 1,
                                flexDirection: 'column',
                                justifyContent: 'center',
                                alignItems: 'center'
                            }}>
                                <Text>{this.state.product.pdName}</Text>
                            </View>
                            <View style={{flex: 1, flexDirection: 'row', justifyContent: 'space-between'}}>
                                <Text>ราคา</Text>
                                <Text>{this.state.product.price}</Text>
                            </View>
                            <View style={{flex: 1, flexDirection: 'row', justifyContent: 'space-between'}}>
                                <Text>จำนวน</Text>
                                <Text>{this.state.productCount}</Text>
                            </View>
                            <View style={{flex: 1, flexDirection: 'row', justifyContent: 'space-between'}}>
                                <Text>รวม</Text>
                                <Text>{this.productTotalPrice()}</Text>
                            </View>
                            <View style={{
                                flex: 1,
                                flexDirection: 'column',
                                justifyContent: 'center',
                                alignItems: 'center'
                            }}>
                                <Text>เพิ่มเติม</Text>
                                <Text>{this.state.productRequirement}</Text>
                            </View>
                        </Card>
                        <Card
                            containerStyle={{marginBottom: 20, padding: 5, paddingBottom: 0}}>
                            <ModalPicker
                                data={data}
                                initValue="Select something yummy!"
                                onChange={(option) => {
                                    this.setState({
                                        paymentText: option.label,
                                        paymentID: option.key
                                    })
                                }}>

                                <TextInput editable={false} style={[styles.textInputForm]}
                                           placeholder="กรุณาเลือกวิธีการชำระค่าสินค้า"
                                           value={this.state.paymentText}/>

                            </ModalPicker>
                        </Card>
                        <Button
                            buttonStyle={{
                                width: Layout.window.width * 0.8,
                                borderRadius: 5,
                                backgroundColor: '#00B013',
                                marginBottom: 10
                            }}
                            iconRight={{name: 'arrow-right', type: 'font-awesome'}}
                            title='ขั้นตอนต่อไป'
                            onPress={() => this.props.navigation.navigate('PaymentThirdScreen', {...this.state})}/>
                        <View style={{flex: 1, flexDirection: 'row', justifyContent: 'center'}}>
                            <Button
                                containerViewStyle={{marginRight: 5, marginLeft: 0}}
                                buttonStyle={{
                                    width: (Layout.window.width * 0.4) - 5,
                                    borderRadius: 5,
                                    backgroundColor: '#03A9F4',
                                }}
                                iconRight={{name: 'shopping-basket', type: 'font-awesome'}}
                                title='เพิ่มในตะกร้า'
                                onPress={() => this.addToCart()}/>
                            <Button
                                containerViewStyle={{marginRight: 0, marginLeft: 5}}
                                buttonStyle={{
                                    width: (Layout.window.width * 0.4) - 5,
                                    borderRadius: 5,
                                    backgroundColor: '#B01B00',
                                }}
                                iconRight={{name: 'close', type: 'font-awesome'}}
                                title='ยกเลิก'
                                onPress={() => this.onFinished()}/>
                        </View>
                    </View>
                </View>
            </ScrollView>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'space-between',
        alignItems: 'center',
        backgroundColor: '#E7E7E7',
    },
    welcome: {
        fontSize: 20,
        textAlign: 'center',
        margin: 10,
    },
    instructions: {
        textAlign: 'center',
        color: '#333333',
        marginBottom: 5,
    },
    textInputForm: {
        color: 'gray',
        marginLeft: 15,
        height: 42,
        width: Layout.window.width
    },
});
