/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, {Component} from 'react';
import {TouchableOpacity, StyleSheet, Text, View, ScrollView, Alert, AsyncStorage} from 'react-native';
import {ButtonGroup, Button, Card, FormInput, CheckBox} from 'react-native-elements'
import {hostIP, port, createCart} from '../common/AuthenVariable'
import Layout from "../constant/Layout";
import {NavigationActions} from 'react-navigation'
import MapView from 'react-native-maps';
import PopupDialog, {DialogTitle, DialogButton} from 'react-native-popup-dialog';
import Icon from 'react-native-vector-icons/dist/FontAwesome';

const Left = ({onPress}) => (
    <TouchableOpacity onPress={onPress}>
        <Icon style={{fontSize: 20, paddingLeft: 15}} name={'chevron-left'}/>
    </TouchableOpacity>
);

export default class CheckOut extends Component<> {

    static navigationOptions = ({navigation}) => ({
        title: 'Check Out',
        mode: 'modal',
        headerLeft: <Left onPress={() => navigation.goBack()}/>
    });

    constructor(props) {
        super(props);
        this.state = {
            ...props.navigation.state.params,
            regisLat: this.props.navigation.state.params.lat,
            regisLong: this.props.navigation.state.params.long,
            regisPhone: this.props.navigation.state.params.phoneNumber,
            selectedIndex: 2,
            regisDataChecked: true,
            textInputEditable: false,
            loginMsgDialog: '',
            error: '',
            region: {
                latitude: 14.987966,
                longitude: 102.117760,
                latitudeDelta: 0.0922,
                longitudeDelta: 0.0421,
            },
            coordinate: {
                latitude: 14.987966,
                longitude: 102.117760,
            },
        };
    }

    onMapPress(e) {
        this.setState({
            coordinate: e.nativeEvent.coordinate,
        });
    }

    // ${userID},${stID},${orderDate},${orderStatusID},${lat},${long},${phoneNumber},${paymentID}
    // ${pdID},${qty},${comment},${isExtra}
    onFinished() {
        this.props
            .navigation
            .dispatch(NavigationActions.reset(
                {
                    index: 0,
                    key: null,
                    actions: [
                        NavigationActions.navigate({routeName: 'Main'})
                    ]
                }));
    }

    updateCheck() {
        let newItem = this.state.fullItem
        if (this.state.regisDataChecked) {
            newItem.lat = this.state.coordinate.latitude
            newItem.long = this.state.coordinate.longitude
            newItem.phoneNumber = this.state.phoneNumber

            this.setState({
                regisDataChecked: false,
                textInputEditable: true,
                fullItem: newItem,
            })
        } else {
            newItem.lat = this.state.regisLat
            newItem.long = this.state.regisLong
            newItem.phoneNumber = this.state.regisPhone

            this.setState({
                regisDataChecked: true,
                textInputEditable: false,
                fullItem: newItem,
            })
        }
    }

    clearCart() {
        AsyncStorage.removeItem('cartItem')
        this.onFinished()
    }

    addOrder() {
        const newItem = this.state.fullItem
        console.log('ORI', newItem)
        console.log('JSON-STR', JSON.stringify(newItem))

        fetch(`${hostIP}${port}${createCart}`, {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
            },
            body: JSON.stringify(newItem)
        })
            .then((response) => response.json())
            .then((res) => {
                console.log(res)
                if (res.ok) {
                    this.setState({
                        finished: true
                    })
                    Alert.alert(
                        'AsOrdered',
                        'การสั่งซื้อสำเร็จ กรุณารอสักครู่เพื่อจัดสส่งสินค้า',
                        [
                            {text: 'ตกลง', onPress: () => this.clearCart()},
                        ],
                        {cancelable: false}
                    )
                } else {
                    Alert.alert(
                        'AsOrdered',
                        'พบปัญหาในการสั่งสินค้า กรุณารอสักครู่แล้วลองอีกครั้ง',
                        [
                            {text: 'ตกลง', onPress: () => console.log('OK')},
                        ],
                        {cancelable: false}
                    )
                }
            })
            .done()
    }

    showPopupDialog(Msg, firebaseMsg) {
        this.setState({
            loginMsgDialog: Msg,
            error: firebaseMsg
        })
        this.popupDialog.show()
    }

    confirmDialog() {
        Alert.alert(
            'AsOrdered',
            'ยืนยันการสั่งซื้อ',
            [
                {text: 'ยกเลิก', onPress: () => console.log('Cancel Pressed'), style: 'cancel'},
                {text: 'ตกลง', onPress: () => this.checkInfo()},
            ],
            {cancelable: false}
        )
    }

    checkInfo() {
        if (!this.state.fullItem.lat || !this.state.fullItem.phoneNumber) {
            Alert.alert(
                'AsOrdered',
                'กรุณากรอกข้อมูลการติดต่อให้ครบถ้วน เช่น ที่อยู่ หรือ เบอร์โทรศัพท์',
                [
                    {text: 'ตกลง', onPress: () => console.log('Waiting for update info')},
                ],
                {cancelable: false}
            )
        } else {
            this.addOrder()
        }
    }

    updatePhoneNumber(phoneNumber) {
        let newItem = this.state.fullItem
        newItem.phoneNumber = phoneNumber

        this.setState({
            fullItem: newItem,
        })

    }

    alertDialog() {
        return (
            <PopupDialog
                ref={(fadeAnimationDialog) => {
                    this.popupDialog = fadeAnimationDialog;
                }}
                dialogTitle={<DialogTitle title="AsOrdered"/>}
                width={Layout.window.width * 0.9}
                actions={[
                    <DialogButton
                        text="ปิดหน้าต่างนี้"
                        onPress={() => {
                            this.popupDialog.dismiss();
                        }}
                        onDismissed={this.onFinished()}
                        key="button-dismiss"
                    />,
                ]}
            ><View style={{flex: 1, justifyContent: 'center', alignItems: 'center'}}>
                <Text style={{textAlign: 'center'}}>
                    {this.state.loginMsgDialog}
                </Text>
                <Text style={{textAlign: 'center'}}>
                    {this.state.error}
                </Text>
            </View>
            </PopupDialog>
        )
    }

    render() {
        console.log('CheckOut', this.state)
        return (
            <View style={{flex: 1}}>
                <ScrollView>
                    <View style={{alignItems: 'center'}}>
                        <Card
                            width={Layout.window.width}
                            containerStyle={{marginBottom: 30, padding: 0}}>
                            <CheckBox
                                onPress={() => this.updateCheck()}
                                containerStyle={{width: Layout.window.width - (20)}}
                                title='ใช้ข้อมูลตามที่ลงทะเบียนไว้'
                                iconType='font-awesome'
                                checkedIcon='check-square-o'
                                uncheckedIcon='square-o'
                                checkedColor='red'
                                checked={this.state.regisDataChecked}
                            />
                            <FormInput
                                containerStyle={{margin: 5}}
                                placeholder="เบอร์โทรศัพท์ผู้รับ" editable={this.state.textInputEditable}
                                value={this.state.fullItem.phoneNumber}
                                onChangeText={(phoneNumber) => this.updatePhoneNumber(phoneNumber)}
                            />
                            <View style={{justifyContent: 'center', alignItems: 'center', margin: 10}}>
                                <Text>ที่อยู่ในการจัดส่ง</Text>
                                <MapView provider={this.props.provider}
                                         style={{flex: 1, width: Layout.window.width * 0.8, height: 200}}
                                         initialRegion={this.state.region}
                                         onPress={(e) => this.onMapPress(e)}
                                >
                                    <MapView.Marker coordinate={this.state.coordinate}>

                                    </MapView.Marker>
                                </MapView>
                            </View>
                        </Card>
                        <Button
                            buttonStyle={{
                                width: Layout.window.width * 0.8,
                                borderRadius: 5,
                                backgroundColor: '#03A9F4',
                                marginBottom: 10
                            }}
                            iconRight={{name: 'check', type: 'font-awesome'}}
                            title='ยืนยันการทำรายการ'
                            onPress={() => this.confirmDialog()}/>
                        <Button
                            buttonStyle={{
                                width: Layout.window.width * 0.8,
                                borderRadius: 5,
                                backgroundColor: '#B01B00',
                                marginBottom: 10
                            }}
                            iconRight={{name: 'close', type: 'font-awesome'}}
                            title='ยกเลิกการทำรายการ'
                            onPress={() => this.onFinished()}/>
                    </View>
                </ScrollView>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'flex-start',
        alignItems: 'center',
        backgroundColor: '#FFF',
    },
    welcome: {
        fontSize: 20,
        textAlign: 'center',
        margin: 10,
    },
    instructions: {
        textAlign: 'center',
        color: '#333333',
        marginBottom: 5,
    },
});
