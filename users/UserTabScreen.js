/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, {Component} from 'react';
import {StyleSheet, View, TouchableOpacity, AsyncStorage, ScrollView} from 'react-native';
import {ListItem, Avatar, List, Button} from 'react-native-elements'
import firebase from 'firebase';
import FontAwesome, {Icons} from 'react-native-fontawesome';
import Layout from '../constant/Layout'
import AuthenScreen from '../auth/AuthenScreen'
import {firebaseConfig} from "../common/AuthenVariable"
import {hostIP, port, getMyOrder, getByToken, getShopManagerInfo, getShopOrder} from "../common/AuthenVariable"
import {
    UserDetail, PostForSell, Order, Help, Cart,
    Setting, Logout, UserAllowed, UserBanned, Report, MyOrder,
    Sending, History
} from './component/common/UserForm'

const waitingOrder = 1
const sendingOrder = 2
const cancelOrder = 3
const deliveredOrder = 4

export default class UserDetailScreen extends Component<> {
    static navigationOptions = {
        header: null
    };

    //============= Component Mount =============//
    constructor(props) {
        super(props);
        this.state = {
            loggedIn: false,
            orderList: [],
            orderCount: 0
        }
    }

    componentWillMount() {
        if (!firebase.apps.length) {
            firebase.initializeApp(firebaseConfig);
        }

        firebase.auth().onAuthStateChanged((user) => {
            if (user) {
                this.getUserInfo(user.uid)
            } else {
                this._loadUser().done()
            }
        });

        this.refreshCart().done()
    }

    componentDidMount() {

    }

    //============= Auth Action =============//
    _logOut = async () => {
        AsyncStorage.clear()
        this.setState({loggedIn: false})
        clearInterval(this.setInterval())
        firebase.auth().signOut()
    }

    _loadUser = async () => {
        AsyncStorage.getItem('currentUser', (err, result) => {
            const data = JSON.parse(result)
            if (data !== null) {
                console.log('Login State ============= >', 'TRUE')
                this.setState({
                    loggedIn: true,
                    displayName: data.displayName,
                    userAccess: data.accessControl,
                    currentUser: data,
                })
            } else {
                console.log('Login State ============= >', 'FALSE')
                this.setState({
                    loggedIn: false
                })
            }
        })
            .then((res) => JSON.parse(res))
            .then((result) => {
                console.log(result)
                this.setInterval(result)
            })
    }

    setInterval(result) {
        if (result.accessControl === 'user') {
            setInterval(() => {
                this.getMyOrder(result.userID)
            }, 2000)
        } else if (result.accessControl === 'shop-manager') {
            setInterval(() => {
                this.getShopOrder(result.stID)
            }, 2000)
        }
    }

    _LoginPage() {
        this.props.navigation.navigate('AuthenScreen', {currentPage: 'UserDetailScreen'})
    }

    getUserInfo(uid) {
        fetch(hostIP + port + getByToken + '/' + uid, {
            method: 'GET',
        })
            .then((response) => response.json())
            .then((responseJson) => {
                if (responseJson.status === 'error') {
                    console.log(responseJson)
                } else if (responseJson.status === 'success') {
                    const data = responseJson.data
                    const noData = 'NoData'
                    this.setState({
                        userUID: uid,
                        displayName: data.displayName ? data.displayName : noData,
                        userAccess: data.accessControl ? data.accessControl : noData,
                    })
                    if (data.accessControl === 'shop-manager') {
                        this.getShopInfo(data.userID)
                    } else {
                        AsyncStorage.setItem('currentUser', JSON.stringify(data))
                            .then(() => this._loadUser().done())

                    }
                }
            })
            .catch((error) => {
                console.error(error);
            });
    }

    getShopInfo = async (userID) => {
        fetch(hostIP + port + getShopManagerInfo + '/' + userID, {
            method: 'GET',
        })
            .then((response) => response.json())
            .then((responseJson) => {
                if (responseJson.status === 'error') {
                    console.log(responseJson)
                } else if (responseJson.status === 'success') {
                    const data = responseJson.data
                    AsyncStorage.setItem('currentUser', JSON.stringify(data))
                        .then(() => this._loadUser().done())
                }
            })
            .catch((error) => {
                console.error(error);
            });
    }

    //============= Get Data =============//
    getMyOrder(userID) {
        fetch(hostIP + port + getMyOrder + '/' + userID, {
            method: 'GET',
        })
            .then((response) => response.json())
            .then((responseJson) => {
                if (responseJson.status === 'error') {
                    console.log(responseJson)
                } else if (responseJson.status === 'success') {
                    const data = responseJson.data
                    this.setState({
                        orderCount: data.length,
                        orderList: data
                    })
                }
            })
            .catch((error) => {
                console.error(error);
            });
    }

    getShopOrder(stID) {
        fetch(hostIP + port + getShopOrder + '/' + stID + '/' + waitingOrder, {
            method: 'GET',
        })
            .then((response) => response.json())
            .then((responseJson) => {
                if (responseJson.status === 'error') {
                    console.log(responseJson)
                } else if (responseJson.status === 'success') {
                    const data = responseJson.data
                    this.setState({
                        orderCount: data.length,
                        orderList: data
                    })
                }
            })
            .catch((error) => {
                console.error(error);
            });

        fetch(hostIP + port + getShopOrder + '/' + stID + '/' + sendingOrder, {
            method: 'GET',
        })
            .then((response) => response.json())
            .then((responseJson) => {
                if (responseJson.status === 'error') {
                    console.log(responseJson)
                } else if (responseJson.status === 'success') {
                    const data = responseJson.data
                    this.setState({
                        sendingOrderCount: data.length,
                        sendingOrderList: data
                    })
                }
            })
            .catch((error) => {
                console.error(error);
            });

        fetch(hostIP + port + getShopOrder + '/' + stID + '/' + deliveredOrder, {
            method: 'GET',
        })
            .then((response) => response.json())
            .then((responseJson) => {
                if (responseJson.status === 'error') {
                    console.log(responseJson)
                } else if (responseJson.status === 'success') {
                    const data = responseJson.data
                    this.setState({
                        deliveredOrderCount: data.length,
                        deliveredOrderList: data
                    })
                }
            })
            .catch((error) => {
                console.error(error);
            });
    }

    refreshCart = async () => {
        AsyncStorage.getItem('cartItem', (err, result) => {
            console.log(result)
            if (result) {
                let item = JSON.parse(result)
                let qty = item.qty
                let itemCount = null
                for (let x = 0; x < qty.length; x++) {
                    itemCount += qty[x]
                }
                this.setState({
                    cartCount: itemCount
                })
            } else {
                this.setState({
                    cartCount: null
                })
            }
        })
    }

    handleOnNavigateBack = (cartCount) => {
        this.setState({
            cartCount
        })
    }

    //============= Component Form =============//
    shopManagerForm() {
        return (
            <View style={styles.container}>
                <View style={{alignItems: 'center'}}>
                    <UserDetail userName={this.state.displayName}
                                status={this.state.userAccess}
                                onPress={() => this.props.navigation.navigate('ShopManageScreen',
                                    {currentUser: this.state.currentUser})}/>
                    <PostForSell
                        onPress={() => this.props.navigation.navigate('AddProductScreen', {
                            lastPathCamera: '',
                            currentUser: this.state.currentUser
                        })}/>

                    <List containerStyle={{marginBottom: 0}}>
                        <Cart orderCount={this.state.cartCount}
                              onPress={() => this.props.navigation.navigate('CartScreen', {
                                  ...this.state.currentUser,
                                  onNavigateBack: this.handleOnNavigateBack
                              })}/>
                    </List>

                    <List containerStyle={{marginBottom: 0}}>
                        <Order orderCount={this.state.orderCount}
                               onPress={() => this.props.navigation.navigate('OrderScreen', {
                                   ...this.state.currentUser,
                                   orderList: this.state.orderList
                               })}/>
                        <Sending sendingCount={this.state.sendingOrderCount}
                                 onPress={() => this.props.navigation.navigate('SendingOrderScreen', {
                                     ...this.state.currentUser,
                                     sendingOrderList: this.state.sendingOrderList
                                 })}/>
                        <History onPress={() => this.props.navigation.navigate('DeliveredOrderScreen', {
                            ...this.state.currentUser,
                            deliveredOrderList: this.state.deliveredOrderList
                        })}/>
                        <Help onPress={() => this.props.navigation.navigate('HelpScreen', {name: 'Help Center'})}/>
                        <Setting onPress={() => this.props.navigation.navigate('SettingScreen', {
                            name: 'Setting',
                            currentUser: this.state.currentUser
                        })}/>
                    </List>

                    <List>
                        <Logout onPress={() => this._logOut()}/>
                    </List>
                </View>
            </View>
        )
    }

    userForm() {
        return (
            <View style={styles.container}>
                <View style={{alignItems: 'center'}}>
                    <UserDetail userName={this.state.displayName}
                                status={this.state.userAccess}/>

                    <List containerStyle={{marginBottom: 0}}>
                        <Cart orderCount={this.state.cartCount}
                              onPress={() => this.props.navigation.navigate('CartScreen', {
                                  ...this.state.currentUser,
                                  onNavigateBack: this.handleOnNavigateBack
                              })}/>
                    </List>

                    <List containerStyle={{marginBottom: 0}}>
                        <MyOrder orderCount={this.state.orderCount}
                                 onPress={() => this.props.navigation.navigate('MyOrder', {
                                     ...this.state.currentUser,
                                     orderList: this.state.orderList
                                 })}/>
                    </List>
                    <List containerStyle={{marginBottom: 0}}>
                        <Help onPress={() => this.props.navigation.navigate('HelpScreen', {name: 'Help Center'})}/>
                        <Setting onPress={() => this.props.navigation.navigate('SettingScreen', {
                            name: 'Setting',
                            currentUser: this.state.currentUser
                        })}/>
                    </List>

                    <List>
                        <Logout onPress={() => this._logOut()}/>
                    </List>
                </View>
            </View>
        )
    }

    adminForm() {
        return (
            <View style={styles.container}>
                <View style={{alignItems: 'center'}}>
                    <UserDetail userName={this.state.displayName}
                                status={this.state.userAccess}
                                onPress={() => this.props.navigation.navigate('ShopManageScreen',
                                    {userUID: this.state.userUID})}/>
                    <List>
                        <UserAllowed onPress={() => this.props.navigation.navigate('UserManagement', {
                            condition: true,
                            userUID: this.state.userUID
                        })}/>
                        <UserBanned onPress={() => this.props.navigation.navigate('UserManagement', {
                            condition: false,
                            userUID: this.state.userUID
                        })}/>
                    </List>

                    <List>
                        <Setting onPress={() => this.props.navigation.navigate('SettingScreen', {
                            name: 'Setting',
                            currentUser: this.state.currentUser
                        })}/>
                    </List>

                    <List>
                        <Report/>
                    </List>


                    <List>
                        <Logout onPress={() => this._logOut()}/>
                    </List>
                </View>
            </View>
        )
    }

    emptyUserForm() {
        return (
            <View style={styles.container}>
                <View>
                    <View style={{alignItems: 'center'}}>
                        <ListItem
                            titleStyle={{
                                fontSize: 25,
                                color: '#000'
                            }}
                            titleContainerStyle={{
                                flexDirection: 'column',
                                justifyContent: 'flex-start',
                                alignItems: 'flex-start',
                                padding: 10
                            }}
                            subtitleContainerStyle={{
                                flexDirection: 'column',
                                justifyContent: 'flex-start',
                                alignItems: 'flex-start',
                                padding: 10
                            }}
                            avatarContainerStyle={{
                                flex: 1,
                                flexDirection: 'row',
                                justifyContent: 'center',
                                alignItems: 'center'
                            }}

                            containerStyle={{
                                justifyContent: 'center',
                                alignItems: 'center'
                            }}
                            width={Layout.window.width}
                            height={Layout.window.height * 0.20}

                            title={'As Ordered'}
                            hideChevron={true}
                            avatar={<Avatar
                                width={Layout.window.height * 0.18}
                                rounded
                                icon={{
                                    name: 'user',
                                    type: 'font-awesome',
                                    color: 'white',
                                    size: (Layout.window.height * 0.15)
                                }}
                                overlayContainerStyle={{backgroundColor: 'orange'}}
                                onPress={() => console.log("Thumbs Up!")}
                                activeOpacity={0.7}
                            />}/>

                        <List containerStyle={{marginBottom: 1}}>
                            <TouchableOpacity onPress={() => this._LoginPage()}>
                                <ListItem
                                    width={Layout.window.width}
                                    height={Layout.window.height * 0.1}
                                    roundAvatar
                                    containerStyle={{justifyContent: 'center', alignItems: 'center'}}
                                    title={'Login'}
                                    titleStyle={{fontSize: 20, color: '#000'}}
                                    avatar={<View
                                        style={{justifyContent: 'center', alignItems: 'center',}}>
                                        <FontAwesome style={{
                                            fontSize: (Layout.window.height * 0.06),
                                            color: '#048AB9'
                                        }}>{Icons.signIn}</FontAwesome>
                                    </View>}/>
                            </TouchableOpacity>
                        </List>
                    </View>

                </View>
            </View>
        )
    }

    firebaseFetch(user) {
        firebase.database().ref(`/users/${user.uid}`).once('value', snapshot => {
            const accessControl = (snapshot.val()) || 'Anonymous';
            console.log('accessControl', accessControl)
            firebase.database().app
        })
    }

    render() {
        // console.log(this.state.currentUser)
        if (this.state.loggedIn) {
            switch (this.state.userAccess) {
                case 'admin' :
                    return (
                        <ScrollView>
                            {this.adminForm()}
                        </ScrollView>
                    );
                case 'shop-manager' :
                    return (
                        <ScrollView>
                            {this.shopManagerForm()}
                        </ScrollView>
                    );
                case 'user' :
                    return (
                        this.userForm()
                    );
                default:
                    return (
                        this.emptyUserForm()
                    );
            }
        }
        else {
            return (
                this.emptyUserForm()
            );
        }

    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'flex-start',
        alignItems: 'center',
        backgroundColor: '#E7E7E7',
    },
    horizontal: {
        flex: 0.1,
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
    },
    content: {
        flex: 0.9,
        flexDirection: 'row'
    },
    welcome: {
        fontSize: 20,
        textAlign: 'center',
        margin: 10,
    },
    instructions: {
        textAlign: 'center',
        color: '#333333',
        marginBottom: 5,
    },
});
