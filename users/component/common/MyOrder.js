/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, {Component} from 'react';
import {
    Platform,
    StyleSheet,
    Text,
    View,
    ScrollView,
    Switch
} from 'react-native';
import {OrderList} from '../../../constant/data'
import {Card, ListItem, Button, Avatar, SearchBar} from 'react-native-elements'
import ModalDropdown from 'react-native-modal-dropdown';
import Layout from '../../../constant/Layout'
import {hostIP, port, getMyOrder, moveOrderToTrash} from '../../../common/AuthenVariable'
import PopupDialog, {DialogTitle, DialogButton} from 'react-native-popup-dialog';

export default class MyOrder extends Component<> {
    static navigationOptions = {
        title: 'ออร์เดอร์',
        mode: 'modal',
    };

    constructor(props) {
        super(props);
        this.state = {...props.navigation.state.params}
    }

    componentDidMount() {
        console.log('MyOrder : ', this.state)
    }

    productNameOrder(or) {
        return (<View style={{flex: 1, flexDirection: 'row', justifyContent: 'space-between', marginBottom: 5}}>
            <Text>{or.pdName} : {or.comment}</Text>
            <Text>{or.qty}</Text>
        </View>)
    }

    renderDate(time) {
        const newTime = new Date(new Date(time).getTime() - 15 * 3600 * 1000).toUTCString()
        return (
            new Date(newTime).toDateString() + ' '
        )
    }

    renderTime(time) {
        const newTime = new Date(new Date(time).getTime() - 15 * 3600 * 1000).toUTCString()
        return (
            new Date(newTime).getHours() + ':' + (new Date(newTime).getMinutes() < 10 ? '0' : '') + new Date(newTime).getMinutes()
        )
    }

    removeOrder(detailID, userID) {
        fetch(hostIP + port + moveOrderToTrash + '/' + detailID, {
            method: 'PUT',
            headers: {
                'Content-Type': 'application/json',
            },
        })
            .then((res) => {
                if (res.ok) {
                    console.log('Update Success')
                    this.refreshOrder(userID)
                } else {
                    console.log('Update ERR')
                }
            })
            .catch((error) => {
                console.error(error);
            });
    }

    refreshOrder(userID) {
        fetch(hostIP + port + getMyOrder + '/' + userID, {
            method: 'GET',
        })
            .then((response) => response.json())
            .then((responseJson) => {
                if (responseJson.status === 'error') {
                    console.log(responseJson)
                } else if (responseJson.status === 'success') {
                    const data = responseJson.data
                    this.setState({
                        orderCount: data.length,
                        orderList: data
                    })
                }
            })
            .catch((error) => {
                console.error(error);
            });
    }

    renderStatus(or) {
        switch (or.orderStatusID) {
            case 1 :
                return (
                    <View>
                        <View style={{flex: 1, flexDirection: 'row', justifyContent: 'center'}}>
                            <Button
                                containerViewStyle={{marginRight: 0, marginLeft: 5, marginBottom: 0, marginTop: 5}}
                                buttonStyle={{
                                    width: (Layout.window.width * 0.4) - 5,
                                    height: (Layout.window.height * 0.05),
                                    borderRadius: 5,
                                    backgroundColor: '#B01B00',
                                }}
                                iconRight={{name: 'close', type: 'font-awesome'}}
                                title='ยกเลิก'
                                onPress={() => this.showPopupDialog(or.detailID)}/>
                        </View>
                    </View>
                )
            case 2 :
                return (
                    <View>
                        <View style={{flex: 1, flexDirection: 'row', justifyContent: 'space-between', marginBottom: 5}}>
                            <Text>จะถึงภายใน</Text>
                            <Text>{this.renderDate(or.arrive) + this.renderTime(or.arrive)}</Text>
                        </View>
                        <View style={{flex: 1, flexDirection: 'row', justifyContent: 'center'}}>
                            <Button
                                containerViewStyle={{
                                    marginRight: 5,
                                    marginLeft: 0,
                                    marginBottom: 0,
                                    marginTop: 10
                                }}
                                disabledStyle={{
                                    width: (Layout.window.width * 0.4) - 5,
                                    height: (Layout.window.height * 0.05),
                                    borderRadius: 5,
                                    backgroundColor: '#4EC300',
                                }}
                                disabled={true}
                                title='กำลังจัดส่ง...'/>
                        </View>
                    </View>
                )
            case 3 :
                return (
                    <View>
                        <View style={{flex: 1, flexDirection: 'row', justifyContent: 'center'}}>
                            <Button
                                containerViewStyle={{
                                    marginRight: 5,
                                    marginLeft: 0,
                                    marginBottom: 0,
                                    marginTop: 10
                                }}
                                disabledStyle={{
                                    width: (Layout.window.width * 0.4) - 5,
                                    height: (Layout.window.height * 0.05),
                                    borderRadius: 5,
                                    backgroundColor: '#565656',
                                }}
                                disabled={true}
                                title='ยกเลิก...'/>
                        </View>
                    </View>
                )
        }
    }

    showPopupDialog(detailID) {
        this.setState({
            detailID: detailID
        })
        this.popupDialog.show()
    }

    confirmDialog() {
        return (
            <PopupDialog
                ref={(fadeAnimationDialog) => {
                    this.popupDialog = fadeAnimationDialog;
                }}
                dialogTitle={<DialogTitle title="AsOrdered"/>}
                width={Layout.window.width * 0.9}
                actions={[
                    <View style={{flex: 1, flexDirection: 'row'}}>
                        <DialogButton
                            buttonStyle={{flex: 1}}
                            text="ยกเลิก"
                            onPress={() => {
                                this.popupDialog.dismiss();
                            }}
                            key="button-dismiss"
                        />
                        <DialogButton
                            buttonStyle={{flex: 1}}
                            text="ตกลง"
                            onPress={() => {
                                this.removeOrder(this.state.detailID, this.state.userID)
                                this.popupDialog.dismiss()
                            }}
                            key="button-accept"
                        />
                    </View>,
                ]}
            ><View style={{flex: 1, justifyContent: 'center', alignItems: 'center', padding: 5}}>
                <Text>ยืนยันการลบออร์เดอร์</Text>
            </View>
            </PopupDialog>
        )
    }

    render() {
        const orderList = this.state.orderList
        console.log(orderList)
        return (
            <View>
                {this.confirmDialog()}
                <ScrollView contentContainerStyle={{paddingBottom: 200}}>
                    <View style={{alignItems: 'center'}}>
                        {
                            orderList.map((or, i) => {
                                return (
                                    <Card
                                        key={i}
                                        width={Layout.window.width}
                                        containerStyle={{marginBottom: 0}}>
                                        <View style={{
                                            flex: 1,
                                            flexDirection: 'row',
                                            justifyContent: 'space-between',
                                            marginBottom: 5
                                        }}>
                                            <View style={{flexDirection: 'row'}}>
                                                <Avatar
                                                    small
                                                    rounded={true}
                                                    source={{uri: or.imgURL}}
                                                    activeOpacity={0.7}
                                                />
                                                <Text style={styles.listText}>{or.stName}</Text>
                                            </View>
                                            <View style={{flexDirection: 'row'}}>
                                                <Text>{this.renderDate(or.orderDate)}</Text>
                                                <Text
                                                    style={{color: '#03A9F4'}}>{this.renderTime(or.orderDate)}</Text>
                                            </View>
                                        </View>
                                        <View style={{
                                            flex: 1,
                                            flexDirection: 'row',
                                            justifyContent: 'center',
                                            marginBottom: 5
                                        }}>
                                            <Text>สินค้าที่สั่ง</Text>
                                        </View>
                                        {this.productNameOrder(or)}
                                        <View style={{
                                            flex: 1,
                                            flexDirection: 'row',
                                            justifyContent: 'space-between',
                                            marginBottom: 5
                                        }}>
                                            <Text>ราคา</Text>
                                            <Text>{or.price + ' บาท'}</Text>
                                        </View>
                                        <View style={{
                                            flex: 1,
                                            flexDirection: 'row',
                                            justifyContent: 'space-between',
                                            marginBottom: 5
                                        }}>
                                            <Text>ราคารวม</Text>
                                            <Text>{parseInt(or.price) * parseInt(or.qty) + ' บาท'}</Text>
                                        </View>
                                        <View style={{
                                            flex: 1,
                                            flexDirection: 'row',
                                            justifyContent: 'space-between',
                                            marginBottom: 5
                                        }}>
                                            <Text>การจ่ายเงิน</Text>
                                            <Text>{or.paymentName}</Text>
                                        </View>
                                        <View style={{
                                            flex: 1,
                                            flexDirection: 'row',
                                            justifyContent: 'space-between',
                                            marginBottom: 5
                                        }}>
                                            <Text>สถานะ</Text>
                                            <Text>{or.orderStatusName}</Text>
                                        </View>
                                        {this.renderStatus(or)}
                                    </Card>
                                );
                            })
                        }

                    </View>
                </ScrollView>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#F5FCFF',
    },
    welcome: {
        fontSize: 20,
        textAlign: 'center',
        margin: 10,
    },
    instructions: {
        textAlign: 'center',
        color: '#333333',
        marginBottom: 5,
    },
    listContainer: {
        flex: 1,
        flexDirection: 'row',
    },
    listContainerColumn: {
        flex: 1,
        flexDirection: 'column',
        justifyContent: 'center',
        alignItems: 'center'
    },
    listText: {
        marginLeft: 5,
        color: '#000',
        fontSize: 16
    },
    buttonPrice: {
        width: Layout.window.width / 4,
        borderRadius: 3,
        justifyContent: 'center',
        alignItems: 'center',
    },
});
