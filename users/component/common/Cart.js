/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, {Component} from 'react';
import {
    TouchableOpacity,
    StyleSheet,
    Text,
    View,
    ScrollView,
    AsyncStorage,
    Alert
} from 'react-native';
import {OrderList} from '../../../constant/data'
import {Card, ListItem, Button, Avatar, SearchBar} from 'react-native-elements'
import ModalDropdown from 'react-native-modal-dropdown';
import Layout from '../../../constant/Layout'
import Icon from 'react-native-vector-icons/dist/FontAwesome';
import {hostIP, port, createCart} from '../../../common/AuthenVariable'

const Left = ({onPress}) => (
    <TouchableOpacity onPress={onPress}>
        <Icon style={{fontSize: 20, paddingLeft: 15}} name={'chevron-left'}/>
    </TouchableOpacity>
);


export default class CartScreen extends Component<> {
    static navigationOptions = ({navigation}) => ({
        title: 'ตะกร้าสินค้า',
        mode: 'modal',
        headerLeft: <Left onPress={() => navigation.goBack()}/>
    });

    constructor(props) {
        super(props);
        this.state = {
            ...props.navigation.state.params,
            cartItem: []
        }
    }

    componentWillMount() {
        this.refreshCart().done()
    }

    componentDidMount() {
        console.log('MyOrder : ', this.state)
    }

    clearCart() {
        AsyncStorage.removeItem('cartItem')
        this.refreshCart().done()
    }

    refreshCart = async () => {
        AsyncStorage.getItem('cartItem', (err, result) => {
            if (result) {
                let item = JSON.parse(result)
                item.orderDate = new Date(new Date().getTime() + 7 * 3600 * 1000).toUTCString()

                let qty = item.qty
                let price = item.price
                let itemCount = null
                let totalPrice = null
                let itemArr = []

                for (let x = 0; x < qty.length; x++) {
                    itemCount += qty[x]
                    totalPrice += (qty[x] * price[x])
                }

                for (let x = 0; x < (item.pdID).length; x++) {
                    let jsonObj = ({
                        imgURL: item.imgURL[x],
                        stName: item.stName[x],
                        qty: item.qty[x],
                        price: item.price[x],
                        pdName: item.pdName[x],
                    })
                    itemArr.push(jsonObj)
                }

                this.setState({
                    totalPrice: totalPrice,
                    cartItem: itemArr,
                    cartCount: itemCount,
                    fullItem: item
                })
            } else {
                this.setState({
                    totalPrice: 0,
                    cartItem: [],
                    cartCount: null
                })
            }
        }).then(() => {
            this.props.navigation.state.params.onNavigateBack(this.state.cartCount)
        })
    }

    selectInCart(position) {
        AsyncStorage.getItem('cartItem', (err, result) => {
            let item = JSON.parse(result)
            item.imgURL.splice(position, 1)
            item.stName.splice(position, 1)
            item.orderStatusID.splice(position, 1)
            item.stID.splice(position, 1)
            item.pdID.splice(position, 1)
            item.qty.splice(position, 1)
            item.comment.splice(position, 1)
            item.isExtra.splice(position, 1)
            item.pdName.splice(position, 1)
            item.price.splice(position, 1)

            let newItem = JSON.stringify(item)
            console.log(newItem)

            AsyncStorage.setItem('cartItem', newItem, () => {
                this.refreshCart().done()
            })
        })
    }

    checkItem() {
        const item = this.state.fullItem.pdID ? this.state.fullItem.pdID : []
        if (item.length > 0) {
            this.props.navigation.navigate('CheckOut', {fullItem: this.state.fullItem})
        } else {
            Alert.alert(
                'AsOrdered',
                'กรุณาเพิ่มสินค้าลงในตะกร้า',
                [
                    {text: 'ตกลง', onPress: () => console.log('OK Pressed')},
                ],
                {cancelable: false}
            )
        }
    }

    uploadOrder() {
        console.log(this.state.fullItem)
    }

    addOrder() {
        this.refreshCart()
            .then(fetch(`${hostIP}${port}${createCart}`, {
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json',
                },
                body: JSON.stringify(this.state.fullItem)
            })
                .then((response) => response.json())
                .then((res) => {
                    console.log(res)
                    console.log('FullItem', this.state.fullItem)
                    if (res.ok) {
                        this.clearCart()
                        console.log('Ordered Success')
                    } else {
                        console.log('พบปัญหาในการสั่งสินค้า')
                    }
                })
                .done())
    }

    render() {
        const cartItem = this.state.cartItem
        const cartSubItem = cartItem
        const circleButton = Layout.window.width * 0.10;
        console.log('CARTITEM', cartSubItem)
        return (
            <View style={{flex: 1}}>
                <ScrollView contentContainerStyle={{paddingBottom: 200}}>
                    <View style={{alignItems: 'center'}}>
                        {
                            cartItem.map((or, i) => {
                                return (
                                    <Card
                                        key={i}
                                        width={Layout.window.width}
                                        containerStyle={{marginBottom: 0, marginTop: 5}}>
                                        <View style={{
                                            flex: 1,
                                            flexDirection: 'row',
                                            justifyContent: 'space-between',
                                            marginBottom: 5
                                        }}>
                                            <View style={{flexDirection: 'row', flex: 0.7}}>
                                                <Avatar
                                                    medium
                                                    rounded={true}
                                                    source={{uri: or.imgURL}}
                                                    activeOpacity={0.7}
                                                />
                                                <View style={{marginLeft: 5, justifyContent: 'center'}}>
                                                    <Text style={{color: '#00A3DC'}}>{or.stName}</Text>
                                                    <Text>{or.pdName}</Text>
                                                </View>
                                            </View>
                                            <View
                                                style={{marginLeft: 5, justifyContent: 'center', alignItems: 'center'}}>
                                                <Text style={{color: '#393939'}}>จำนวน</Text>
                                                <Text>{or.qty}</Text>
                                            </View>
                                            <View
                                                style={{marginLeft: 5, justifyContent: 'center', alignItems: 'center'}}>
                                                <Text style={{color: '#393939'}}>ราคา/หน่วย</Text>
                                                <Text>{or.price}</Text>
                                            </View>
                                            <Button
                                                containerViewStyle={{
                                                    marginRight: 0,
                                                    marginLeft: 5,
                                                    marginBottom: 0,
                                                    marginTop: 5
                                                }}
                                                buttonStyle={{
                                                    width: circleButton,
                                                    height: circleButton,
                                                    borderRadius: circleButton,
                                                    backgroundColor: '#B01B00',
                                                }}
                                                icon={{
                                                    name: 'close', type: 'font-awesome',
                                                    size: circleButton * 0.4,
                                                    style: {marginRight: 0, padding: 0}
                                                }}
                                                onPress={() => this.selectInCart(i)}/>
                                        </View>
                                    </Card>
                                );
                            })
                        }

                    </View>
                </ScrollView>
                <View style={{
                    flexDirection: 'row',
                    width: Layout.window.width,
                    height: Layout.window.height * 0.1
                }}>
                    <View style={{
                        flex: 1,
                        flexDirection: 'row',
                        justifyContent: 'space-between',
                        alignItems: 'center',
                        paddingLeft: 25,
                        paddingRight: 25,
                        backgroundColor: '#FFF'
                    }}>
                        <Text style={{fontSize: 16, color: '#000'}}>ราคารวม</Text>
                        <Text style={{fontSize: 22, color: '#00A3DC'}}>{this.state.totalPrice}</Text>
                    </View>
                    <TouchableOpacity onPress={() => this.checkItem()} style={{
                        paddingLeft: 25,
                        paddingRight: 25,
                        justifyContent: 'center',
                        alignItems: 'center',
                        backgroundColor: '#4EC300'
                    }}>
                        <Text style={{fontSize: 16, color: '#000'}}>สั่งซื้อ</Text>
                    </TouchableOpacity>
                </View>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#F5FCFF',
    },
    welcome: {
        fontSize: 20,
        textAlign: 'center',
        margin: 10,
    },
    instructions: {
        textAlign: 'center',
        color: '#333333',
        marginBottom: 5,
    },
    listContainer: {
        flex: 1,
        flexDirection: 'row',
    },
    listContainerColumn: {
        flex: 1,
        flexDirection: 'column',
        justifyContent: 'center',
        alignItems: 'center'
    },
    listText: {
        flex: 1,
        flexDirection: 'column',
        justifyContent: 'center',
        alignItems: 'center',
        marginLeft: 5,
        color: '#000',
        fontSize: 16
    },
    buttonPrice: {
        width: Layout.window.width / 4,
        borderRadius: 3,
        justifyContent: 'center',
        alignItems: 'center',
    },
});
