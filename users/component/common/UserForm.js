import React, {Component} from 'react';
import {View, TouchableOpacity,} from 'react-native';
import {ListItem, Avatar, List} from 'react-native-elements'
import FontAwesome, {Icons} from 'react-native-fontawesome';
import Layout from '../../../constant/Layout'
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';

export const UserDetail = props => {
    const {
        onPress,
        userName,
        status
    } = props;
    return (
        <TouchableOpacity style={{backgroundColor: '#FFF'}} onPress={onPress}>
            <ListItem
                titleStyle={{
                    fontSize: 25,
                    color: '#000'
                }}
                titleContainerStyle={{
                    flexDirection: 'column',
                    justifyContent: 'flex-start',
                    alignItems: 'flex-start',
                    padding: 10
                }}
                subtitleContainerStyle={{
                    flexDirection: 'column',
                    justifyContent: 'flex-start',
                    alignItems: 'flex-start',
                    padding: 10
                }}
                avatarContainerStyle={{
                    flex: 1,
                    flexDirection: 'row',
                    justifyContent: 'center',
                    alignItems: 'center'
                }}

                containerStyle={{
                    justifyContent: 'center',
                    alignItems: 'center'
                }}
                width={Layout.window.width}
                height={Layout.window.height * 0.20}

                title={userName}
                subtitle={status}
                hideChevron={true}
                avatar={<Avatar
                    width={Layout.window.height * 0.18}
                    rounded
                    icon={{
                        name: 'user',
                        type: 'font-awesome',
                        color: 'white',
                        size: (Layout.window.height * 0.15)
                    }}
                    overlayContainerStyle={{backgroundColor: 'orange'}}
                    onPress={() => console.log("Thumbs Up!")}
                    activeOpacity={0.7}
                />}/>
        </TouchableOpacity>
    )
}

export const PostForSell = props => {
    const {
        onPress,
    } = props;
    return (
        <TouchableOpacity
            onPress={onPress}>
            <List containerStyle={{marginBottom: 1}}>
                <ListItem
                    width={Layout.window.width}
                    height={Layout.window.height * 0.1}
                    roundAvatar
                    containerStyle={{justifyContent: 'center', alignItems: 'center'}}
                    title={'โพสต์ขาย'}
                    titleStyle={{fontSize: 20, color: '#000'}}
                    avatar={<View
                        style={{justifyContent: 'center', alignItems: 'center',}}>
                        <FontAwesome style={{
                            fontSize: (Layout.window.height * 0.06),
                            color: '#048AB9'
                        }}>{Icons.plusCircle}</FontAwesome>
                    </View>}/>
            </List>
        </TouchableOpacity>
    )
}

export const Cart = props => {
    const {
        onPress,
        orderCount
    } = props;
    return (
        <TouchableOpacity style={{backgroundColor: '#FFF'}}
                          onPress={onPress}>
            <ListItem
                width={Layout.window.width}
                height={Layout.window.height * 0.1}
                roundAvatar
                containerStyle={{justifyContent: 'center', alignItems: 'center'}}
                title={'ตะกร้าสินค้า'}
                titleStyle={{fontSize: 20, color: '#000'}}
                avatar={<View
                    style={{justifyContent: 'center', alignItems: 'center',}}>
                    <FontAwesome style={{
                        fontSize: (Layout.window.height * 0.06),
                        color: 'grey'
                    }}>{Icons.shoppingCart}</FontAwesome>
                </View>}
                badge={{value: orderCount ? orderCount : 0, textStyle: {color: 'orange'}, containerStyle: {marginTop: 5}}}/>
        </TouchableOpacity>
    )
}

export const MyOrder = props => {
    const {
        onPress,
        orderCount
    } = props;
    return (
        <TouchableOpacity style={{backgroundColor: '#FFF'}}
                          onPress={onPress}>
            <ListItem
                width={Layout.window.width}
                height={Layout.window.height * 0.1}
                roundAvatar
                containerStyle={{justifyContent: 'center', alignItems: 'center'}}
                title={'ออร์เดอร์ของฉัน'}
                titleStyle={{fontSize: 20, color: '#000'}}
                avatar={<View
                    style={{justifyContent: 'center', alignItems: 'center',}}>
                    <FontAwesome style={{
                        fontSize: (Layout.window.height * 0.06),
                        color: 'grey'
                    }}>{Icons.bellO}</FontAwesome>
                </View>}
                badge={{value: orderCount, textStyle: {color: 'orange'}, containerStyle: {marginTop: 5}}}/>
        </TouchableOpacity>
    )
}

export const Order = props => {
    const {
        onPress,
        orderCount
    } = props;
    return (
        <TouchableOpacity style={{backgroundColor: '#FFF'}}
                          onPress={onPress}>
            <ListItem
                width={Layout.window.width}
                height={Layout.window.height * 0.1}
                roundAvatar
                containerStyle={{justifyContent: 'center', alignItems: 'center'}}
                title={'ออร์เดอร์เข้า'}
                titleStyle={{fontSize: 20, color: '#000'}}
                avatar={<View
                    style={{justifyContent: 'center', alignItems: 'center',}}>
                    <FontAwesome style={{
                        fontSize: (Layout.window.height * 0.06),
                        color: 'grey'
                    }}>{Icons.bellO}</FontAwesome>
                </View>}
                badge={{value: orderCount, textStyle: {color: 'orange'}, containerStyle: {marginTop: 5}}}/>
        </TouchableOpacity>
    )
}

export const Sending = props => {
    const {
        onPress,
        sendingCount
    } = props;
    return (
        <TouchableOpacity style={{backgroundColor: '#FFF'}}
                          onPress={onPress}>
            <ListItem
                width={Layout.window.width}
                height={Layout.window.height * 0.1}
                roundAvatar
                containerStyle={{justifyContent: 'center', alignItems: 'center'}}
                title={'กำลังจัดส่ง..'}
                titleStyle={{fontSize: 20, color: '#000'}}
                avatar={<View
                    style={{justifyContent: 'center', alignItems: 'center',}}>
                    <FontAwesome style={{
                        fontSize: (Layout.window.height * 0.06),
                        color: 'grey'
                    }}>{Icons.truck}</FontAwesome>
                </View>}
                badge={{value: sendingCount, textStyle: {color: 'orange'}, containerStyle: {marginTop: 5}}}/>
        </TouchableOpacity>
    )
}

export const History = props => {
    const {
        onPress,
    } = props;
    return (
        <TouchableOpacity style={{backgroundColor: '#FFF'}}
                          onPress={onPress}>
            <ListItem
                width={Layout.window.width}
                height={Layout.window.height * 0.1}
                roundAvatar
                containerStyle={{justifyContent: 'center', alignItems: 'center'}}
                title={'ประวัติออร์เดอร์'}
                titleStyle={{fontSize: 20, color: '#000'}}
                avatar={<View
                    style={{justifyContent: 'center', alignItems: 'center',}}>
                    <FontAwesome style={{
                        fontSize: (Layout.window.height * 0.06),
                        color: 'grey'
                    }}>{Icons.history}</FontAwesome>
                </View>}/>
        </TouchableOpacity>
    )
}

export const Help = props => {
    const {
        onPress,
    } = props;
    return (
        <TouchableOpacity style={{backgroundColor: '#FFF'}}
                          onPress={onPress}>
            <ListItem
                width={Layout.window.width}
                height={Layout.window.height * 0.1}
                roundAvatar
                containerStyle={{justifyContent: 'center', alignItems: 'center'}}
                title={'ความช่วยเหลือ'}
                titleStyle={{fontSize: 20, color: '#000'}}
                avatar={<View
                    style={{justifyContent: 'center', alignItems: 'center',}}>
                    <FontAwesome style={{
                        fontSize: (Layout.window.height * 0.06),
                        color: 'grey'
                    }}>{Icons.questionCircle}</FontAwesome>
                </View>}/>
        </TouchableOpacity>
    )
}

export const Setting = props => {
    const {
        onPress,
    } = props;
    return (
        <TouchableOpacity style={{backgroundColor: '#FFF'}}
                          onPress={onPress}>
            <ListItem
                width={Layout.window.width}
                height={Layout.window.height * 0.1}
                roundAvatar
                containerStyle={{justifyContent: 'center', alignItems: 'center'}}
                title={'ตั้งค่า'}
                titleStyle={{fontSize: 20, color: '#000'}}
                avatar={<View
                    style={{justifyContent: 'center', alignItems: 'center',}}>
                    <Icon
                        name="account-settings-variant"
                        size={Layout.window.height * 0.06}
                        color={'grey'}
                    />
                </View>}/>
        </TouchableOpacity>
    )
}

export const UserAllowed = props => {
    const {
        onPress,
    } = props;
    return (
        <TouchableOpacity style={{backgroundColor: '#FFF'}}
                          onPress={onPress}>
            <ListItem
                width={Layout.window.width}
                height={Layout.window.height * 0.1}
                roundAvatar
                containerStyle={{justifyContent: 'center', alignItems: 'center'}}
                title={'จัดการสิทธิ์ผู้ใช้'}
                titleStyle={{fontSize: 20, color: '#000'}}
                avatar={<View
                    style={{justifyContent: 'center', alignItems: 'center',}}>
                    <Icon
                        name="account"
                        size={Layout.window.height * 0.06}
                        color={'#0A9F00'}
                    />
                </View>}/>
        </TouchableOpacity>
    )
}

export const UserBanned = props => {
    const {
        onPress,
    } = props;
    return (
        <TouchableOpacity style={{backgroundColor: '#FFF'}}
                          onPress={onPress}>
            <ListItem
                width={Layout.window.width}
                height={Layout.window.height * 0.1}
                roundAvatar
                containerStyle={{justifyContent: 'center', alignItems: 'center'}}
                title={'ผู้ใช้ที่ถูกระงับสิทธิ์'}
                titleStyle={{fontSize: 20, color: '#000'}}
                avatar={<View
                    style={{justifyContent: 'center', alignItems: 'center',}}>
                    <Icon
                        name="account-off"
                        size={Layout.window.height * 0.06}
                        color={'#FF4242'}
                    />
                </View>}/>
        </TouchableOpacity>
    )
}

export const Report = props => {
    const {
        onPress,
    } = props;
    return (
        <TouchableOpacity style={{backgroundColor: '#FFF'}}
                          onPress={onPress}>
            <ListItem
                width={Layout.window.width}
                height={Layout.window.height * 0.1}
                roundAvatar
                containerStyle={{justifyContent: 'center', alignItems: 'center'}}
                title={'รายงาน'}
                titleStyle={{fontSize: 20, color: '#000'}}
                avatar={<View
                    style={{justifyContent: 'center', alignItems: 'center',}}>
                    <Icon
                        name="clipboard-text"
                        size={Layout.window.height * 0.06}
                        color={'grey'}
                    />
                </View>}/>
        </TouchableOpacity>
    )
}

export const Logout = props => {
    const {
        onPress,
    } = props;
    return (
        <TouchableOpacity
            onPress={onPress}>
            <ListItem
                width={Layout.window.width}
                height={Layout.window.height * 0.1}
                roundAvatar
                containerStyle={{justifyContent: 'center', alignItems: 'center'}}
                title={'ออกจากระบบ'}
                titleStyle={{fontSize: 20, color: '#000'}}
                hideChevron={true}
                avatar={<View
                    style={{justifyContent: 'center', alignItems: 'center',}}>
                    <Icon
                        name="logout-variant"
                        size={Layout.window.height * 0.06}
                        color={'#FF4242'}
                    />
                </View>}/>
        </TouchableOpacity>
    )
}