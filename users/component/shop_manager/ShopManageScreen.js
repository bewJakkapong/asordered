/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, {Component} from 'react';
import {StyleSheet, Text, View, ScrollView, Switch, AsyncStorage} from 'react-native';
import {productData} from '../../../constant/data'
import {Card, ListItem, Button, Avatar, SearchBar} from 'react-native-elements'
import StarRating from 'react-native-star-rating';
import Layout from '../../../constant/Layout'
import {hostIP, port, updateProductStatus, getProductListShop} from "../../../common/AuthenVariable"

export default class ShopManageScreen extends Component<> {
    static navigationOptions = {
        title: 'ShopManager',
        headerTitleStyle: {
            alignSelf: 'center'
        },
    };

    constructor(props) {
        super(props);
        const star = [];
        const isOnSale = [];
        productData.map((pd) => {
            star.push(pd.starCount)
            isOnSale.push(pd.isOnSale)
        })
        this.state = {
            ...props.navigation.state.params,
            starCount: star,
            productList: []
        };
    }

    onStarRatingPress(rating, i) {
        let starChange = this.state.starCount;
        starChange[i] = rating
        this.setState({
            starCount: starChange
        });
    }

    _getProductList = async () => {
        fetch(hostIP + port + getProductListShop + '/' + this.state.currentUser.stID, {
            method: 'GET',
        })
            .then((response) => response.json())
            .then((responseJson) => {
                if (responseJson.status === 'error') {
                    console.log('ERR')
                } else if (responseJson.status === 'success') {
                    let isOnSale = []
                    const pdEnable = responseJson.data
                    for (let x = 0; x < pdEnable.length; x++) {
                        isOnSale.push(pdEnable[x].enable)
                    }
                    this.setState({
                        productList: responseJson.data,
                        isOnSale: isOnSale
                    })
                }
            })
            .catch((error) => {
                console.error(error)
            });
    }

    updateProductStatus(value, i, pdID) {
        let isOnSale = this.state.isOnSale;
        isOnSale[i] = value
        this.setState({
            isOnSale: isOnSale
        });

        fetch(hostIP + port + updateProductStatus + '/' + pdID + '/' + value, {
            method: 'PUT',
            headers: {
                'Content-Type': 'application/json',
            }
        })
            .then((res) => {
                if (res.ok) {
                    this.reRender()
                } else {
                    console.log('ERR')
                }
            })
            .catch((error) => {
                console.error(error);
            });
    }

    reRender() {
        this._getProductList().done()
    }

    componentDidMount() {
        this._getProductList().done()
    }

    _addProductPayment() {
        console.log('Add To Payment', this.state.displayName)
        if (this.state.displayName === 'N' || null) {
            this.props.navigation.navigate('AuthenScreen', {
                product: this.state.paramProduct,
                authenFinish: 'PaymentFirstScreen'
            })
        }
        else if (this.state.displayName === 'Y') {
            this.props.navigation.navigate('PaymentNavigation', {
                product: this.state.paramProduct,
            })
        }
    }

    render() {
        return (
            <View>
                <ScrollView contentContainerStyle={{paddingBottom: 200}}>
                    <View style={{alignItems: 'center'}}>
                        <Card containerStyle={{padding: 0, marginTop: 0, flex: 1}}>
                            {
                                this.state.productList.map((pd, i) => {
                                    return (
                                        <ListItem
                                            titleStyle={{
                                                fontSize: 20,
                                                color: '#000'
                                            }}
                                            titleContainerStyle={{
                                                flex: 0.3,
                                                flexDirection: 'column',
                                                justifyContent: 'center',
                                                alignItems: 'center',
                                            }}
                                            subtitleContainerStyle={{
                                                flex: 0.7,
                                                flexDirection: 'column',
                                                justifyContent: 'flex-start',
                                                alignItems: 'flex-start'
                                            }}
                                            avatarContainerStyle={{
                                                flex: 1,
                                                flexDirection: 'column',
                                                justifyContent: 'flex-end',
                                                alignItems: 'flex-end'
                                            }}

                                            width={Layout.window.width}
                                            height={120}
                                            key={i}
                                            hideChevron={true}

                                            title={pd.pdName}

                                            subtitle={<View style={styles.listContainerColumn}>
                                                <View style={styles.listContainer}>
                                                    <Switch value={this.state.isOnSale[i]}
                                                            onValueChange={(value) => this.updateProductStatus(value, i, pd.pdID)}/>
                                                </View>
                                                <View style={{marginLeft: 5}}>
                                                    <Button
                                                        icon={{name: 'shopping-cart'}}
                                                        backgroundColor={'#03A9F4'}
                                                        title={pd.price + ' ฿'}
                                                        buttonStyle={styles.buttonPrice}
                                                        disabled={true}
                                                    />
                                                </View>
                                            </View>}

                                            avatar={<View style={styles.listContainerColumn}>
                                                <View style={styles.listContainer}>
                                                    <Avatar
                                                        small
                                                        rounded={true}
                                                        source={{uri: 'https://lh3.googleusercontent.com/ez8pDFoxU2ZqDmyfeIjIba6dWisd8MY_6choHhZNpO0WwLhICu0v0s5eV2WHOhuhKw=w170'}}
                                                        activeOpacity={0.7}
                                                    />
                                                    <Text style={styles.listText}>{pd.stName}</Text>
                                                </View>
                                                <View style={{marginLeft: 5}}><StarRating
                                                    disabled={false}
                                                    starSize={22}
                                                    starColor='#FFAA00'
                                                    emptyStarColor='#FFAA00'
                                                    maxStars={5}
                                                    rating={this.state.starCount[i]}
                                                    selectedStar={(rating) => this.onStarRatingPress(rating, i)}
                                                /></View>
                                            </View>}

                                            leftIcon={<Avatar
                                                height={100}
                                                width={130}
                                                source={{uri: pd.imgURL}}
                                                onPress={() => console.log("Works!")}
                                                activeOpacity={0.7}
                                            />}
                                        />
                                    );
                                })
                            }
                        </Card>
                    </View>
                </ScrollView>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#F5FCFF',
    },
    welcome: {
        fontSize: 20,
        textAlign: 'center',
        margin: 10,
    },
    instructions: {
        textAlign: 'center',
        color: '#333333',
        marginBottom: 5,
    },
    listContainer: {
        flex: 1,
        flexDirection: 'row',
    },
    listContainerColumn: {
        flex: 1,
        flexDirection: 'column',
        justifyContent: 'center',
        alignItems: 'center'
    },
    listText: {
        flex: 1,
        flexDirection: 'column',
        justifyContent: 'center',
        alignItems: 'center',
        marginLeft: 5,
        color: '#000',
        fontSize: 16
    },
    buttonPrice: {
        width: Layout.window.width / 4,
        borderRadius: 3,
        justifyContent: 'center',
        alignItems: 'center',
    },
});
