/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, {Component} from 'react';
import {
    TextInput,
    StyleSheet,
    Text,
    View,
    ScrollView,
    Alert,
    TouchableOpacity
} from 'react-native';
import {Card, ListItem, Button, Avatar, SearchBar} from 'react-native-elements'
import ModalDropdown from 'react-native-modal-dropdown';
import Layout from '../../../constant/Layout'
import {hostIP, port, storeCallBack, getShopOrder} from '../../../common/AuthenVariable'
import PopupDialog, {DialogTitle, DialogButton} from 'react-native-popup-dialog';
import {FormLabel, FormInput, FormValidationMessage} from 'react-native-elements'
import MapView from 'react-native-maps';

const avatar = 'https://lh3.googleusercontent.com/ez8pDFoxU2ZqDmyfeIjIba6dWisd8MY_6choHhZNpO0WwLhICu0v0s5eV2WHOhuhKw=w170'
export default class OrderScreen extends Component<> {
    static navigationOptions = {
        title: 'กำลังจัดส่ง..',
        mode: 'modal',
    };

    constructor(props) {
        super(props);
        this.state = {
            ...props.navigation.state.params,
            arriveTime: [],
            region: {
                latitude: 14.987966,
                longitude: 102.117760,
                latitudeDelta: 0.0922,
                longitudeDelta: 0.0421,
            },
            coordinate: {
                latitude: 14.987966,
                longitude: 102.117760,
                latitudeDelta: 0.0922,
                longitudeDelta: 0.0421,
            },
        }
    }

    componentDidMount() {
        let orderLenght = this.state.sendingOrderList
        let arriveTime = []
        orderLenght.map(() => {
            arriveTime.push(0)
        })

        this.setState({
            arriveTime: arriveTime
        })
    }

    renderDate(time) {
        const newTime = new Date(new Date(time).getTime() - 15 * 3600 * 1000).toUTCString()
        return (
            new Date(newTime).toDateString() + ' '
        )
    }

    renderTime(time) {
        const newTime = new Date(new Date(time).getTime() - 15 * 3600 * 1000).toUTCString()
        return (
            new Date(newTime).getHours() + ':' + (new Date(newTime).getMinutes() < 10 ? '0' : '') + new Date(newTime).getMinutes()
        )
    }

    onChanged(text, index) {
        let newText = '';
        let numbers = '0123456789';

        for (let i = 0; i < text.length; i++) {
            if (numbers.indexOf(text[i]) > -1) {
                newText = newText + text[i];
            }
            else {
                // your call back function
                alert("กรุณาระบุเวลาเป็นตัวเลข");
            }
        }

        let finalTime = this.state.arriveTime
        finalTime[index] = newText
        this.setState({arriveTime: finalTime});
    }

    storeCallBack(detailID, stID, val, index) {

        let convertTime = parseInt(this.state.arriveTime[index]) * 60
        let arriveTime = new Date(new Date().getTime() + 7 * 3600 * 1000 + (convertTime * 1000)).toUTCString()

        fetch(hostIP + port + storeCallBack + '/' + detailID, {
            method: 'PUT',
            headers: {
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({
                orderStatusID: val,
                storeComm: this.state.storeComm ? this.state.storeComm : null,
                arrive: arriveTime ? arriveTime : null
            })
        })
            .then((res) => {
                if (res.ok) {
                    console.log('Update Success')
                    this.refreshShopOrder(stID)
                } else {
                    console.log('Update ERR')
                }
            })
            .catch((error) => {
                console.error(error);
            });
    }

    renderStatus(or, i) {
        switch (or.orderStatusID) {
            case 1 :
                return (
                    <View>
                        <View style={{
                            flex: 1,
                            flexDirection: 'row',
                            justifyContent: 'space-between',
                            alignItems: 'center',
                            marginBottom: 5
                        }}>
                            <Text>จะถึงภายใน</Text>
                            <View style={{flexDirection: 'row', justifyContent: 'center', alignItems: 'center'}}>
                                <TextInput
                                    style={styles.textInput}
                                    width={50}
                                    keyboardType='numeric'
                                    onChangeText={(text) => this.onChanged(text, i)}
                                    value={this.state.arriveTime[i]}
                                    maxLength={10}
                                />
                                <Text>นาที</Text>
                            </View>
                        </View>
                        <View style={{flex: 1, flexDirection: 'row', justifyContent: 'center'}}>
                            <Button
                                containerViewStyle={{
                                    marginRight: 5,
                                    marginLeft: 0,
                                    marginBottom: 0,
                                    marginTop: 10
                                }}
                                buttonStyle={{
                                    width: (Layout.window.width * 0.4) - 5,
                                    height: (Layout.window.height * 0.05),
                                    borderRadius: 5,
                                    backgroundColor: '#03A9F4',
                                }}
                                iconRight={{name: 'check', type: 'font-awesome'}}
                                title='ยืนยัน'
                                onPress={() => this.storeCallBack(or.detailID, or.stID, 2, i)}/>
                            <Button
                                containerViewStyle={{
                                    marginRight: 0,
                                    marginLeft: 5,
                                    marginBottom: 0,
                                    marginTop: 10
                                }}
                                buttonStyle={{
                                    width: (Layout.window.width * 0.4) - 5,
                                    height: (Layout.window.height * 0.05),
                                    borderRadius: 5,
                                    backgroundColor: '#B01B00',
                                }}
                                iconRight={{name: 'close', type: 'font-awesome'}}
                                title='ยกเลิก'
                                onPress={() => this.showPopupDialog(or.detailID, or.stID)}/>
                        </View>
                    </View>
                )
            case 2 :
                return (
                    <View>
                        <View style={{flex: 1, flexDirection: 'row', justifyContent: 'center'}}>
                            <Button
                                containerViewStyle={{
                                    marginRight: 5,
                                    marginLeft: 0,
                                    marginBottom: 0,
                                    marginTop: 10
                                }}
                                disabledStyle={{
                                    width: (Layout.window.width * 0.4) - 5,
                                    height: (Layout.window.height * 0.05),
                                    borderRadius: 5,
                                    backgroundColor: '#4EC300',
                                }}
                                disabled={true}
                                title='กำลังจัดส่ง...'/>
                            <Button
                                containerViewStyle={{
                                    marginRight: 5,
                                    marginLeft: 0,
                                    marginBottom: 0,
                                    marginTop: 10
                                }}
                                buttonStyle={{
                                    width: (Layout.window.width * 0.4) - 5,
                                    height: (Layout.window.height * 0.05),
                                    borderRadius: 5,
                                    backgroundColor: '#03A9F4',
                                }}
                                title='จัดส่งเรียบร้อย'
                                onPress={() => this.storeCallBack(or.detailID, or.stID, 4, i)}/>
                        </View>
                    </View>
                )
            case 3 :
                return (
                    <View>
                        <View style={{flex: 1, flexDirection: 'row', justifyContent: 'center'}}>
                            <Button
                                containerViewStyle={{
                                    marginRight: 5,
                                    marginLeft: 0,
                                    marginBottom: 0,
                                    marginTop: 10
                                }}
                                disabledStyle={{
                                    width: (Layout.window.width * 0.4) - 5,
                                    height: (Layout.window.height * 0.05),
                                    borderRadius: 5,
                                    backgroundColor: '#565656',
                                }}
                                disabled={true}
                                title='ยกเลิก...'/>
                        </View>
                    </View>
                )
        }
    }

    productNameOrder(or) {
        return (<View style={{flex: 1, flexDirection: 'row', justifyContent: 'space-between', marginBottom: 5}}>
            <Text>{or.pdName} : {or.isExtra ? 'พิเศษ' : 'ธรรมดา'}</Text>
            <Text>{or.qty}</Text>
        </View>)
    }

    showPopupDialog(or) {
        console.log(or)

        const region = {
            latitude: parseFloat(or.lat),
            longitude: parseFloat(or.long),
            latitudeDelta: 0.0922,
            longitudeDelta: 0.0421,
        }

        this.setState({
            coordinate: region,
            region: region
        })

        this.popupDialog.show()
    }

    cancelDialog() {
        return (
            <PopupDialog
                ref={(fadeAnimationDialog) => {
                    this.popupDialog = fadeAnimationDialog;
                }}
                dialogTitle={<DialogTitle title="AsOrdered"/>}
                width={Layout.window.width * 0.9}
                actions={[
                    <DialogButton
                        text="ปิดหน้าต่างนี้"
                        onPress={() => {
                            this.popupDialog.dismiss();
                        }}
                        key="button-dismiss"
                    />,
                ]}
            ><View style={{flex: 1, justifyContent: 'center', alignItems: 'center'}}>
                <MapView provider={this.props.provider}
                         style={{flex: 1, width: Layout.window.width * 0.8, height: 200}}
                         region={this.state.region}
                >
                    <MapView.Marker coordinate={this.state.coordinate}>

                    </MapView.Marker>
                </MapView>
            </View>
            </PopupDialog>
        )
    }

    refreshShopOrder(stID) {
        fetch(hostIP + port + getShopOrder + '/' + stID + '/' + 2, {
            method: 'GET',
        })
            .then((response) => response.json())
            .then((responseJson) => {
                if (responseJson.status === 'error') {
                    console.log(responseJson)
                } else if (responseJson.status === 'success') {
                    const data = responseJson.data
                    this.setState({
                        sendingOrderCount: data.length,
                        sendingOrderList: data
                    })
                }
            })
            .catch((error) => {
                console.error(error);
            });
    }

    render() {
        const sendingOrderList = this.state.sendingOrderList
        return (
            <View>
                {this.cancelDialog()}
                <ScrollView contentContainerStyle={{paddingBottom: 200}}>
                    <View style={{alignItems: 'center'}}>
                        {
                            sendingOrderList.map((or, i) => {
                                return (
                                    <TouchableOpacity onPress={() => this.showPopupDialog(or)}>
                                        <Card
                                            key={i}
                                            width={Layout.window.width}
                                            containerStyle={{marginBottom: 0}}>

                                            <View style={{
                                                flex: 1,
                                                flexDirection: 'row',
                                                justifyContent: 'space-between',
                                                marginBottom: 5
                                            }}>
                                                <View style={{flexDirection: 'row'}}>
                                                    <Avatar
                                                        small
                                                        rounded={true}
                                                        source={{uri: avatar}}
                                                        activeOpacity={0.7}
                                                    />
                                                    <Text style={styles.listText}>{or.displayName}</Text>
                                                </View>
                                                <View style={{flexDirection: 'row'}}>
                                                    <Text>{this.renderDate(or.orderDate)}</Text>
                                                    <Text
                                                        style={{color: '#03A9F4'}}>{this.renderTime(or.orderDate)}</Text>
                                                </View>
                                            </View>
                                            {this.productNameOrder(or)}
                                            <View style={{
                                                flex: 1,
                                                flexDirection: 'row',
                                                justifyContent: 'space-between',
                                                marginBottom: 5
                                            }}>
                                                <Text>ราคารวม</Text>
                                                <Text>{parseInt(or.price) * parseInt(or.qty) + ' บาท'}</Text>
                                            </View>
                                            <View style={{
                                                flex: 1,
                                                flexDirection: 'row',
                                                justifyContent: 'space-between',
                                                marginBottom: 5
                                            }}>
                                                <Text>การจ่ายเงิน</Text>
                                                <Text>{or.paymentName}</Text>
                                            </View>
                                            <View style={{
                                                flex: 1,
                                                flexDirection: 'row',
                                                justifyContent: 'space-between',
                                                marginBottom: 10
                                            }}>
                                                <Text>โทรศัพท์</Text>
                                                <Text>{or.phoneNumber}</Text>
                                            </View>
                                            {this.renderStatus(or, i)}
                                        </Card>
                                    </TouchableOpacity>
                                );
                            })
                        }

                    </View>
                </ScrollView>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#F5FCFF',
    },
    welcome: {
        fontSize: 20,
        textAlign: 'center',
        margin: 10,
    },
    instructions: {
        textAlign: 'center',
        color: '#333333',
        marginBottom: 5,
    },
    listContainer: {
        flex: 1,
        flexDirection: 'row',
        justifyContent: 'space-between'
    },
    listContainerColumn: {
        flex: 1,
        flexDirection: 'column',
        justifyContent: 'center',
        alignItems: 'center'
    },
    listText: {
        marginLeft: 5,
        color: '#000',
        fontSize: 16
    },
    buttonPrice: {
        width: Layout.window.width / 4,
        borderRadius: 3,
        justifyContent: 'center',
        alignItems: 'center',
    },
});
