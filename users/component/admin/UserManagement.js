/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, {Component} from 'react';
import {
    View,
    StyleSheet,
    Text,
    TouchableOpacity,
    ScrollView
} from 'react-native';
import {List, ListItem} from 'react-native-elements'
import {categoryList} from '../../../constant/data'
import {hostIP, port, getUserWithCondition, updateUserStatus} from "../../../common/AuthenVariable"
import Layout from '../../../constant/Layout'
import PopupDialog, {DialogTitle, DialogButton} from 'react-native-popup-dialog';

export default class UserManagement extends Component<> {

    static navigationOptions = {
        title: 'UserManagement',
        headerTitleStyle: {
            alignSelf: 'center'
        },
    };

    constructor(props) {
        super(props)
        this.state = {
            userData: [],
            condition: props.navigation.state.params.condition,
            userUID: props.navigation.state.params.userUID
        }
    }

    getUserInfo(condition) {
        fetch(hostIP + port + getUserWithCondition + '/' + condition, {
            method: 'GET',
        })
            .then((response) => response.json())
            .then((responseJson) => {
                if (responseJson.status === 'error') {
                    console.log(responseJson)
                } else if (responseJson.status === 'success') {
                    const data = responseJson.data
                    const noData = 'No Data'
                    this.setState({
                        userData: data,
                    })
                }
            })
            .catch((error) => {
                console.error(error);
            });
    }

    updateUserStatus(uid) {
        fetch(hostIP + port + updateUserStatus + '/' + uid + '/' + !this.state.condition, {
            method: 'PUT',
            headers: {
                'Content-Type': 'application/json',
            }
        })
            .then((res) => {
                if (res.ok) {
                    this.showPopupDialog('Update Success')
                } else {
                    this.showPopupDialog('Update Error')
                }
            })
            .catch((error) => {
                console.error(error);
            });
    }

    showPopupDialog(Msg, firebaseMsg) {
        this.setState({
            MsgDialog: Msg,
        })

        this.popupDialog.show()
    }

    reRender(){
        console.log('Re-Rendering')
        this.getUserInfo(this.state.condition)
    }

    alertDialog() {
        return (
            <PopupDialog
                ref={(fadeAnimationDialog) => {
                    this.popupDialog = fadeAnimationDialog;
                }}
                dialogTitle={<DialogTitle title="AsOrdered"/>}
                width={Layout.window.width * 0.9}
                onDismissed={()=> this.reRender()}
                actions={[
                    <DialogButton
                        text="ปิดหน้าต่างนี้"
                        onPress={() => {
                            this.popupDialog.dismiss();
                        }}
                        key="button-dismiss"
                    />,
                ]}
            ><View style={{flex: 1, justifyContent: 'center', alignItems: 'center'}}>
                <Text style={{textAlign: 'center'}}>
                    {this.state.MsgDialog}
                </Text>
            </View>
            </PopupDialog>
        )
    }

    componentDidMount() {
        console.log('condition', this.state.condition)
        this.getUserInfo(this.state.condition)
    }

    render() {
        const userData = this.state.userData
        const userStatus = this.state.condition

        let changeCondition = userStatus ? 'Click to disable this user' : 'Click to enable this user'
        return (
            <View style={styles.container}>
                {this.alertDialog()}
                <ScrollView>
                    <View style={{alignItems: 'center', justifyContent: 'center'}}>
                        <List containerStyle={{marginBottom: 20}}>
                            {
                                userData.map((list, i) => (
                                    <TouchableOpacity
                                        onPress={()=> this.updateUserStatus(list.uid)}
                                        key={list.uid}>
                                        <ListItem
                                            width={Layout.window.width}
                                            height={60}
                                            roundAvatar
                                            avatar={{uri: list.photoURL}}
                                            title={list.displayName}
                                            subtitle={changeCondition}
                                        />
                                    </TouchableOpacity>
                                ))
                            }
                        </List>
                    </View>
                </ScrollView>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#E7E7E7',
    },
    welcome: {
        fontSize: 20,
        textAlign: 'center',
        margin: 10,
    },
    instructions: {
        textAlign: 'center',
        color: '#333333',
        marginBottom: 5,
    },
});
