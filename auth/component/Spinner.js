import React, { Component } from 'react';
import { View, ActivityIndicator } from 'react-native';

const Spinner = props => {
    const {
        size,
        containerStyle
    } = props;
    return (
        <View style={containerStyle}>
            <ActivityIndicator size={size} />
        </View>
    );
};

export { Spinner };