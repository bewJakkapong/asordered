/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, {Component} from 'react';
import {StyleSheet, Text, View, ScrollView, AsyncStorage, BackHandler, Keyboard} from 'react-native';
import {FormInput, ButtonGroup, Button} from 'react-native-elements'
import {NavigationActions} from 'react-navigation'
import firebase from 'firebase';
import Layout from "../constant/Layout";
import {hostIP, port, createByToken, getByToken, createShopManager, getShopManagerInfo} from "../common/AuthenVariable"
import {Spinner} from "./component/Spinner"
import PopupDialog, {DialogTitle, DialogButton} from 'react-native-popup-dialog';
import {loginFailMsg} from '../constant/Msg'

export default class AuthenScreen extends Component<> {
    static navigationOptions = {
        header: null
    };

    // ========================= Mount Component ========================= //
    constructor(props) {
        super(props);

        const param = props.navigation.state.params
        this.state = {
            param: param.product,
            selectedIndex: 2,
            currentPage: props.navigation.state.params.currentPage,
            loading: false,
            loginMsgDialog: '',
            email: '',
            password: '',
            passwordCF: '',
            stName:'',
            displayName:'',
            error: '',
            clearState: false
        }
        this.updateIndex = this.updateIndex.bind(this)
    }

    componentDidMount() {
        firebase.auth().onAuthStateChanged((user) => {
            if (user) {
                this.setState({loggedIn: true});
            } else {
                this.setState({loggedIn: false});
            }

            // const updateUser = firebase.auth().currentUser;
            // updateUser.updateProfile({
            //     userStatus: "admin",
            // }).then(function() {
            //     console.log('UPDATED')
            // }).catch(function(error) {
            //     console.log('UPDATE ERR')
            // });
        });

        console.log('Product ', this.state.param)
    }

    updateIndex(selectedIndex) {
        if (selectedIndex === this.state.selectedIndex) {
            this.setState({selectedIndex: 2})
        } else {
            this.setState({selectedIndex})
        }
    }

    // ========================= Authentication ========================= //
    firebaseAuth() {
        Keyboard.dismiss()
        const {email, password} = this.state;

        firebase.auth().signInWithEmailAndPassword(email, password)
            .then((user) => {
                this.loginSuccess(user)
            })
            .catch((signInError) => {
                console.log(signInError)
                const signInErrorMsg = `${signInError.message}`
                this.showPopupDialog(loginFailMsg, signInErrorMsg)
            })
    }

    databaseFetch(user) {
        Keyboard.dismiss()
        fetch(hostIP + port + getByToken + '/' + user.uid, {
            method: 'GET',
        })
            .then((response) => response.json())
            .then((responseJson) => {
                if (responseJson.status === 'error') {
                    this.showPopupDialog(loginFailMsg)
                } else if (responseJson.status === 'success') {

                    const data = responseJson.data
                    let displayName = responseJson.data.displayName ? responseJson.data.displayName : user.email

                    if (data.status) {
                        AsyncStorage.setItem('currentUser', JSON.stringify(data), () => {
                            AsyncStorage.getItem('currentUser', (err, result) => {
                                const resetAction = NavigationActions.reset({
                                    index: 0,
                                    actions: [
                                        NavigationActions.navigate({
                                            routeName: this.state.currentPage,
                                            params: {product: this.state.param}
                                        })

                                    ]
                                })
                                this.props.navigation.dispatch(resetAction)
                            })
                        })
                    } else {
                        this.showPopupDialog('ผู้ใช้ถูกระงับสิทธิ์ชั่วคราว กรุณาติดต่อเจ้าหน้าที่')
                    }

                }
            })
            .catch((error) => {
                console.error(error);
            });
    }

    demoLogin() {
        AsyncStorage.multiSet([['userSignIn', 'Somchai'],
            ['userSignInACC', 'shop-manager']], () => {
            AsyncStorage.multiGet(['userSignIn', 'userSignInACC'], (err, result) => {
                console.log(result)
                this.props.navigation.navigate(this.state.currentPage, {product: this.state.param})
            })
        })
    }

    loginSuccess(user) {
        this.databaseFetch(user)
    }

    // ========================= SignUp =========================//
    signUp(userType) {
        Keyboard.dismiss()

        const {email, password, passwordCF} = this.state;
        console.log(password)
        if (password === '') {
            this.showPopupDialog('กรุณากรอกรหัสผ่าน')
        } else {
            if (password !== passwordCF) {
                this.showPopupDialog('รหัสผ่านไม่ตรงกัน')
            } else {
                this.setState({
                    loading: true
                })

                firebase.auth().createUserWithEmailAndPassword(email, password)
                    .then(this.SignUpSuccess(userType))
                    .catch((error => console.log(error)));
            }
        }
    }

    SignUpSuccess(userType) {
        Keyboard.dismiss()
        const {email, password} = this.state;

        setTimeout(() => {
            this.setState({
                loading: false
            })
            firebase.auth().signInWithEmailAndPassword(email, password)
                .then((user) => {
                    switch (userType) {
                        case 'shop-manager' :
                            this.createShopManager(user.uid)
                            break;
                        case 'user' :
                            this.createUser(user.uid)
                            break;
                    }
                })
                .catch((signInError) => {
                    console.log(signInError)
                    const signInErrorMsg = `${signInError.message}`
                    this.showPopupDialog(loginFailMsg, signInErrorMsg)
                })
        }, 3000);
    }

    createUser(uid) {
        fetch(`${hostIP}${port}${createByToken}`, {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({
                uid: uid,
                accessControl: 'user',
                displayName: this.state.displayName,
                status: true,
            })
        })
            .then((response) => response.json())
            .then((res) => {
                console.log(res)

                this.setState({
                    clearState: true
                })

                if (res.status === 'success') {
                    this.showPopupDialog('ลงทะเบียนสำเร็จ')
                } else {
                    this.showPopupDialog('พบปัญหาในการลงทะเบียน')
                }
            })
            .done()

    }

    createShopManager(uid) {
        fetch(`${hostIP}${port}${createShopManager}`, {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({
                uid: uid,
                accessControl: 'shop-manager',
                displayName: this.state.displayName,
                storeName: this.state.stName,
                status: true,
                enable: true,
            })
        })
            .then((response) => response.json())
            .then((res) => {
                console.log(res)

                this.setState({
                    clearState: true
                })

                if (res.status === 'success') {
                    this.showPopupDialog('ลงทะเบียนสำเร็จ')
                } else {
                    this.showPopupDialog('พบปัญหาในการลงทะเบียน')
                }
            })
            .done()
    }

    // ========================= Component =========================//
    clearState() {
        if (this.state.clearState) {
            this.setState({
                selectedIndex: 2,
                password: '',
                passwordCF: '',
                displayName:'',
                stName:''
            })
        }

        this.setState({
            clearState: false
        })
    }

    showPopupDialog(Msg, firebaseMsg) {
        this.setState({
            loginMsgDialog: Msg,
            error: firebaseMsg
        })
        this.popupDialog.show()
    }

    alertDialog() {
        return (
            <PopupDialog
                ref={(fadeAnimationDialog) => {
                    this.popupDialog = fadeAnimationDialog;
                }}
                dialogTitle={<DialogTitle title="AsOrdered"/>}
                width={Layout.window.width * 0.9}
                onDismissed={() => this.clearState()}
                actions={[
                    <DialogButton
                        text="ปิดหน้าต่างนี้"
                        onPress={() => {
                            this.popupDialog.dismiss();
                        }}
                        key="button-dismiss"
                    />,
                ]}
            ><View style={{flex: 1, justifyContent: 'center', alignItems: 'center'}}>
                <Text style={{textAlign: 'center'}}>
                    {this.state.loginMsgDialog}
                </Text>
                <Text style={{textAlign: 'center'}}>
                    {this.state.error}
                </Text>
            </View>
            </PopupDialog>
        )
    }

    renderSignUpButton(name, userType) {
        // if (this.state.loading) {
        //     return <Spinner containerStyle={{marginBottom:10}} size="small"/>;
        // }

        let spinner = this.state.loading ? <Spinner containerStyle={{marginBottom: 10}} size="small"/> : null
        return (
            <View>
                {spinner}
                <Button
                    buttonStyle={{
                        width: Layout.window.width * 0.8,
                        borderRadius: 5,
                        backgroundColor: '#00B013',
                        marginBottom: 10
                    }}
                    disabled={this.state.loading}
                    icon={{name: 'sign-in', type: 'font-awesome'}}
                    title={name}
                    onPress={() => this.signUp(userType)}
                />
            </View>
        );
    }

    switchForm() {
        const UserSignUp = () => <Text>ลงทะเบียนผู้ซื้อ</Text>
        const ShopSignUp = () => <Text>ลงทะเบียนผู้ขาย</Text>

        const buttons = [{element: UserSignUp}, {element: ShopSignUp}]
        const {selectedIndex} = this.state

        let form = this.state.selectedIndex
        switch (form) {
            case 0 :
                return (
                    <View>
                        <ButtonGroup
                            onPress={this.updateIndex}
                            selectedIndex={selectedIndex}
                            buttons={buttons}
                            selectedBackgroundColor={'#03A9F4'}
                            selectedTextStyle={{color: '#FFF', fontSize: 24}}
                            containerStyle={{height: 40, marginTop: (Layout.window.height * 0.05)}}
                        />
                        <View style={{flexGrow: 1, alignItems: 'center'}}>
                            <FormInput
                                containerStyle={{margin: 5}}
                                placeholder="อีเมลล์"
                                onChangeText={(email) => this.setState({email})}
                            />
                            <FormInput
                                containerStyle={{margin: 5}}
                                secureTextEntry={true}
                                placeholder="รหัสผ่าน"
                                value={this.state.password}
                                onChangeText={(password) => this.setState({password})}
                            />
                            <FormInput
                                containerStyle={{margin: 5}}
                                secureTextEntry={true}
                                placeholder="ยืนยัน รหัสผ่าน"
                                value={this.state.passwordCF}
                                onChangeText={(passwordCF) => this.setState({passwordCF})}
                            />
                            <FormInput
                                containerStyle={{margin: 5}}
                                placeholder="ชื่อที่ใช้ในการแสดงผล"
                                onChangeText={(displayName) => this.setState({displayName})}
                            />
                            {this.renderSignUpButton('ลงทะเบียนผู้ซื้อ', 'user')}
                        </View>
                    </View>
                )
            case 1 :
                return (
                    <View>
                        <ButtonGroup
                            onPress={this.updateIndex}
                            selectedIndex={selectedIndex}
                            buttons={buttons}
                            selectedBackgroundColor={'#03A9F4'}
                            selectedTextStyle={{color: '#FFF', fontSize: 24}}
                            containerStyle={{height: 40, marginTop: (Layout.window.height * 0.05)}}
                        />
                        <View style={{flexGrow: 1, alignItems: 'center'}}>
                            <FormInput
                                containerStyle={{margin: 5}}
                                placeholder="อีเมลล์ (ใช้ในการเข้าสู่ระบบ)"
                                onChangeText={(email) => this.setState({email})}
                            />
                            <FormInput
                                containerStyle={{margin: 5}}
                                secureTextEntry={true}
                                placeholder="รหัสผ่าน"
                                onChangeText={(password) => this.setState({password})}
                            />
                            <FormInput
                                containerStyle={{margin: 5}}
                                secureTextEntry={true}
                                placeholder="ยืนยัน รหัสผ่าน"
                                onChangeText={(passwordCF) => this.setState({passwordCF})}
                            />
                            <FormInput
                                containerStyle={{margin: 5}}
                                placeholder="ชื่อร้าน"
                                onChangeText={(stName) => this.setState({stName})}
                            />
                            <FormInput
                                containerStyle={{margin: 5}}
                                placeholder="ชื่อที่ใช้ในการแสดงผล"
                                onChangeText={(displayName) => this.setState({displayName})}
                            />
                            {this.renderSignUpButton('ลงทะเบียนผู้ขาย', 'shop-manager')}
                        </View>
                    </View>
                )
            case 2 :
                return (
                    <View>
                        <ButtonGroup
                            onPress={this.updateIndex}
                            selectedIndex={selectedIndex}
                            buttons={buttons}
                            selectedBackgroundColor={'#03A9F4'}
                            selectedTextStyle={{color: '#FFF', fontSize: 24}}
                            containerStyle={{height: 40, marginTop: (Layout.window.height * 0.05)}}
                        />
                        <View style={{flexGrow: 1, alignItems: 'center'}}>
                            <FormInput
                                containerStyle={{margin: 5}}
                                placeholder="Username or Email"
                                onChangeText={(email) => this.setState({email})}
                            />
                            <FormInput
                                containerStyle={{margin: 5}}
                                placeholder="Password"
                                onChangeText={(password) => this.setState({password})}
                                secureTextEntry={true}
                            />
                            <Button
                                buttonStyle={{
                                    width: Layout.window.width * 0.8,
                                    borderRadius: 5,
                                    backgroundColor: '#00B013'
                                }}
                                icon={{name: 'sign-in', type: 'font-awesome'}}
                                title='เข้าสู่ระบบ'
                                onPress={() => this.firebaseAuth()}/>
                        </View>
                    </View>
                )
        }
    }

    render() {
        return (
            <View style={styles.container}>
                {this.alertDialog()}
                <ScrollView>
                    {this.switchForm()}
                </ScrollView>
            </View>
        )
    }
}

// Disable BackPress
// componentDidMount() {
//     BackHandler.addEventListener("hardwareBackPress", this.onBackPress);
// }
// componentWillUnmount() {
//     BackHandler.removeEventListener("hardwareBackPress", this.onBackPress);
// }
// onBackPress = () => {
//     return true
// };

// const updateUser = firebase.auth().currentUser;
// updateUser.updateProfile({
//     displayName: "Jakkapong",
//     photoURL: "https://example.com/jane-q-user/profile.jpg"
// }).then(function() {
//     console.log('UPDATED')
// }).catch(function(error) {
//     console.log('UPDATE ERR')
// });

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'flex-start',
        alignItems: 'center',
        backgroundColor: '#FFF',
    },
    welcome: {
        fontSize: 20,
        textAlign: 'center',
        margin: 10,
    },
    instructions: {
        textAlign: 'center',
        color: '#333333',
        marginBottom: 5,
    },
})
