export const hostIP = 'http://103.13.231.182'
export const port = ':3000'
export const basicAuthen = '/api/basic_authen'

export const getByToken = '/api/getByToken'
export const getAllUser = '/api/alluser'
export const getUserContacts = '/api/getUserContacts'
export const getShopManagerInfo = '/api/getShopManagerInfo'
export const getUserWithCondition = '/api/usercondition'
export const getCategoryList = '/api/getCategoryList'
export const getPaymentList = '/api/getPaymentList'
export const getProductList = '/api/getProductList'
export const getProductListCate = '/api/getProductListCate'
export const getProductListShop = '/api/getProductListShop'
export const getFavoriteList = '/api/getFavoriteList'
export const getMyOrder = '/api/getMyOrder'
export const getShopOrder = '/api/getShopOrder'
export const getProductReview = '/api/getProductReview'
export const getSearchProduct = '/api/getSearchProduct'

export const createByToken = '/api/createUserByToken'
export const createShopManager = '/api/createShopManager'
export const createProduct = '/api/createProduct'
export const createOrder = '/api/createOrder'
export const createCart = '/api/createCart'
export const createComment = '/api/createComment'
export const addProductCategory = '/api/addProductCategory'

export const updateUserInfo = '/api/updateUserInfo'
export const updateUserContacts = '/api/updateUserContacts'
export const updateUserStatus = '/api/updateUserStatus'
export const updateProductStatus = '/api/updateProductStatus'
export const storeCallBack = '/api/storeCallBack'
export const moveOrderToTrash = '/api/moveOrderToTrash'

export const firebaseConfig = {
    apiKey: "AIzaSyDQBoCgYIL-DI57YO_IGpEbbn-lQshU22I",
    authDomain: "auth-asordered.firebaseapp.com",
    databaseURL: "https://auth-asordered.firebaseio.com",
    projectId: "auth-asordered",
    storageBucket: "auth-asordered.appspot.com",
    messagingSenderId: "419960745105"
};