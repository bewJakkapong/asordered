import React, {Component} from 'react';
import {StyleSheet, Text, View, ScrollView, AsyncStorage, BackHandler, Keyboard} from 'react-native';
import {FormInput, ButtonGroup, Button} from 'react-native-elements'
import {NavigationActions} from 'react-navigation'
import firebase from 'firebase';
import Layout from "../constant/Layout";
import {hostIP, port, createByToken, getByToken} from "../common/AuthenVariable"
import PopupDialog, {DialogTitle, DialogButton} from 'react-native-popup-dialog';
import {loginFailMsg} from '../constant/Msg'

export function getUserProfile(uid,state) {

    fetch(hostIP + port + getByToken + '/' + uid, {
        method: 'GET',
    })
        .then((response) => response.json())
        .then((responseJson) => {
            if (responseJson.status === 'error') {
                console.log(responseJson)
            } else if (responseJson.status === 'success') {
                return({ ...state, displayName: responseJson.data.displayName })
            }
        })
        .catch((error) => {
            console.error(error);
        });
}