/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, {Component} from 'react';
import {StyleSheet, TouchableOpacity, Picker, View, Button, Text, ScrollView} from 'react-native';
import {ListItem, Avatar, List, FormInput} from 'react-native-elements'
import Layout from '../constant/Layout'
import FontAwesome, {Icons} from 'react-native-fontawesome';
import firebase from 'firebase';
import {hostIP, port, updateUserInfo, getByToken, getUserContacts, updateUserContacts} from "../common/AuthenVariable"
import PopupDialog, {DialogTitle, DialogButton} from 'react-native-popup-dialog';
import {NavigationActions} from 'react-navigation'
import MapView from 'react-native-maps';

const listHeight = Layout.window.height * 0.07

export default class SettingScreen extends Component<> {
    static navigationOptions = ({navigation}) => ({
        title: navigation.state.params.name,
    });

    constructor(props) {
        super(props)
        this.state = {
            language: 'En',
            userInfo: false,
            userContact: false,
            userID: props.navigation.state.params.currentUser.userID,
            displayName: '',
            name: '',
            lastName: '',
            phoneNumber: '',
            address: '',
            userUID: '',
            status: '',
            region: {
                latitude: 14.987966,
                longitude: 102.117760,
                latitudeDelta: 0.0922,
                longitudeDelta: 0.0421,
            },
            coordinate: {
                latitude: 14.987966,
                longitude: 102.117760,
                latitudeDelta: 0.0922,
                longitudeDelta: 0.0421,
            },
        }

        console.log('state props: ', props.navigation.state.params.currentUser)
    }

    componentDidMount() {
        const user = firebase.auth().currentUser
        this.setState({
            userUID: user.uid
        })
        this.getUserInfo(user.uid)
        this.getUserContacts(this.state.userID)
    }

    onMapPress(e) {
        this.setState({
            coordinate: e.nativeEvent.coordinate,
            lat: e.nativeEvent.coordinate.latitude,
            long: e.nativeEvent.coordinate.longitude,
        });
    }

    onUpdateFinished() {
        this.props
            .navigation
            .dispatch(NavigationActions.reset(
                {
                    index: 0,
                    actions: [
                        NavigationActions.navigate({routeName: 'UserDetailScreen'})
                    ]
                }));
    }

    getUserInfo(uid) {
        fetch(hostIP + port + getByToken + '/' + uid, {
            method: 'GET',
        })
            .then((response) => response.json())
            .then((responseJson) => {
                if (responseJson.status === 'error') {
                    console.log(responseJson)
                } else if (responseJson.status === 'success') {
                    const data = responseJson.data
                    const noData = null
                    this.setState({
                        displayName: data.displayName ? data.displayName : noData,
                        name: data.name ? data.name : noData,
                        lastName: data.lastName ? data.lastName : noData
                    })
                }
            })
            .catch((error) => {
                console.error(error);
            });
    }

    getUserContacts(userID) {
        fetch(hostIP + port + getUserContacts + '/' + userID, {
            method: 'GET',
        })
            .then((response) => response.json())
            .then((responseJson) => {
                if (responseJson.status === 'error') {
                    console.log(responseJson)
                } else if (responseJson.status === 'success') {
                    const data = responseJson.data
                    const coordinate = {
                        latitude: data.lat ? parseFloat(data.lat) : 14.987966,
                        longitude: data.long ? parseFloat(data.long) : 102.117760,
                        latitudeDelta: 0.0922,
                        longitudeDelta: 0.0421,
                    }
                    const noData = null

                    this.setState({
                        addNumber: data.addNumber ? data.addNumber : noData,
                        addRoad: data.addRoad ? data.addRoad : noData,
                        addDistrict: data.addDistrict ? data.addDistrict : noData,
                        addCity: data.addCity ? data.addCity : noData,
                        addProvince: data.addProvince ? data.addProvince : noData,
                        postCode: data.postCode ? data.postCode : noData,
                        phoneNumber: data.phoneNumber ? data.phoneNumber : noData,
                        coordinate: coordinate,
                        lat: data.lat ? parseFloat(data.lat) : 14.987966,
                        long: data.long ? parseFloat(data.long) : 102.117760,
                    })
                }
            })
            .catch((error) => {
                console.error(error);
            });
    }

    renderUserInfo() {
        if (this.state.userInfo) {
            this.setState({userInfo: false})
        } else {
            this.setState({userInfo: true})
        }
    }

    renderUserContact() {
        if (this.state.userContact) {
            this.setState({userContact: false})
        } else {
            this.setState({userContact: true})
        }
    }

    userInfo() {
        return (
            <View style={{alignItems: 'center'}}>
                <ListItem
                    width={Layout.window.width}
                    height={listHeight}
                    roundAvatar
                    containerStyle={{justifyContent: 'center', alignItems: 'center'}}
                    title={'ชื่อที่ใช้แสดง'}
                    titleStyle={styles.userInfoLabel}
                    rightIcon={
                        <FormInput
                            containerStyle={{width: Layout.window.width * 0.5}}
                            inputStyle={styles.listText}
                            onChangeText={(displayName) => this.setState({displayName})}
                            value={this.state.displayName}
                        />}
                />
                <ListItem
                    width={Layout.window.width}
                    height={listHeight}
                    roundAvatar
                    containerStyle={{justifyContent: 'center', alignItems: 'center'}}
                    title={'ชื่อจริง'}
                    titleStyle={styles.userInfoLabel}
                    rightIcon={
                        <FormInput
                            containerStyle={{width: Layout.window.width * 0.5}}
                            inputStyle={styles.listText}
                            onChangeText={(name) => this.setState({name})}
                            value={this.state.name}
                        />}
                />
                <ListItem
                    width={Layout.window.width}
                    height={listHeight}
                    roundAvatar
                    containerStyle={{justifyContent: 'center', alignItems: 'center'}}
                    title={'นามสกุล'}
                    titleStyle={styles.userInfoLabel}
                    rightIcon={
                        <FormInput
                            containerStyle={{width: Layout.window.width * 0.5}}
                            inputStyle={styles.listText}
                            onChangeText={(lastName) => this.setState({lastName})}
                            value={this.state.lastName}
                        />}
                />

            </View>
        )
    }

    userContact() {
        return (
            <View style={{alignItems: 'center'}}>
                <ListItem
                    width={Layout.window.width}
                    height={listHeight}
                    roundAvatar
                    containerStyle={{justifyContent: 'center', alignItems: 'center'}}
                    title={'โทรศัพท์'}
                    titleStyle={styles.userInfoLabel}
                    rightIcon={
                        <FormInput
                            containerStyle={{width: Layout.window.width * 0.5}}
                            inputStyle={styles.listText}
                            onChangeText={(phoneNumber) => this.setState({phoneNumber})}
                            value={this.state.phoneNumber}
                        />}
                />
                <ListItem
                    width={Layout.window.width}
                    height={listHeight}
                    roundAvatar
                    containerStyle={{justifyContent: 'center', alignItems: 'center'}}
                    title={'บ้านเลขที่'}
                    titleStyle={styles.userInfoLabel}
                    rightIcon={
                        <FormInput
                            containerStyle={{width: Layout.window.width * 0.5}}
                            inputStyle={styles.listText}
                            onChangeText={(addNumber) => this.setState({addNumber})}
                            value={this.state.addNumber}
                        />}
                />
                <ListItem
                    width={Layout.window.width}
                    height={listHeight}
                    roundAvatar
                    containerStyle={{justifyContent: 'center', alignItems: 'center'}}
                    title={'ถนน'}
                    titleStyle={styles.userInfoLabel}
                    rightIcon={
                        <FormInput
                            containerStyle={{width: Layout.window.width * 0.5}}
                            inputStyle={styles.listText}
                            onChangeText={(addRoad) => this.setState({addRoad})}
                            value={this.state.addRoad}
                        />}
                />
                <ListItem
                    width={Layout.window.width}
                    height={listHeight}
                    roundAvatar
                    containerStyle={{justifyContent: 'center', alignItems: 'center'}}
                    title={'ตำบล'}
                    titleStyle={styles.userInfoLabel}
                    rightIcon={
                        <FormInput
                            containerStyle={{width: Layout.window.width * 0.5}}
                            inputStyle={styles.listText}
                            onChangeText={(addDistrict) => this.setState({addDistrict})}
                            value={this.state.addDistrict}
                        />}
                />
                <ListItem
                    width={Layout.window.width}
                    height={listHeight}
                    roundAvatar
                    containerStyle={{justifyContent: 'center', alignItems: 'center'}}
                    title={'อำเภอ'}
                    titleStyle={styles.userInfoLabel}
                    rightIcon={<FormInput
                        containerStyle={{width: Layout.window.width * 0.5}}
                        inputStyle={styles.listText}
                        onChangeText={(addCity) => this.setState({addCity})}
                        value={this.state.addCity}
                    />}
                />
                <ListItem
                    width={Layout.window.width}
                    height={listHeight}
                    roundAvatar
                    containerStyle={{justifyContent: 'center', alignItems: 'center'}}
                    title={'จังหวัด'}
                    titleStyle={styles.userInfoLabel}
                    rightIcon={<FormInput
                        containerStyle={{width: Layout.window.width * 0.5}}
                        inputStyle={styles.listText}
                        onChangeText={(addProvince) => this.setState({addProvince})}
                        value={this.state.addProvince}
                    />}
                />
                <ListItem
                    width={Layout.window.width}
                    height={listHeight}
                    roundAvatar
                    containerStyle={{justifyContent: 'center', alignItems: 'center'}}
                    title={'รหัสไปรษณีย์'}
                    titleStyle={styles.userInfoLabel}
                    rightIcon={<FormInput
                        containerStyle={{width: Layout.window.width * 0.5}}
                        inputStyle={styles.listText}
                        onChangeText={(postCode) => this.setState({postCode})}
                        value={this.state.postCode}
                    />}
                />
            </View>
        )
    }

    showPopupDialog(Msg, firebaseMsg) {
        this.setState({
            MsgDialog: Msg,
        })

        this.popupDialog.show()
    }

    alertDialog() {
        return (
            <PopupDialog
                ref={(fadeAnimationDialog) => {
                    this.popupDialog = fadeAnimationDialog;
                }}
                dialogTitle={<DialogTitle title="AsOrdered"/>}
                width={Layout.window.width * 0.9}
                onDismissed={() => this.onUpdateFinished()}
                actions={[
                    <DialogButton
                        text="ปิดหน้าต่างนี้"
                        onPress={() => {
                            this.popupDialog.dismiss();
                        }}
                        key="button-dismiss"
                    />,
                ]}
            ><View style={{flex: 1, justifyContent: 'center', alignItems: 'center'}}>
                <Text style={{textAlign: 'center'}}>
                    {this.state.MsgDialog}
                </Text>
            </View>
            </PopupDialog>
        )
    }

    updateUserInfo() {
        fetch(hostIP + port + updateUserInfo + '/' + this.state.userID, {
            method: 'PUT',
            headers: {
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({
                displayName: this.state.displayName,
                name: this.state.name,
                lastName: this.state.lastName,
            })
        })
            .then((res) => {
                if (res.ok) {
                    this.showPopupDialog('Update Success')
                } else {
                    this.showPopupDialog('Update Error')
                }
            })
            .catch((error) => {
                console.error(error);
            });
    }

    updateUserContacts() {
        fetch(hostIP + port + updateUserContacts + '/' + this.state.userID, {
            method: 'PUT',
            headers: {
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({
                addNumber: this.state.addNumber,
                addRoad: this.state.addRoad,
                addDistrict: this.state.addDistrict,
                addCity: this.state.addCity,
                addProvince: this.state.addProvince,
                postCode: this.state.postCode,
                phoneNumber: this.state.phoneNumber,
                lat: this.state.lat,
                long: this.state.long,
            })
        })
            .then((res) => {
                console.log(res)
                if (res.ok) {
                    this.showPopupDialog('Update Success')
                } else {
                    this.showPopupDialog('Update Error')
                }
            })
            .catch((error) => {
                console.error(error);
            });
    }

    render() {
        let userInfo = this.state.userInfo ? this.userInfo()
            :
            null;

        let updateInfoButton = this.state.userInfo ? <Button
                containerViewStyle={{width: Layout.window.width}}
                title={'อัพเดทข้อมูล'}
                icon={{name: 'camera', type: 'font-awesome'}}
                onPress={() => this.updateUserInfo()}/>
            :
            null;

        let userContact = this.state.userContact ? this.userContact()
            :
            null;

        let updateContactButton = this.state.userContact ?
            <View style={{height: Layout.window.width * 0.6}}>
                <MapView provider={this.props.provider}
                         style={{flex: 1, width: Layout.window.width, height: Layout.window.width * 0.6}}
                         initialRegion={this.state.coordinate}
                         onPress={(e) => this.onMapPress(e)}
                >
                    <MapView.Marker coordinate={this.state.coordinate}/>
                </MapView>
                <Button
                    containerViewStyle={{width: Layout.window.width}}
                    title={'อัพเดทข้อมูล'}
                    icon={{name: 'camera', type: 'font-awesome'}}
                    onPress={() => this.updateUserContacts()}
                />
            </View>
            :
            null;

        return (
            <View style={styles.container}>
                {this.alertDialog()}
                <ScrollView>
                    <View style={{alignItems: 'center'}}>
                        <List containerStyle={{marginBottom: 1}}>
                            <TouchableOpacity onPress={() => this.renderUserInfo()} disabled={this.state.userContact}>
                                <ListItem
                                    width={Layout.window.width}
                                    height={Layout.window.height * 0.1}
                                    roundAvatar
                                    hideChevron
                                    containerStyle={{justifyContent: 'center', alignItems: 'center'}}
                                    title={'ข้อมูลส่วนตัว'}
                                    titleStyle={{fontSize: 20, color: '#000'}}
                                    avatar={<View
                                        style={{justifyContent: 'center', alignItems: 'center',}}>
                                        <FontAwesome style={{
                                            fontSize: (Layout.window.height * 0.05),
                                            color: '#00A3DC'
                                        }}>{Icons.infoCircle}</FontAwesome>
                                    </View>}/>
                            </TouchableOpacity>
                            {userInfo}
                            {updateInfoButton}
                        </List>

                        <List containerStyle={{marginBottom: 1}}>
                            <TouchableOpacity onPress={() => this.renderUserContact()} disabled={this.state.userInfo}>
                                <ListItem
                                    width={Layout.window.width}
                                    height={Layout.window.height * 0.1}
                                    roundAvatar
                                    hideChevron
                                    containerStyle={{justifyContent: 'center', alignItems: 'center'}}
                                    title={'ข้อมูลการติดต่อ'}
                                    titleStyle={{fontSize: 20, color: '#000'}}
                                    avatar={<View
                                        style={{justifyContent: 'center', alignItems: 'center',}}>
                                        <FontAwesome style={{
                                            fontSize: (Layout.window.height * 0.05),
                                            color: '#00C733'
                                        }}>{Icons.phoneSquare}</FontAwesome>
                                    </View>}/>
                            </TouchableOpacity>
                            {userContact}
                            {updateContactButton}
                        </List>

                        <List containerStyle={{marginBottom: 1}}>
                            <ListItem
                                width={Layout.window.width}
                                height={Layout.window.height * 0.1}
                                roundAvatar
                                containerStyle={{justifyContent: 'center', alignItems: 'center'}}
                                title={'ภาษา'}
                                titleStyle={{fontSize: 20, color: '#000'}}
                                avatar={<View
                                    style={{justifyContent: 'center', alignItems: 'center',}}>
                                    <FontAwesome style={{
                                        fontSize: (Layout.window.height * 0.05),
                                        color: 'grey'
                                    }}>{Icons.language}</FontAwesome>
                                </View>}
                                rightIcon={<Picker style={{flex: 1}}
                                                   selectedValue={this.state.language}
                                                   onValueChange={(itemValue, itemIndex) => this.setState({language: itemValue})}>
                                    <Picker.Item label="English" value="En"/>
                                    <Picker.Item label="Thai" value="Th"/>
                                </Picker>}/>
                        </List>
                    </View>
                </ScrollView>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#E7E7E7',
    },
    welcome: {
        fontSize: 20,
        textAlign: 'center',
        margin: 10,
    },
    instructions: {
        textAlign: 'center',
        color: '#333333',
        marginBottom: 5,
    },
    listText: {
        fontSize: Layout.window.width * 0.04,
        color: '#00A3DC'
    },
    userInfoLabel: {
        fontSize: Layout.window.width * 0.04,
        color: '#000'
    },
});
