export const product = [
    {
        owner: 'ตำแซ่บ',
        ownerImg: 'https://lh3.googleusercontent.com/ez8pDFoxU2ZqDmyfeIjIba6dWisd8MY_6choHhZNpO0WwLhICu0v0s5eV2WHOhuhKw=w170',
        productName: 'ส้มตำ',
        productImg: 'https://food.mthai.com/app/uploads/2015/06/iStock_000049519500_Small.jpg',
        price: '50',
        starCount: 5
    }, {
        owner: 'ลุงแดงตามสั่ง',
        ownerImg: 'https://lh3.googleusercontent.com/ez8pDFoxU2ZqDmyfeIjIba6dWisd8MY_6choHhZNpO0WwLhICu0v0s5eV2WHOhuhKw=w170',
        productName: 'ผัดกะเพรา',
        productImg: 'https://www.ifit4health.com/wp-content/uploads/2015/07/%E0%B8%82%E0%B9%89%E0%B8%B2%E0%B8%A7%E0%B8%81%E0%B8%A3%E0%B8%B0%E0%B9%80%E0%B8%9E%E0%B8%A3%E0%B8%B2-plamplaza.jpg',
        price: '40',
        starCount: 3.5
    }, {
        owner: 'ก๋วยเตี๋ยวเรือ',
        ownerImg: 'https://lh3.googleusercontent.com/ez8pDFoxU2ZqDmyfeIjIba6dWisd8MY_6choHhZNpO0WwLhICu0v0s5eV2WHOhuhKw=w170',
        productName: 'ก๋วยเตี๋ยว',
        productImg: 'http://www.goodlifeupdate.com/wp-content/uploads/2017/06/IMG_9873-1.jpg',
        price: '45',
        starCount: 5
    }, {
        owner: 'Bakery',
        ownerImg: 'https://lh3.googleusercontent.com/ez8pDFoxU2ZqDmyfeIjIba6dWisd8MY_6choHhZNpO0WwLhICu0v0s5eV2WHOhuhKw=w170',
        productName: 'เค้กช็อคโกแลต',
        productImg: 'https://bakerykhunmea.files.wordpress.com/2015/04/cake.jpg',
        price: '70',
        starCount: 4.5
    }, {
        owner: 'บ้านมีโชค',
        ownerImg: 'https://lh3.googleusercontent.com/ez8pDFoxU2ZqDmyfeIjIba6dWisd8MY_6choHhZNpO0WwLhICu0v0s5eV2WHOhuhKw=w170',
        productName: 'ขนมจีน',
        productImg: 'http://s.isanook.com/he/0/ud/0/3769/istock_92782817_medium.jpg',
        price: '40',
        starCount: 3.5
    }
]