/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, {Component} from 'react';
import {
    ScrollView,
    StyleSheet,
    Platform,
    View,
    Text,
    TextInput,
    TouchableOpacity
} from 'react-native';
import {FormInput, ListItem, Button, Avatar, Card, FormLabel} from 'react-native-elements'
import Layout from '../constant/Layout'
import {hostIP, port, getCategoryList, addProductCategory, createProduct} from '../common/AuthenVariable'
import firebase from 'firebase';
import RNFetchBlob from 'react-native-fetch-blob'
import PopupDialog, {DialogTitle, DialogButton} from 'react-native-popup-dialog';
import {NavigationActions} from 'react-navigation'
import DateTimePicker from 'react-native-modal-datetime-picker';
import ModalPicker from 'react-native-modal-picker'

const productID = 'pd'

export default class AddProductScreen extends Component<> {
    static navigationOptions = {
        header: null
    };

    //============= Component Mount =============//
    constructor(props) {
        super(props)
        const data = props.navigation.state.params.currentUser ?
            props.navigation.state.params.currentUser : props.navigation.state.params

        this.state = {
            displayName: data.displayName,
            accessControl: data.accessControl,
            lastPathCamera: null,
            imgSize: 0,
            language: 'En',
            pdName: data.pdName ? data.pdName : null,
            pdDesc: data.pdDesc ? data.pdDesc : null,
            price: data.price ? data.price : null,
            priceSpecial: data.priceSpecial ? data.priceSpecial : null,
            isSale: true,
            imgURL: null,
            createDate: null,
            userID: data.userID,
            stID: data.stID,
            enable: true,
            finished: false,
            saleDate: data.saleDate ? data.saleDate : null,
            saleDateEnd: data.saleDateEnd ? data.saleDateEnd : null,
            isSaleDatePickerVisible: false,
            isSaleDateEndPickerVisible: false,
            categoryText: data.categoryText ? data.categoryText : null,
            categoryList: [],
            categoryID: data.categoryID ? data.categoryID : null,
        }

        console.log(data)
    }

    componentDidMount() {
        if (this.props.navigation.state.params.lastPathCamera) {
            try {
                this.setState({
                    lastPathCamera: this.props.navigation.state.params.lastPathCamera,
                    imgSize: 300
                })
            } catch (error) {

            }
        }
    }

    componentWillMount() {
        this._getCategoryList().done()
    }

    //============= Get Data =============//
    _getCategoryList = async () => {
        fetch(hostIP + port + getCategoryList, {
            method: 'GET',
        })
            .then((response) => response.json())
            .then((responseJson) => {
                if (responseJson.status === 'error') {
                } else if (responseJson.status === 'success') {
                    this.setState({
                        categoryList: responseJson.data
                    })
                }
            })
            .catch((error) => {
                console.error(error)
            });
    }

    //============= Upload =============//
    uploadImage(uri, mime = 'image/jpeg') {
        const Blob = RNFetchBlob.polyfill.Blob;
        const fs = RNFetchBlob.fs;
        window.XMLHttpRequest = RNFetchBlob.polyfill.XMLHttpRequest
        window.Blob = Blob;

        return new Promise((resolve, reject) => {
            const uploadUri = Platform.OS === 'ios' ? uri.replace('file://', '') : uri
            let uploadBlob = null;

            const imageRef = firebase.storage().ref('productImg').child(`${this.state.storeID}/${this.state.lastPathCamera.replace(/^.*[\\\/]/, '')}`)

            fs.readFile(uploadUri, 'base64')
                .then((data) => {
                    return Blob.build(data, {type: `${mime};BASE64`})
                })
                .then((blob) => {
                    console.log('BLOB', blob)
                    uploadBlob = blob
                    return imageRef.put(blob, {contentType: mime})
                })
                .then(() => {
                    uploadBlob.close()
                    return imageRef.getDownloadURL()
                })
                .then((url) => {
                    this.setState({
                        imgURL: url
                    })
                })
                .then(() => {
                    this.uploadInfo()
                })
                .catch((error) => {
                    console.log(error)
                })
        })
    }

    uploadInfo() {
        fetch(`${hostIP}${port}${createProduct}`, {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({
                pdName: this.state.pdName,
                pdDesc: this.state.pdDesc,
                price: this.state.price,
                priceSpecial: this.state.priceSpecial,
                isSale: this.state.isSale,
                saleStart: new Date((this.state.saleDate).getTime() + 7 * 3600 * 1000).toUTCString(),
                saleEnd: new Date((this.state.saleDateEnd).getTime() + 7 * 3600 * 1000).toUTCString(),
                imgURL: this.state.imgURL,
                createDate: this.timeStamp(),
                userID: this.state.userID,
                stID: this.state.stID,
                enable: this.state.enable,
                cateID: this.state.categoryID
            })
        })
            .then((response) => response.json())
            .then((res) => {
                console.log(res)
                if (res.ok) {
                    this.setState({
                        finished: true
                    })
                    this.showPopupDialog('Upload Success')
                } else {
                    this.showPopupDialog('พบปัญหาในการเพิ่มสินค้า')
                }
            })
            .done()
    }

    //============= Upload Condition =============//
    uploadProduct() {
        if (this.state.lastPathCamera) {
            this.uploadImage(this.state.lastPathCamera)
        } else {
            this.uploadInfo()
        }
    }

    //============= Date Time Picker =============//
    _showSaleDatePicker() {
        this.setState({
            isSaleDatePickerVisible: true
        })
    };

    _showSaleDateEndPicker() {
        this.setState({
            isSaleDateEndPickerVisible: true
        })
    };

    _handleSaleDate(date) {
        this.setState({
            saleDate: date,
        })

        this._hideDateTimePicker();
    };

    _handleSaleDateEnd(date) {
        this.setState({
            saleDateEnd: date,
        })

        this._hideDateTimePicker();
    };

    _hideDateTimePicker = () => this.setState({
        isSaleDatePickerVisible: false,
        isSaleDateEndPickerVisible: false,
    });

    //============= Generator =============//
    UUID(type) {
        function s4() {
            return Math.floor((1 + Math.random()) * 0x10000)
                .toString(16)
                .substring(1);
        }

        return `${type}${s4()}${s4()}${s4()}${s4()}${s4()}${s4()}${s4()}`;
    }

    timeStamp() {
        const offset = +7 * 3600 * 1000;
        return new Date(new Date().getTime() + offset).toUTCString().replace(/ GMT/, "");
    }

    //============= Common =============//
    showPopupDialog(Msg) {
        this.setState({
            MsgDialog: Msg,
        })

        this.popupDialog.show()
    }

    enableShareButton = () => {
        return (this.state.pdName ? false : this.state.pdName > 0 || this.state.price ? false : this.state.price > 0 || this.state.cateID ? false : this.state.cateID > 0)
    }

    alertDialog() {
        return (
            <PopupDialog
                ref={(fadeAnimationDialog) => {
                    this.popupDialog = fadeAnimationDialog;
                }}
                dialogTitle={<DialogTitle title="AsOrdered"/>}
                width={Layout.window.width * 0.9}
                onDismissed={() => this.onFinish()}
                actions={[
                    <DialogButton
                        text="ปิดหน้าต่างนี้"
                        onPress={() => {
                            this.popupDialog.dismiss();
                        }}
                        key="button-dismiss"
                    />,
                ]}
            ><View style={{flex: 1, justifyContent: 'center', alignItems: 'center'}}>
                <Text style={{textAlign: 'center'}}>
                    {this.state.MsgDialog}
                </Text>
            </View>
            </PopupDialog>
        )
    }

    //============= Component Action =============//
    onClosed() {
        this.props
            .navigation
            .dispatch(NavigationActions.reset(
                {
                    index: 0,
                    actions: [
                        NavigationActions.navigate({routeName: 'UserDetailScreen'})
                    ]
                }));
    }

    onFinish() {
        if (this.state.finished) {
            this.setState({
                lastPathCamera: null,
                pdName: null,
                pdDesc: null,
                price: null,
                priceSpecial: null,
                isSale: true,
                saleDate: null,
                saleDateEnd: null,
                imgURL: null,
                createDate: null,
                finished: false,
            })
            this.onClosed()
        }
    }

    render() {
        console.log('stID', this.state.stID)
        console.log('userID', this.state.userID)
        let renderCam = this.state.lastPathCamera ? <Card
                width={Layout.window.width}
                image={{uri: this.state.lastPathCamera}}
                imageStyle={{height: this.state.imgSize}}
                containerStyle={{marginBottom: 10, padding: 0, margin: 0}}/>
            :
            null
        const circleButton = Layout.window.width * 0.15;

        const data = this.state.categoryList

        for (let x = 0; x < data.length; x++) {
            data[x]['key'] = data[x].cateID ? data[x].cateID : data[x].key
            data[x]['label'] = data[x].cateName ? data[x].cateName : data[x].label

            delete data[x].cateID
            delete data[x].cateName
        }

        return (
            <View>
                {this.alertDialog()}
                <DateTimePicker
                    date={this.state.saleDate ? this.state.saleDate : new Date()}
                    isVisible={this.state.isSaleDatePickerVisible}
                    onConfirm={(date) => this._handleSaleDate(date)}
                    onCancel={this._hideDateTimePicker}
                />
                <DateTimePicker
                    date={this.state.saleDateEnd ? this.state.saleDateEnd : new Date()}
                    isVisible={this.state.isSaleDateEndPickerVisible}
                    onConfirm={(date) => this._handleSaleDateEnd(date)}
                    onCancel={this._hideDateTimePicker}
                />
                <ScrollView>
                    <View style={styles.container}>
                        <View style={{alignItems: 'center'}}>
                            <View style={{alignItems: 'center', backgroundColor: '#FFF'}}>
                                <ListItem
                                    titleStyle={{
                                        fontSize: 25,
                                        color: '#000'
                                    }}
                                    titleContainerStyle={{
                                        flexDirection: 'column',
                                        justifyContent: 'flex-start',
                                        alignItems: 'flex-start',
                                        padding: 10
                                    }}
                                    subtitleContainerStyle={{
                                        flexDirection: 'column',
                                        justifyContent: 'flex-start',
                                        alignItems: 'flex-start',
                                        padding: 10
                                    }}
                                    avatarContainerStyle={{
                                        flex: 1,
                                        flexDirection: 'row',
                                        justifyContent: 'center',
                                        alignItems: 'center'
                                    }}

                                    containerStyle={{
                                        justifyContent: 'center',
                                        alignItems: 'center'
                                    }}
                                    width={Layout.window.width}
                                    height={Layout.window.height * 0.20}

                                    title={this.state.displayName}
                                    subtitle={this.state.accessControl}
                                    hideChevron={true}
                                    avatar={<Avatar
                                        width={Layout.window.height * 0.18}
                                        rounded
                                        icon={{
                                            name: 'user',
                                            type: 'font-awesome',
                                            color: 'white',
                                            size: (Layout.window.height * 0.15)
                                        }}
                                        overlayContainerStyle={{backgroundColor: 'orange'}}
                                        onPress={() => console.log("Thumbs Up!")}
                                        activeOpacity={0.7}
                                    />}/>
                            </View>
                            <Card
                                width={Layout.window.width}
                                containerStyle={{marginBottom: 10, marginTop: 0, padding: 0}}>
                                <TextInput
                                    style={[styles.textInputForm, {marginTop: 15}]}
                                    placeholder="ชื่อสินค้า"
                                    value={this.state.pdName}
                                    onChangeText={(pdName) => this.setState({pdName})}
                                />
                                <TextInput
                                    style={styles.textInputForm}
                                    placeholder="ราคาปกติ"
                                    value={this.state.price}
                                    onChangeText={(price) => this.setState({price})}
                                />
                                <TextInput
                                    style={styles.textInputForm}
                                    placeholder="ราคาพิเศษ(ระบุหรือไม่ก็ได้)"
                                    value={this.state.priceSpecial}
                                    onChangeText={(priceSpecial) => this.setState({priceSpecial})}
                                />
                                <TouchableOpacity onPress={() => this._showSaleDatePicker()}>
                                    <TextInput editable={false} style={styles.textInputForm}>
                                        {'เริ่มลดราคา : ' + (this.state.saleDate ? (this.state.saleDate).toDateString() : 'ระบุวันที่')}
                                    </TextInput>
                                </TouchableOpacity>
                                <TouchableOpacity onPress={() => this._showSaleDateEndPicker()}>
                                    <TextInput editable={false} style={[styles.textInputForm]}>
                                        {'ลดราคาถึงวันที่ : ' + (this.state.saleDateEnd ? (this.state.saleDateEnd).toDateString() : 'ระบุวันที่')}
                                    </TextInput>
                                </TouchableOpacity>
                                <ModalPicker
                                    data={data}
                                    initValue="Select something yummy!"
                                    onChange={(option) => {
                                        this.setState({
                                            categoryText: option.label,
                                            categoryID: option.key
                                        })
                                    }}>

                                    <TextInput editable={false} style={[styles.textInputForm]}
                                               placeholder="ประเภทอาหาร"
                                               value={this.state.categoryText}/>

                                </ModalPicker>
                                <TextInput
                                    style={styles.textInputForm}
                                    placeholder="คำอธิบาย(ระบุหรือไม่ก็ได้)"
                                    value={this.state.pdDesc}
                                    onChangeText={(pdDesc) => this.setState({pdDesc})}
                                />
                            </Card>
                            {renderCam}
                            <View style={{flexDirection: 'row'}}>
                                <Button
                                    buttonStyle={{
                                        width: circleButton,
                                        height: circleButton,
                                        borderRadius: circleButton,
                                        backgroundColor: '#00B013',
                                        marginBottom: 10
                                    }}
                                    icon={{
                                        name: 'add-a-photo',
                                        size: circleButton * 0.4,
                                        style: {marginRight: 0}
                                    }}
                                    onPress={() => this.props.navigation.navigate('CameraScreen', {...this.state})}/>
                                <Button
                                    disabled={false}
                                    buttonStyle={{
                                        width: circleButton,
                                        height: circleButton,
                                        borderRadius: circleButton,
                                        backgroundColor: '#03A9F4',
                                        marginBottom: 10
                                    }}
                                    icon={{
                                        name: 'share-variant',
                                        size: circleButton * 0.4,
                                        type: 'material-community',
                                        style: {marginRight: 0}
                                    }}
                                    onPress={() => this.uploadProduct()}/>
                                <Button
                                    buttonStyle={{
                                        width: circleButton,
                                        height: circleButton,
                                        borderRadius: circleButton,
                                        backgroundColor: '#B01B00',
                                        marginBottom: 10
                                    }}
                                    icon={{
                                        name: 'close',
                                        size: circleButton * 0.4,
                                        type: 'material-community',
                                        style: {marginRight: 0}
                                    }}
                                    onPress={() => this.onClosed()}/>
                            </View>
                        </View>
                    </View>
                </ScrollView>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'flex-start',
        alignItems: 'center',
        backgroundColor: '#E7E7E7',
    },
    horizontal: {
        flex: 0.1,
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
    },
    content: {
        flex: 0.9,
        flexDirection: 'row'
    },
    welcome: {
        fontSize: 20,
        textAlign: 'center',
        margin: 10,
    },
    instructions: {
        textAlign: 'center',
        color: '#333333',
        marginBottom: 5,
    },
    textInputForm: {
        color: 'gray',
        marginLeft: 15,
        marginBottom: 5,
        height: 42,
        width: Layout.window.width
    },
});
