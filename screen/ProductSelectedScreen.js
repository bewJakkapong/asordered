/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, {Component} from 'react';
import {
    Platform,
    StyleSheet,
    Text,
    View,
    TouchableOpacity,
    ScrollView,
    AsyncStorage
} from 'react-native';
import {Card, ListItem, Button, Avatar, SearchBar, List} from 'react-native-elements'
import StarRating from 'react-native-star-rating';
import Layout from '../constant/Layout'
import FontAwesome, {Icons} from 'react-native-fontawesome';
import {hostIP, port, getMyOrder, getProductReview, getUserContacts} from "../common/AuthenVariable"
import PopupDialog, {DialogTitle, DialogButton} from 'react-native-popup-dialog';

export default class ProductSelectedScreen extends Component<> {
    static navigationOptions = ({navigation, screenProps}) => ({
        title: navigation.state.params.product.owner + ' - ' + navigation.state.params.product.productName,
    });

    constructor(props) {
        super(props);
        const param = props.navigation.state.params
        this.state = {
            ...props.navigation.state.params,
            starCount: param.product.starCount,
            isFavorite: false,
            reviewList: [],
            reviewContent: '',
            reviewer:''
        };
    }

    FavoriteUpdate() {
        if (this.state.isFavorite === Icons.heartO) {
            this.setState({
                isFavorite: Icons.heart
            });
        } else {
            this.setState({
                isFavorite: Icons.heartO
            });
        }
    }

    _getReviewList = async () => {
        fetch(hostIP + port + getProductReview + '/' + this.state.product.pdID, {
            method: 'GET',
        })
            .then((response) => response.json())
            .then((responseJson) => {
                if (responseJson.status === 'error') {
                } else if (responseJson.status === 'success') {
                    this.setState({
                        reviewList: responseJson.data
                    })
                }
            })
            .catch((error) => {
                console.error(error)
            });
    }

    _loadUser = async () => {
        AsyncStorage.getItem('currentUser', (err, result) => {
            const data = JSON.parse(result)
            if (data !== null) {
                this.setState({
                    loggedIn: true
                })
            } else {
                this.setState({
                    loggedIn: false
                })
            }
        });
    }

    _addProductPayment() {
        if (this.state.loggedIn) {
            this.props.navigation.navigate('PaymentNavigation', {...this.state})
        } else {
            this.props.navigation.navigate('AuthenScreen', {
                product: this.state.product,
                currentPage: 'PaymentNavigation'
            })
        }
    }

    favoriteChange() {
        AsyncStorage.getItem('favorite', (err, result) => {
            if (result) {
                let item = JSON.parse(result)
                let existIndex = item.favorite.findIndex((id) => this.state.product.pdID === id)

                console.log('EXIST', existIndex)
                if (existIndex >= 0) {
                    item.favorite.splice(existIndex, 1)

                    let newItem = JSON.stringify(item)
                    AsyncStorage.setItem('favorite', newItem, () => {
                        this.checkFavorite().done()
                    })
                } else if (existIndex < 0) {
                    item.favorite.push(this.state.product.pdID)

                    let newItem = JSON.stringify(item)
                    AsyncStorage.setItem('favorite', newItem, () => {
                        this.checkFavorite().done()
                    })
                }
            } else {
                let item = JSON.stringify({
                    favorite: [this.state.product.pdID],
                })
                AsyncStorage.setItem('favorite', item, () => {
                    this.checkFavorite().done()
                })
            }
        })
    }

    renderDate(time) {
        return (
            new Date(time).toDateString()
        )
    }

    checkFavorite = async () => {
        AsyncStorage.getItem('favorite', (err, result) => {
            console.log('FAVORITE', result)
            if (result) {
                let item = JSON.parse(result)
                let existIndex = item.favorite.findIndex((id) => this.state.product.pdID === id)
                if (existIndex >= 0) {
                    this.setState({isFavorite: true})
                } else if (existIndex < 0) {
                    this.setState({isFavorite: false})
                }
            } else {
                this.setState({isFavorite: false})
            }
        })
    }

    componentDidMount() {
        this._loadUser().done()
        this.checkFavorite().done()
        this._getReviewList().done()
        console.log('productSelected', this.state)
    }

    onStarRatingPress(rating) {
        this.setState({
            starCount: rating
        })
    }

    showPopupDialog(reviewContent, reviewer) {
        this.setState({
            reviewContent: reviewContent,
            reviewer: reviewer,
        })
        this.popupDialog.show()
    }

    contentDialog() {
        return (
            <PopupDialog
                ref={(fadeAnimationDialog) => {
                    this.popupDialog = fadeAnimationDialog;
                }}
                dialogTitle={<DialogTitle title={this.state.reviewer}/>}
                width={Layout.window.width * 0.9}
                actions={[
                    <DialogButton
                        buttonStyle={{flex: 1}}
                        text="ตกลง"
                        onPress={() => {
                            this.popupDialog.dismiss();
                        }}
                        key="button-dismiss"
                    />
                ]}
            ><View style={{flex: 1, justifyContent: 'center', alignItems: 'center'}}>
                <Text style={{textAlign: 'center'}}>
                    {this.state.reviewContent}
                </Text>
            </View>
            </PopupDialog>
        )
    }

    render() {
        let isFavorite = this.state.isFavorite ? Icons.heart : Icons.heartO
        console.log(this.state.reviewList)
        return (
            <View>
                {this.contentDialog()}
                <ScrollView>
                    <View style={styles.container}>
                        <View style={{alignItems: 'center'}}>
                            <TouchableOpacity style={{backgroundColor: '#FFF'}}>
                                <ListItem
                                    titleStyle={{
                                        fontSize: 25,
                                        color: '#000'
                                    }}
                                    titleContainerStyle={{
                                        flexDirection: 'column',
                                        justifyContent: 'flex-start',
                                        alignItems: 'flex-start',
                                        padding: 10
                                    }}
                                    avatarContainerStyle={{
                                        flex: 1,
                                        flexDirection: 'row',
                                        justifyContent: 'center',
                                        alignItems: 'center'
                                    }}

                                    containerStyle={{
                                        justifyContent: 'center',
                                        alignItems: 'center'
                                    }}
                                    width={Layout.window.width}
                                    height={Layout.window.height * 0.12}

                                    title={this.state.product.stName}
                                    hideChevron={true}
                                    avatar={<Avatar
                                        width={Layout.window.height * 0.10}
                                        rounded
                                        icon={{
                                            name: 'user',
                                            type: 'font-awesome',
                                            color: 'white',
                                            size: (Layout.window.height * 0.08)
                                        }}
                                        overlayContainerStyle={{backgroundColor: 'orange'}}
                                        onPress={() => console.log("Thumbs Up!")}
                                        activeOpacity={0.7}
                                    />}/>
                            </TouchableOpacity>
                            <Card
                                title={this.state.product.pdName}
                                titleStyle={{fontSize: 24}}
                                width={Layout.window.width}
                                image={{uri: this.state.product.imgURL}}
                                imageStyle={{height: 300}}>
                                <Text style={{marginBottom: 10}}>
                                    {this.state.product.pdDesc}
                                </Text>
                                <View style={{
                                    flexDirection: 'row',
                                    justifyContent: 'space-between',
                                    alignItems: 'center'
                                }}>
                                    <StarRating
                                        disabled={true}
                                        starSize={22}
                                        starColor='#FFAA00'
                                        emptyStarColor='#FFAA00'
                                        maxStars={5}
                                        rating={this.state.product.rating ? this.state.product.rating : 0}
                                        selectedStar={(rating) => this.onStarRatingPress(rating)}
                                    />
                                    <TouchableOpacity
                                        onPress={() => this.favoriteChange()}>
                                        <FontAwesome style={{fontSize: 40, color: 'red'}}>
                                            {isFavorite}
                                        </FontAwesome>
                                    </TouchableOpacity>
                                    <Button
                                        icon={{name: 'shopping-cart'}}
                                        backgroundColor='#03A9F4'
                                        buttonStyle={{
                                            borderRadius: 3,
                                            marginLeft: 0,
                                            marginRight: 0,
                                            marginBottom: 0,
                                            width: (Layout.window.width * 0.3)
                                        }}
                                        title={this.state.product.price + ' ฿'}
                                        onPress={() => this._addProductPayment()}/>
                                </View>
                            </Card>
                            <Button
                                buttonStyle={{
                                    width: Layout.window.width * 0.8,
                                    borderRadius: 5,
                                    borderWidth: 2,
                                    borderColor: '#00B013',
                                    backgroundColor: '#FFF',
                                    marginBottom: 10,
                                    marginTop: 10,
                                }}
                                title='เขียนรีวิว'
                                textStyle={{color: '#000'}}
                                onPress={() => this.props.navigation.navigate('CommentScreen', {...this.state})}/>

                            {
                                this.state.reviewList.map((rev, i) => {
                                    return (
                                        <ListItem
                                            key={i}
                                            containerStyle={{
                                                justifyContent: 'center',
                                                alignItems: 'center',
                                                backgroundColor: '#FFF'
                                            }}
                                            width={Layout.window.width}
                                            height={Layout.window.height * 0.12}

                                            label={<StarRating
                                                disabled={true}
                                                starSize={22}
                                                starColor='#FFAA00'
                                                emptyStarColor='#FFAA00'
                                                maxStars={5}
                                                rating={rev.rating}
                                                selectedStar={(rating) => this.onStarRatingPress(rating)}
                                            />}
                                            subtitle={this.renderDate(rev.commentDate)}
                                            title={rev.displayName + ' ' + rev.commentContent}
                                            hideChevron={true}
                                            avatar={<Avatar
                                                width={Layout.window.height * 0.10}
                                                rounded
                                                icon={{
                                                    name: 'user',
                                                    type: 'font-awesome',
                                                    color: 'white',
                                                    size: (Layout.window.height * 0.05)
                                                }}
                                                small
                                                overlayContainerStyle={{backgroundColor: 'orange'}}
                                                onPress={() => console.log("Thumbs Up!")}
                                                activeOpacity={0.7}
                                            />}
                                            onPress={() => this.showPopupDialog(rev.commentContent, rev.displayName)}
                                        />
                                    )
                                })
                            }
                        </View>
                    </View>
                </ScrollView>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'flex-start',
        alignItems: 'center',
        backgroundColor: '#E7E7E7',
    },
    welcome: {
        fontSize: 28,
        textAlign: 'center',
        margin: 10,
    },
    instructions: {
        textAlign: 'center',
        color: '#333333',
        marginBottom: 5,
    },
});
