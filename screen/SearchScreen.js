/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, {Component} from 'react';
import {
    ScrollView,
    StyleSheet,
    Text,
    View
} from 'react-native';
import {SearchBar, Card, ListItem, Button, Avatar} from 'react-native-elements'
import {productData} from '../constant/data'
import Layout from '../constant/Layout'
import StarRating from 'react-native-star-rating';
import {hostIP, port, getSearchProduct} from '../common/AuthenVariable'

export default class SearchScreen extends Component<> {
    constructor(props) {
        super(props)
        const star = [];
        productData.map((pd) => {
            star.push(pd.starCount)
        })
        this.state = {
            starCount: star,
            data: []
        };
    }

    componentDidMount() {
        this.searchQuery(' ')
    }

    searchFilter(seachTxt) {
        const data = productData.filter((data) => {
            if (seachTxt !== undefined) {
                seachTxt.toLowerCase()
            }
            if (data.owner.toLowerCase().search(seachTxt) > -1 ||
                data.productName.toLowerCase().search(seachTxt) > -1) {
                return data
            }
        })
        this.setState({data: data})
    }

    searchQuery(searchStr) {
        const convertTxt = searchStr.replace(/\s/g, '');
        const finalTxt = convertTxt.length > 0 ? convertTxt : ' all'
        fetch(hostIP + port + getSearchProduct + '/' + finalTxt, {
            method: 'GET',
            headers: {
                'Content-Type': 'application/json',
            },
        })
            .then((response) => response.json())
            .then((res) => {
                this.setState({data: res.data})
                console.log('searching')
            })
            .catch((error) => {
                console.error(error);
            });
    }

    render() {
        return (
            <View style={styles.container}>
                <SearchBar width={Layout.window.width}
                           placeholder='Search...'
                           lightTheme
                           round
                           ref={(search) => {
                               this.search = search
                           }}
                           onChangeText={(txt) => this.searchQuery(txt)}/>
                <ScrollView>
                    <View style={{alignItems: 'center', justifyContent: 'center'}}>
                        <Card containerStyle={{padding: 0, flex: 1, margin: 0}}>
                            {
                                this.state.data.map((pd, i) => {
                                    return (
                                        <ListItem
                                            titleStyle={{
                                                fontSize: 20,
                                                color: '#000'
                                            }}
                                            titleContainerStyle={{
                                                flex: 0.3,
                                                flexDirection: 'column',
                                                justifyContent: 'center',
                                                alignItems: 'center',
                                            }}
                                            subtitleContainerStyle={{
                                                flex: 0.7,
                                                flexDirection: 'column',
                                                justifyContent: 'flex-start',
                                                alignItems: 'flex-start'
                                            }}
                                            avatarContainerStyle={{
                                                flex: 1,
                                                flexDirection: 'column',
                                                justifyContent: 'flex-end',
                                                alignItems: 'flex-end'
                                            }}

                                            width={Layout.window.width}
                                            height={120}
                                            key={i}
                                            hideChevron={true}

                                            title={pd.pdName}

                                            subtitle={<View style={styles.listContainerColumn}>
                                                <View style={styles.listContainer}>

                                                </View>
                                                <View style={{marginLeft: 5}}>
                                                    <Button
                                                        icon={{name: 'shopping-cart'}}
                                                        backgroundColor={'#03A9F4'}
                                                        title={pd.price + ' ฿'}
                                                        buttonStyle={styles.buttonPrice}
                                                        onPress={() => {
                                                            this.props.navigation.navigate('ProductSelected', {product: pd})
                                                        }}/>
                                                </View>
                                            </View>}

                                            avatar={<View style={styles.listContainerColumn}>
                                                <View style={styles.listContainer}>
                                                    <Avatar
                                                        small
                                                        rounded={true}
                                                        source={{uri: 'https://lh3.googleusercontent.com/ez8pDFoxU2ZqDmyfeIjIba6dWisd8MY_6choHhZNpO0WwLhICu0v0s5eV2WHOhuhKw=w170'}}
                                                        activeOpacity={0.7}
                                                    />
                                                    <Text style={styles.listText}>{pd.stName}</Text>
                                                </View>
                                                <View style={{marginLeft: 5}}><StarRating
                                                    disabled={true}
                                                    starSize={22}
                                                    starColor='#FFAA00'
                                                    emptyStarColor='#FFAA00'
                                                    maxStars={5}
                                                    rating={pd.rating}
                                                /></View>
                                            </View>}

                                            leftIcon={<Avatar
                                                height={100}
                                                width={130}
                                                source={{uri: pd.imgURL}}
                                                onPress={() => console.log("Works!")}
                                                activeOpacity={0.7}
                                            />}
                                        />
                                    );
                                })
                            }
                        </Card>
                    </View>
                </ScrollView>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: '#E7E7E7',
    },
    welcome: {
        fontSize: 20,
        textAlign: 'center',
        margin: 10,
    },
    instructions: {
        textAlign: 'center',
        color: '#333333',
        marginBottom: 5,
    },
    searchBar: {
        flex: 1,
    },
    listContainer: {
        flex: 1,
        flexDirection: 'row',
        padding: 5,
        paddingTop: 5
    },
    listContainerColumn: {
        flex: 1,
        flexDirection: 'column',
    },
    listText: {
        flex: 1,
        flexDirection: 'column',
        justifyContent: 'center',
        alignItems: 'center',
        marginLeft: 5,
        color: '#000',
        fontSize: 16
    },
    buttonPrice: {
        width: Layout.window.width / 4,
        borderRadius: 3,
        justifyContent: 'center',
        alignItems: 'center',
    },
});
