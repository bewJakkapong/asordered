/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, {Component} from 'react';
import {
    Platform,
    StyleSheet,
    Text,
    View
} from 'react-native';
import MapView from 'react-native-maps'
import Layout from "../../constant/Layout";

function randomColor() {
    return `#${Math.floor(Math.random() * 16777215).toString(16)}`;
}

export default class NearbyCateScreen extends Component<> {
    static navigationOptions = ({navigation}) => ({
        title: navigation.state.params.name
    });

    constructor(props) {
        super(props);

        this.state = {
            region: {
                latitude: 14.987966,
                longitude: 102.117760,
                latitudeDelta: 0.0922,
                longitudeDelta: 0.0421,
            },
            coordinate: {
                latitude: 14.987966,
                longitude: 102.117760,
            },
        };
    }

    onMapPress(e) {
        this.setState({
            markers: [
                ...this.state.markers,
                {
                    coordinate: e.nativeEvent.coordinate,
                    key: id++,
                    color: randomColor(),
                },
            ],
        });
    }

    render() {
        return (
            <View style={styles.container}>
                <MapView
                    provider={this.props.provider}
                    style={{flex: 1, backgroundColor: 'red', width: Layout.window.width, height: Layout.window.height}}
                    initialRegion={this.state.region}
                >
                    <MapView.Marker coordinate={this.state.coordinate}>

                    </MapView.Marker>
                </MapView>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#F5FCFF',
    },
    welcome: {
        fontSize: 20,
        textAlign: 'center',
        margin: 10,
    },
    instructions: {
        textAlign: 'center',
        color: '#333333',
        marginBottom: 5,
    },
});
