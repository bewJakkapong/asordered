/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, {Component} from 'react';
import {
    Platform,
    StyleSheet,
    Text,
    TouchableOpacity
} from 'react-native';
import {List, ListItem} from 'react-native-elements'
import {categoryList} from '../constant/data'
import {hostIP, port, getCategoryList} from '../common/AuthenVariable'

export default class CategoryScreen extends Component<> {

    static navigationOptions = {
        title: 'Category',
        headerTitleStyle: {
            alignSelf: 'center'
        },
        headerLeft: null
    };

    constructor(props) {
        super(props)
        this.state = {
            categoryList: []
        }
    }

    componentWillMount(){
        this._getCategoryList().done()
    }

    _getCategoryList = async () => {
        fetch(hostIP + port + getCategoryList, {
            method: 'GET',
        })
            .then((response) => response.json())
            .then((responseJson) => {
                if (responseJson.status === 'error') {
                } else if (responseJson.status === 'success') {
                    this.setState({
                        categoryList: responseJson.data
                    })
                }
            })
            .catch((error) => {
                console.error(error)
            });
    }

    render() {
        return (
            <List containerStyle={{marginBottom: 20}}>
                {
                    this.state.categoryList.map((list, i) => (
                        <TouchableOpacity
                            key={list.cateID}
                            onPress={() => {
                                this.props.navigation.navigate('CategorySubScreen', {product: list})
                            }}>
                            <ListItem
                                height={60}
                                roundAvatar
                                avatar={{uri: list.cateImgURL}}
                                title={list.cateName}
                            />
                        </TouchableOpacity>
                    ))
                }
            </List>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#F5FCFF',
    },
    welcome: {
        fontSize: 20,
        textAlign: 'center',
        margin: 10,
    },
    instructions: {
        textAlign: 'center',
        color: '#333333',
        marginBottom: 5,
    },
});
