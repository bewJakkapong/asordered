/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, {Component} from 'react';
import {
    Platform,
    StyleSheet,
    Text,
    View,
    ScrollView,
    AsyncStorage
} from 'react-native';
import {Card, ListItem, Button, Avatar, SearchBar} from 'react-native-elements'
import Layout from '../constant/Layout'
import StarRating from 'react-native-star-rating';
import Swiper from 'react-native-swiper';
import {hostIP, port, getProductList} from '../common/AuthenVariable'
import {productData} from '../constant/data'

export default class HomeScreen extends Component<> {

    static navigationOptions = {
        title: 'As Ordered',
        headerTitleStyle: {
            alignSelf: 'center'
        }
    };

    componentWillMount() {
        this._getProductList().done()
    }

    componentDidMount() {
        setInterval(() => {
            this._getProductList().done()
        }, 2000);

        AsyncStorage.getItem('currentUser', (err, result) => {
            const data = JSON.parse(result)
            if (data !== null) {
                this.setState({
                    currentUser: data,
                })
            } else {

            }
        });
    }

    constructor(props) {
        super(props);
        // const star = [];
        // productData.map((pd) => {
        //     star.push(pd.starCount)
        // })
        this.state = {
            starCount: 5,
            productList: []
        };
    }

    _getProductList = async () => {
        fetch(hostIP + port + getProductList, {
            method: 'GET',
        })
            .then((response) => response.json())
            .then((responseJson) => {
                if (responseJson.status === 'error') {
                } else if (responseJson.status === 'success') {
                    this.setState({
                        productList: responseJson.data
                    })
                }
            })
            .catch((error) => {
                console.error(error)
            });
    }

    onStarRatingPress(rating, i) {
        let starChange = this.state.starCount;
        starChange[i] = rating
        this.setState({
            starCount: starChange
        });
    }

    _renderScene = (Scene, Param) => {
        this.props.navigation.navigate(Scene, Param)
    }

    render() {
        let quarterWidth = Layout.window.width * 0.22
        const productList = this.state.productList
        return (
            <ScrollView>
                <View style={{
                    flex: 1,
                    flexDirection: 'row',
                    alignItems: 'center',
                    justifyContent: 'space-between',
                    padding: 5
                }}>
                    <Avatar
                        width={quarterWidth}
                        height={quarterWidth}
                        rounded
                        icon={{name: 'thumbs-up', type: 'font-awesome', color: 'white'}}
                        overlayContainerStyle={{backgroundColor: 'orange'}}
                        onPress={() => this._renderScene('PopularCateScreen', {name: 'POPULAR'})}
                        activeOpacity={0.7}
                    />
                    <Avatar
                        width={quarterWidth}
                        height={quarterWidth}
                        rounded
                        icon={{name: 'star', type: 'font-awesome', color: 'yellow'}}
                        overlayContainerStyle={{backgroundColor: '#40AF00'}}
                        onPress={() => this._renderScene('TopRateCateScreen', {name: 'TOP RATE'})}
                        activeOpacity={0.7}
                    />
                    <Avatar
                        width={quarterWidth}
                        height={quarterWidth}
                        rounded
                        icon={{name: 'tags', type: 'font-awesome', color: 'white'}}
                        overlayContainerStyle={{backgroundColor: '#0087AF'}}
                        onPress={() => this._renderScene('PriceCateScreen', {name: 'PRICE'})}
                        activeOpacity={0.7}
                    />
                    <Avatar
                        width={quarterWidth}
                        height={quarterWidth}
                        rounded
                        icon={{name: 'map-marker', type: 'font-awesome', color: 'white'}}
                        overlayContainerStyle={{backgroundColor: '#9F06D8'}}
                        onPress={() => this._renderScene('NearbyCateScreen', {name: 'NEARBY'})}
                        activeOpacity={0.7}
                    />
                </View>

                <View style={{height: (Layout.window.height * 0.2)}}>
                    <Swiper style={styles.wrapper} autoplay>
                        <View style={styles.slide1}>
                            <Text style={styles.text}>We are As Ordered</Text>
                        </View>
                        <View style={styles.slide2}>
                            <Text style={styles.text}>Fastest</Text>
                        </View>
                        <View style={styles.slide3}>
                            <Text style={styles.text}>And simple</Text>
                        </View>
                    </Swiper>
                </View>

                <View style={{alignItems: 'center'}}>
                    <Card containerStyle={{padding: 0, marginTop: 0, flex: 1}}>
                        {
                            productList.map((pd, i) => {
                                return (
                                    <ListItem
                                        titleStyle={{
                                            fontSize: 20,
                                            color: '#000'
                                        }}
                                        titleContainerStyle={{
                                            flex: 0.3,
                                            flexDirection: 'column',
                                            justifyContent: 'center',
                                            alignItems: 'center',
                                        }}
                                        subtitleContainerStyle={{
                                            flex: 0.7,
                                            flexDirection: 'column',
                                            justifyContent: 'flex-start',
                                            alignItems: 'flex-start'
                                        }}
                                        avatarContainerStyle={{
                                            flex: 1,
                                            flexDirection: 'column',
                                            justifyContent: 'flex-end',
                                            alignItems: 'flex-end'
                                        }}

                                        width={Layout.window.width}
                                        height={120}
                                        key={i}
                                        hideChevron={true}

                                        title={pd.pdName}

                                        subtitle={<View style={styles.listContainerColumn}>
                                            <View style={styles.listContainer}>

                                            </View>
                                            <View style={{marginLeft: 5}}>
                                                <Button
                                                    icon={{name: 'shopping-cart'}}
                                                    backgroundColor={'#03A9F4'}
                                                    title={pd.price + ' ฿'}
                                                    buttonStyle={styles.buttonPrice}
                                                    onPress={() => {
                                                        this.props.navigation.navigate('ProductSelected', {product: pd, currentUser:this.state.currentUser})
                                                    }}/>
                                            </View>
                                        </View>}

                                        avatar={<View style={styles.listContainerColumn}>
                                            <View style={styles.listContainer}>
                                                <Avatar
                                                    small
                                                    rounded={true}
                                                    source={{uri: 'https://lh3.googleusercontent.com/ez8pDFoxU2ZqDmyfeIjIba6dWisd8MY_6choHhZNpO0WwLhICu0v0s5eV2WHOhuhKw=w170'}}
                                                    activeOpacity={0.7}
                                                />
                                                <Text style={styles.listText}>{pd.stName}</Text>
                                            </View>
                                            <View style={{marginLeft: 5}}>
                                                <StarRating
                                                    disabled={true}
                                                    starSize={22}
                                                    starColor='#FFAA00'
                                                    emptyStarColor='#FFAA00'
                                                    maxStars={5}
                                                    // rating={this.state.starCount[i]}
                                                    rating={pd.rating ? pd.rating : 0}
                                                    selectedStar={(rating) => this.onStarRatingPress(rating, i)}
                                                /></View>
                                        </View>}

                                        leftIcon={<Avatar
                                            height={100}
                                            width={130}
                                            source={{uri: pd.imgURL}}
                                            onPress={() => console.log("Works!")}
                                            activeOpacity={0.7}
                                        />}
                                    />
                                );
                            })
                        }
                    </Card>
                </View>

            </ScrollView>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#E7E7E7',
    },
    listContainer: {
        flex: 1,
        flexDirection: 'row',
        padding: 5,
        paddingTop: 5
    },
    listContainerColumn: {
        flex: 1,
        flexDirection: 'column',
    },
    listText: {
        flex: 1,
        flexDirection: 'column',
        justifyContent: 'center',
        alignItems: 'center',
        marginLeft: 5,
        color: '#000',
        fontSize: 16
    },
    buttonPrice: {
        width: Layout.window.width / 4,
        borderRadius: 3,
        justifyContent: 'center',
        alignItems: 'center',
    },
    wrapper: {},
    slide1: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#9DD6EB',
    },
    slide2: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#97CAE5',
    },
    slide3: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#92BBD9',
    },
    text: {
        color: '#fff',
        fontSize: 30,
        fontWeight: 'bold',
    }
});
