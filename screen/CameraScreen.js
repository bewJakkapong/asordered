import React, {Component} from 'react';
import {
    AppRegistry,
    Dimensions,
    StyleSheet,
    Text,
    TouchableOpacity,
    View
} from 'react-native';
import FontAwesome, {Icons} from 'react-native-fontawesome';
import Camera from 'react-native-camera';

export default class CameraScreen extends Component {

    constructor(props) {
        super(props)
        this.state = props.navigation.state.params
    }

    componentDidMount() {

    }

    render() {
        return (
            <View style={styles.container}>
                <Camera
                    ref={(cam) => {
                        this.camera = cam;
                    }}
                    style={styles.preview}
                    aspect={Camera.constants.Aspect.fill}>
                    <TouchableOpacity style={styles.capture} onPress={this.takePicture.bind(this)}>
                        <FontAwesome style={{fontSize: 28, color: '#FFF'}}>{Icons.camera}</FontAwesome>
                    </TouchableOpacity>
                </Camera>
            </View>
        );
    }

    takePicture() {
        const options = {};
        this.camera.capture({metadata: options})
            .then((data) => {
                const dataPath = data.path
                console.log('DataPath ', dataPath.replace(/^.*[\\\/]/, ''))

                this.props.navigation.navigate('AddProductScreen', {...this.state, lastPathCamera: data.path})
            })
            .catch(err => console.error(err));

    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        flexDirection: 'row',
    },
    preview: {
        flex: 1,
        justifyContent: 'flex-end',
        alignItems: 'center'
    },
    capture: {
        flex: 0,
        padding: 10,
        margin: 40
    }
});