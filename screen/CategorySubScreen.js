/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, {Component} from 'react';
import {
    Text,
    StyleSheet,
    View,
    ScrollView
} from 'react-native';
import {Card, ListItem, Button, Avatar, SearchBar} from 'react-native-elements'
import StarRating from 'react-native-star-rating';
import Layout from '../constant/Layout'
import {hostIP, port, getProductListCate} from '../common/AuthenVariable'

export default class CategorySubScreen extends Component<> {

    static navigationOptions = ({ navigation }) => ({
        title: `${navigation.state.params.product.cateName}`,
        headerTitleStyle: {
            alignSelf: 'center'
        },
        headerRight: <View/>
    });

    constructor(props) {
        super(props)
        this.state = {
            ...props.navigation.state.params,
            categoryList: [],
            productList: [],
        }
    }

    componentWillMount(){
        console.log(this.state)
        this._getCategoryList().done()
    }

    _getCategoryList = async () => {
        fetch(hostIP + port + getProductListCate + '/' + this.props.navigation.state.params.product.cateID, {
            method: 'GET',
        })
            .then((response) => response.json())
            .then((responseJson) => {
                if (responseJson.status === 'error') {
                } else if (responseJson.status === 'success') {
                    this.setState({
                        productList: responseJson.data
                    })
                }
            })
            .catch((error) => {
                console.error(error)
            });
    }

    render() {
        console.log(this.state.productList)
        return (
            <ScrollView>
                <View style={{alignItems: 'center'}}>
                    <Card containerStyle={{padding: 0, marginTop: 0, flex: 1}}>
                        {
                            this.state.productList.map((pd, i) => {
                                return (
                                    <ListItem
                                        titleStyle={{
                                            fontSize: 20,
                                            color: '#000'
                                        }}
                                        titleContainerStyle={{
                                            flex: 0.3,
                                            flexDirection: 'column',
                                            justifyContent: 'center',
                                            alignItems: 'center',
                                        }}
                                        subtitleContainerStyle={{
                                            flex: 0.7,
                                            flexDirection: 'column',
                                            justifyContent: 'flex-start',
                                            alignItems: 'flex-start'
                                        }}
                                        avatarContainerStyle={{
                                            flex: 1,
                                            flexDirection: 'column',
                                            justifyContent: 'flex-end',
                                            alignItems: 'flex-end'
                                        }}

                                        width={Layout.window.width}
                                        height={120}
                                        key={i}
                                        hideChevron={true}

                                        title={pd.pdName}

                                        subtitle={<View style={styles.listContainerColumn}>
                                            <View style={styles.listContainer}>

                                            </View>
                                            <View style={{marginLeft: 5}}>
                                                <Button
                                                    icon={{name: 'shopping-cart'}}
                                                    backgroundColor={'#03A9F4'}
                                                    title={pd.price + ' ฿'}
                                                    buttonStyle={styles.buttonPrice}
                                                    onPress={() => {
                                                        this.props.navigation.navigate('ProductSelected', {product: pd})
                                                    }}/>
                                            </View>
                                        </View>}

                                        avatar={<View style={styles.listContainerColumn}>
                                            <View style={styles.listContainer}>
                                                <Avatar
                                                    small
                                                    rounded={true}
                                                    source={{uri: 'https://lh3.googleusercontent.com/ez8pDFoxU2ZqDmyfeIjIba6dWisd8MY_6choHhZNpO0WwLhICu0v0s5eV2WHOhuhKw=w170'}}
                                                    activeOpacity={0.7}
                                                />
                                                <Text style={styles.listText}>{pd.stName}</Text>
                                            </View>
                                            <View style={{marginLeft: 5}}><StarRating
                                                disabled={true}
                                                starSize={22}
                                                starColor='#FFAA00'
                                                emptyStarColor='#FFAA00'
                                                maxStars={5}
                                                rating={pd.rating ? pd.rating : 0}
                                            /></View>
                                        </View>}

                                        leftIcon={<Avatar
                                            height={100}
                                            width={130}
                                            source={{uri: pd.imgURL}}
                                            onPress={() => console.log("Works!")}
                                            activeOpacity={0.7}
                                        />}
                                    />
                                );
                            })
                        }
                    </Card>
                </View>
            </ScrollView>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#F5FCFF',
    },
    welcome: {
        fontSize: 20,
        textAlign: 'center',
        margin: 10,
    },
    instructions: {
        textAlign: 'center',
        color: '#333333',
        marginBottom: 5,
    },
    listContainer: {
        flex: 1,
        flexDirection: 'row',
        padding: 5,
        paddingTop: 5
    },
    listContainerColumn: {
        flex: 1,
        flexDirection: 'column',
    },
    listText: {
        flex: 1,
        flexDirection: 'column',
        justifyContent: 'center',
        alignItems: 'center',
        marginLeft: 5,
        color: '#000',
        fontSize: 16
    },
    buttonPrice: {
        width: Layout.window.width / 4,
        borderRadius: 3,
        justifyContent: 'center',
        alignItems: 'center',
    },
});
