/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, {Component} from 'react';
import {
    Image,
    StyleSheet,
    TextInput,
    View,
    Alert
} from 'react-native';
import StarRating from 'react-native-star-rating';
import {ListItem, Avatar, List, Button, FormLabel} from 'react-native-elements'
import Layout from '../constant/Layout'
import {hostIP, port, createComment} from '../common/AuthenVariable'

const listHeight = Layout.window.height * 0.07

export default class CommentScreen extends Component<> {
    static navigationOptions = ({navigation}) => ({
        title: `${navigation.state.params.product.pdName}`,
        headerTitleStyle: {
            alignSelf: 'center'
        },
        headerRight: <View/>
    });

    constructor(props) {
        super(props)
        this.state = {
            ...props.navigation.state.params,
            starCount: 0,
            buttonCanclick: false,
            commentContent:''
        }
    }

    componentDidMount() {
        console.log(this.state)
    }

    onStarRatingPress(rating) {
        this.setState({
            starCount: rating
        })
    }

    sendReview() {
        fetch(`${hostIP}${port}${createComment}`, {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({
                pdID: this.state.product.pdID,
                userID: this.state.currentUser.userID,
                commentDate: new Date(new Date().getTime() + 7 * 3600 * 1000).toUTCString(),
                commentContent: this.state.commentContent,
                rating: this.state.starCount
            })
        })
            .then((response) => response.json())
            .then((res) => {
                if (res.ok) {
                    this.onFinish()
                } else {
                    console.log(res)
                }
            })
            .done()
    }

    onFinish() {
        Alert.alert(
            'Asordered',
            'ขอบคุณที่ร่วมแสดงความคิดเห็น',
            [
                {text: 'OK', onPress: () => this.props.navigation.goBack()},
            ],
            {cancelable: false}
        )
    }

    buttonEnable = () => {
            return (this.state.commentContent.length <= 0 || this.state.starCount <= 0)
    }

    render() {
        return (
            <View style={styles.container}>
                <Image style={{width: Layout.window.width * 0.5, height: Layout.window.width * 0.5, marginBottom: 10}}
                       source={{uri: this.state.product.imgURL}}/>
                <StarRating
                    disabled={false}
                    starSize={50}
                    starColor='#FFAA00'
                    emptyStarColor='#FFAA00'
                    maxStars={5}
                    rating={this.state.starCount}
                    selectedStar={(rating) => this.onStarRatingPress(rating)}
                />
                <FormLabel>แสดงความคิดเห็น</FormLabel>
                <View style={{
                    backgroundColor: '#FFF',
                    borderColor: '#D0D0D0',
                    borderBottomColor: '#000000',
                    borderWidth: 1,
                    alignItems: 'flex-start',
                    justifyContent: 'flex-start'
                }}
                >
                    <TextInput
                        multiline={true}
                        numberOfLines={4}
                        style={{width: Layout.window.width * 0.9}}
                        onChangeText={(commentContent) => this.setState({commentContent})}
                        value={this.state.commentContent}
                    />
                </View>
                <Button
                    buttonStyle={{
                        width: Layout.window.width * 0.8,
                        borderRadius: 5,
                        backgroundColor: '#00B013',
                        marginBottom: 10,
                        marginTop: 10,
                    }}
                    disabled={this.buttonEnable()}
                    title='ส่งรีวิว'
                    onPress={() => this.sendReview()}
                />
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        paddingTop: 20,
        justifyContent: 'flex-start',
        alignItems: 'center',
        backgroundColor: '#F5FCFF',
    },
    welcome: {
        fontSize: 20,
        textAlign: 'center',
        margin: 10,
    },
    instructions: {
        textAlign: 'center',
        color: '#333333',
        marginBottom: 5,
    },
    listText: {
        fontSize: Layout.window.width * 0.04,
        color: '#00A3DC'
    },
    textLabel: {
        fontSize: Layout.window.width * 0.04,
        color: '#000'
    },
});
