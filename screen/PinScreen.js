/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, {Component} from 'react';
import {
    Platform,
    StyleSheet,
    Text,
    View,
    ScrollView,
    AsyncStorage
} from 'react-native';
import {productData} from '../constant/data'
import {Card, ListItem, Button, Avatar, SearchBar} from 'react-native-elements'
import StarRating from 'react-native-star-rating';
import Layout from '../constant/Layout'
import {hostIP, port, getFavoriteList} from '../common/AuthenVariable'

const instructions = Platform.select({
    ios: 'Press Cmd+R to reload,\n' +
    'Cmd+D or shake for dev menu',
    android: 'Double tap R on your keyboard to reload,\n' +
    'Shake or press menu button for dev menu',
});

export default class PinScreen extends Component<> {
    static navigationOptions = {
        title: 'Your Favorite',
        headerTitleStyle: {
            alignSelf: 'center'
        },
        headerLeft: null
    };

    constructor(props) {
        super(props);
        const star = [];
        productData.map((pd) => {
            star.push(pd.starCount)
        })
        this.state = {
            starCount: star,
            data:[]
        };
    }

    onStarRatingPress(rating, i) {
        let starChange = this.state.starCount;
        starChange[i] = rating
        this.setState({
            starCount: starChange
        });
    }

    componentDidMount(){
        setInterval(() => {
            this.FavoriteList().done()
        }, 3000);
    }

    FavoriteList = async () => {
        AsyncStorage.getItem('favorite', (err, result) => {
            if (result) {
                let item = JSON.parse(result)

                fetch(hostIP + port + getFavoriteList + '/' + item.favorite, {
                    method: 'GET',
                    headers: {
                        'Content-Type': 'application/json',
                    },
                })
                    .then((response) => response.json())
                    .then((res) => {
                        this.setState({data: res.data ? res.data : []})
                    })
                    .catch((error) => {
                        console.error(error);
                    });

            } else {

            }
        })
    }

    _addProductPayment() {
        console.log('Add To Payment', this.state.userSignIn)
        if (this.state.userSignIn === 'N' || null) {
            this.props.navigation.navigate('AuthenScreen', {
                product: this.state.paramProduct,
                authenFinish: 'PaymentFirstScreen'
            })
        }
        else if (this.state.userSignIn === 'Y') {
            this.props.navigation.navigate('PaymentNavigation', {
                product: this.state.paramProduct,
            })
        }
    }

    render() {
        return (
            <ScrollView>
                <View style={{alignItems: 'center'}}>
                    <Card containerStyle={{padding: 0, marginTop: 0, flex: 1}}>
                        {
                            this.state.data.map((pd, i) => {
                                return (
                                    <ListItem
                                        titleStyle={{
                                            fontSize: 20,
                                            color: '#000'
                                        }}
                                        titleContainerStyle={{
                                            flex: 0.3,
                                            flexDirection: 'column',
                                            justifyContent: 'center',
                                            alignItems: 'center',
                                        }}
                                        subtitleContainerStyle={{
                                            flex: 0.7,
                                            flexDirection: 'column',
                                            justifyContent: 'flex-start',
                                            alignItems: 'flex-start'
                                        }}
                                        avatarContainerStyle={{
                                            flex: 1,
                                            flexDirection: 'column',
                                            justifyContent: 'flex-end',
                                            alignItems: 'flex-end'
                                        }}

                                        width={Layout.window.width}
                                        height={120}
                                        key={i}
                                        hideChevron={true}

                                        title={pd.pdName}

                                        subtitle={<View style={styles.listContainerColumn}>
                                            <View style={styles.listContainer}>

                                            </View>
                                            <View style={{marginLeft: 5}}>
                                                <Button
                                                    icon={{name: 'shopping-cart'}}
                                                    backgroundColor={'#03A9F4'}
                                                    title={pd.price + ' ฿'}
                                                    buttonStyle={styles.buttonPrice}
                                                    onPress={() => {
                                                        this.props.navigation.navigate('ProductSelected', {product: pd})
                                                    }}/>
                                            </View>
                                        </View>}

                                        avatar={<View style={styles.listContainerColumn}>
                                            <View style={styles.listContainer}>
                                                <Avatar
                                                    small
                                                    rounded={true}
                                                    source={{uri: 'https://lh3.googleusercontent.com/ez8pDFoxU2ZqDmyfeIjIba6dWisd8MY_6choHhZNpO0WwLhICu0v0s5eV2WHOhuhKw=w170'}}
                                                    activeOpacity={0.7}
                                                />
                                                <Text style={styles.listText}>{pd.stName}</Text>
                                            </View>
                                            <View style={{marginLeft: 5}}><StarRating
                                                disabled={true}
                                                starSize={22}
                                                starColor='#FFAA00'
                                                emptyStarColor='#FFAA00'
                                                maxStars={5}
                                                rating={pd.rating ? pd.rating : 0}
                                                selectedStar={(rating) => this.onStarRatingPress(rating, i)}
                                            /></View>
                                        </View>}

                                        leftIcon={<Avatar
                                            height={100}
                                            width={130}
                                            source={{uri: pd.imgURL}}
                                            onPress={() => console.log("Works!")}
                                            activeOpacity={0.7}
                                        />}
                                    />
                                );
                            })
                        }
                    </Card>
                </View>
            </ScrollView>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#F5FCFF',
    },
    welcome: {
        fontSize: 20,
        textAlign: 'center',
        margin: 10,
    },
    instructions: {
        textAlign: 'center',
        color: '#333333',
        marginBottom: 5,
    },
    listContainer: {
        flex: 1,
        flexDirection: 'row',
        padding: 5,
        paddingTop: 5
    },
    listContainerColumn: {
        flex: 1,
        flexDirection: 'column',
    },
    listText: {
        flex: 1,
        flexDirection: 'column',
        justifyContent: 'center',
        alignItems: 'center',
        marginLeft: 5,
        color: '#000',
        fontSize: 16
    },
    buttonPrice: {
        width: Layout.window.width / 4,
        borderRadius: 3,
        justifyContent: 'center',
        alignItems: 'center',
    },
});
